export interface Environment {
    appName: string;
    envName: string;
    production: boolean;
    gssoLoginUrl: string;
    loginCompleteUrl: string;
    versions: any;
}