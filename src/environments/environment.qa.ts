import { Environment } from "./models/environment.interface";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=qa` then `environment.qa.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment: Environment = {
  appName: 'Trialoversite QA Environment',
  envName: 'QA',
  production: false,
  gssoLoginUrl: 'https://dev.api.trialoversight.io/auth/gsso/login',
  loginCompleteUrl: 'https://dev.api.trialoversight.io/auth/gsso/login/complete',
  versions: {
  }
};
