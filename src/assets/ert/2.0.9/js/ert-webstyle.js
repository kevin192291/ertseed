/*!
 * Datepicker for Bootstrap v1.6.4 (https://github.com/eternicode/bootstrap-datepicker)
 *
 * Copyright 2012 Stefan Petre
 * Improvements by Andrew Rowls
 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */(function(factory){
    if (typeof define === "function" && define.amd) {
        define(["jquery"], factory);
    } else if (typeof exports === 'object') {
        factory(require('jquery'));
    } else {
        factory(jQuery);
    }
}(function($, undefined){

	function UTCDate(){
		return new Date(Date.UTC.apply(Date, arguments));
	}
	function UTCToday(){
		var today = new Date();
		return UTCDate(today.getFullYear(), today.getMonth(), today.getDate());
	}
	function isUTCEquals(date1, date2) {
		return (
			date1.getUTCFullYear() === date2.getUTCFullYear() &&
			date1.getUTCMonth() === date2.getUTCMonth() &&
			date1.getUTCDate() === date2.getUTCDate()
		);
	}
	function alias(method){
		return function(){
			return this[method].apply(this, arguments);
		};
	}
	function isValidDate(d) {
		return d && !isNaN(d.getTime());
	}

	var DateArray = (function(){
		var extras = {
			get: function(i){
				return this.slice(i)[0];
			},
			contains: function(d){
				// Array.indexOf is not cross-browser;
				// $.inArray doesn't work with Dates
				var val = d && d.valueOf();
				for (var i=0, l=this.length; i < l; i++)
					if (this[i].valueOf() === val)
						return i;
				return -1;
			},
			remove: function(i){
				this.splice(i,1);
			},
			replace: function(new_array){
				if (!new_array)
					return;
				if (!$.isArray(new_array))
					new_array = [new_array];
				this.clear();
				this.push.apply(this, new_array);
			},
			clear: function(){
				this.length = 0;
			},
			copy: function(){
				var a = new DateArray();
				a.replace(this);
				return a;
			}
		};

		return function(){
			var a = [];
			a.push.apply(a, arguments);
			$.extend(a, extras);
			return a;
		};
	})();


	// Picker object

	var Datepicker = function(element, options){
		$(element).data('datepicker', this);
		this._process_options(options);

		this.dates = new DateArray();
		this.viewDate = this.o.defaultViewDate;
		this.focusDate = null;

		this.element = $(element);
		this.isInput = this.element.is('input');
		this.inputField = this.isInput ? this.element : this.element.find('input');
		this.component = this.element.hasClass('date') ? this.element.find('.add-on, .input-group-addon, .btn') : false;
		this.hasInput = this.component && this.inputField.length;
		if (this.component && this.component.length === 0)
			this.component = false;
		this.isInline = !this.component && this.element.is('div');

		this.picker = $(DPGlobal.template);

		// Checking templates and inserting
		if (this._check_template(this.o.templates.leftArrow)) {
			this.picker.find('.prev').html(this.o.templates.leftArrow);
		}
		if (this._check_template(this.o.templates.rightArrow)) {
			this.picker.find('.next').html(this.o.templates.rightArrow);
		}

		this._buildEvents();
		this._attachEvents();

		if (this.isInline){
			this.picker.addClass('datepicker-inline').appendTo(this.element);
		}
		else {
			this.picker.addClass('datepicker-dropdown dropdown-menu');
		}

		if (this.o.rtl){
			this.picker.addClass('datepicker-rtl');
		}

		this.viewMode = this.o.startView;

		if (this.o.calendarWeeks)
			this.picker.find('thead .datepicker-title, tfoot .today, tfoot .clear')
						.attr('colspan', function(i, val){
							return parseInt(val) + 1;
						});

		this._allow_update = false;

		this.setStartDate(this._o.startDate);
		this.setEndDate(this._o.endDate);
		this.setDaysOfWeekDisabled(this.o.daysOfWeekDisabled);
		this.setDaysOfWeekHighlighted(this.o.daysOfWeekHighlighted);
		this.setDatesDisabled(this.o.datesDisabled);

		this.fillDow();
		this.fillMonths();

		this._allow_update = true;

		this.update();
		this.showMode();

		if (this.isInline){
			this.show();
		}
	};

	Datepicker.prototype = {
		constructor: Datepicker,

		_resolveViewName: function(view, default_value){
			if (view === 0 || view === 'days' || view === 'month') {
				return 0;
			}
			if (view === 1 || view === 'months' || view === 'year') {
				return 1;
			}
			if (view === 2 || view === 'years' || view === 'decade') {
				return 2;
			}
			if (view === 3 || view === 'decades' || view === 'century') {
				return 3;
			}
			if (view === 4 || view === 'centuries' || view === 'millennium') {
				return 4;
			}
			return default_value === undefined ? false : default_value;
		},

		_check_template: function(tmp){
			try {
				// If empty
				if (tmp === undefined || tmp === "") {
					return false;
				}
				// If no html, everything ok
				if ((tmp.match(/[<>]/g) || []).length <= 0) {
					return true;
				}
				// Checking if html is fine
				var jDom = $(tmp);
				return jDom.length > 0;
			}
			catch (ex) {
				return false;
			}
		},

		_process_options: function(opts){
			// Store raw options for reference
			this._o = $.extend({}, this._o, opts);
			// Processed options
			var o = this.o = $.extend({}, this._o);

			// Check if "de-DE" style date is available, if not language should
			// fallback to 2 letter code eg "de"
			var lang = o.language;
			if (!dates[lang]){
				lang = lang.split('-')[0];
				if (!dates[lang])
					lang = defaults.language;
			}
			o.language = lang;

			// Retrieve view index from any aliases
			o.startView = this._resolveViewName(o.startView, 0);
			o.minViewMode = this._resolveViewName(o.minViewMode, 0);
			o.maxViewMode = this._resolveViewName(o.maxViewMode, 4);

			// Check that the start view is between min and max
			o.startView = Math.min(o.startView, o.maxViewMode);
			o.startView = Math.max(o.startView, o.minViewMode);

			// true, false, or Number > 0
			if (o.multidate !== true){
				o.multidate = Number(o.multidate) || false;
				if (o.multidate !== false)
					o.multidate = Math.max(0, o.multidate);
			}
			o.multidateSeparator = String(o.multidateSeparator);

			o.weekStart %= 7;
			o.weekEnd = (o.weekStart + 6) % 7;

			var format = DPGlobal.parseFormat(o.format);
			if (o.startDate !== -Infinity){
				if (!!o.startDate){
					if (o.startDate instanceof Date)
						o.startDate = this._local_to_utc(this._zero_time(o.startDate));
					else
						o.startDate = DPGlobal.parseDate(o.startDate, format, o.language, o.assumeNearbyYear);
				}
				else {
					o.startDate = -Infinity;
				}
			}
			if (o.endDate !== Infinity){
				if (!!o.endDate){
					if (o.endDate instanceof Date)
						o.endDate = this._local_to_utc(this._zero_time(o.endDate));
					else
						o.endDate = DPGlobal.parseDate(o.endDate, format, o.language, o.assumeNearbyYear);
				}
				else {
					o.endDate = Infinity;
				}
			}

			o.daysOfWeekDisabled = o.daysOfWeekDisabled||[];
			if (!$.isArray(o.daysOfWeekDisabled))
				o.daysOfWeekDisabled = o.daysOfWeekDisabled.split(/[,\s]*/);
			o.daysOfWeekDisabled = $.map(o.daysOfWeekDisabled, function(d){
				return parseInt(d, 10);
			});

			o.daysOfWeekHighlighted = o.daysOfWeekHighlighted||[];
			if (!$.isArray(o.daysOfWeekHighlighted))
				o.daysOfWeekHighlighted = o.daysOfWeekHighlighted.split(/[,\s]*/);
			o.daysOfWeekHighlighted = $.map(o.daysOfWeekHighlighted, function(d){
				return parseInt(d, 10);
			});

			o.datesDisabled = o.datesDisabled||[];
			if (!$.isArray(o.datesDisabled)) {
				o.datesDisabled = [
					o.datesDisabled
				];
			}
			o.datesDisabled = $.map(o.datesDisabled,function(d){
				return DPGlobal.parseDate(d, format, o.language, o.assumeNearbyYear);
			});

			var plc = String(o.orientation).toLowerCase().split(/\s+/g),
				_plc = o.orientation.toLowerCase();
			plc = $.grep(plc, function(word){
				return /^auto|left|right|top|bottom$/.test(word);
			});
			o.orientation = {x: 'auto', y: 'auto'};
			if (!_plc || _plc === 'auto')
				; // no action
			else if (plc.length === 1){
				switch (plc[0]){
					case 'top':
					case 'bottom':
						o.orientation.y = plc[0];
						break;
					case 'left':
					case 'right':
						o.orientation.x = plc[0];
						break;
				}
			}
			else {
				_plc = $.grep(plc, function(word){
					return /^left|right$/.test(word);
				});
				o.orientation.x = _plc[0] || 'auto';

				_plc = $.grep(plc, function(word){
					return /^top|bottom$/.test(word);
				});
				o.orientation.y = _plc[0] || 'auto';
			}
			if (o.defaultViewDate) {
				var year = o.defaultViewDate.year || new Date().getFullYear();
				var month = o.defaultViewDate.month || 0;
				var day = o.defaultViewDate.day || 1;
				o.defaultViewDate = UTCDate(year, month, day);
			} else {
				o.defaultViewDate = UTCToday();
			}
		},
		_events: [],
		_secondaryEvents: [],
		_applyEvents: function(evs){
			for (var i=0, el, ch, ev; i < evs.length; i++){
				el = evs[i][0];
				if (evs[i].length === 2){
					ch = undefined;
					ev = evs[i][1];
				}
				else if (evs[i].length === 3){
					ch = evs[i][1];
					ev = evs[i][2];
				}
				el.on(ev, ch);
			}
		},
		_unapplyEvents: function(evs){
			for (var i=0, el, ev, ch; i < evs.length; i++){
				el = evs[i][0];
				if (evs[i].length === 2){
					ch = undefined;
					ev = evs[i][1];
				}
				else if (evs[i].length === 3){
					ch = evs[i][1];
					ev = evs[i][2];
				}
				el.off(ev, ch);
			}
		},
		_buildEvents: function(){
            var events = {
                keyup: $.proxy(function(e){
                    if ($.inArray(e.keyCode, [27, 37, 39, 38, 40, 32, 13, 9]) === -1)
                        this.update();
                }, this),
                keydown: $.proxy(this.keydown, this),
                paste: $.proxy(this.paste, this)
            };

            if (this.o.showOnFocus === true) {
                events.focus = $.proxy(this.show, this);
            }

            if (this.isInput) { // single input
                this._events = [
                    [this.element, events]
                ];
            }
            else if (this.component && this.hasInput) { // component: input + button
                this._events = [
                    // For components that are not readonly, allow keyboard nav
                    [this.inputField, events],
                    [this.component, {
                        click: $.proxy(this.show, this)
                    }]
                ];
            }
			else {
				this._events = [
					[this.element, {
						click: $.proxy(this.show, this),
						keydown: $.proxy(this.keydown, this)
					}]
				];
			}
			this._events.push(
				// Component: listen for blur on element descendants
				[this.element, '*', {
					blur: $.proxy(function(e){
						this._focused_from = e.target;
					}, this)
				}],
				// Input: listen for blur on element
				[this.element, {
					blur: $.proxy(function(e){
						this._focused_from = e.target;
					}, this)
				}]
			);

			if (this.o.immediateUpdates) {
				// Trigger input updates immediately on changed year/month
				this._events.push([this.element, {
					'changeYear changeMonth': $.proxy(function(e){
						this.update(e.date);
					}, this)
				}]);
			}

			this._secondaryEvents = [
				[this.picker, {
					click: $.proxy(this.click, this)
				}],
				[$(window), {
					resize: $.proxy(this.place, this)
				}],
				[$(document), {
					mousedown: $.proxy(function(e){
						// Clicked outside the datepicker, hide it
						if (!(
							this.element.is(e.target) ||
							this.element.find(e.target).length ||
							this.picker.is(e.target) ||
							this.picker.find(e.target).length ||
							this.isInline
						)){
							this.hide();
						}
					}, this)
				}]
			];
		},
		_attachEvents: function(){
			this._detachEvents();
			this._applyEvents(this._events);
		},
		_detachEvents: function(){
			this._unapplyEvents(this._events);
		},
		_attachSecondaryEvents: function(){
			this._detachSecondaryEvents();
			this._applyEvents(this._secondaryEvents);
		},
		_detachSecondaryEvents: function(){
			this._unapplyEvents(this._secondaryEvents);
		},
		_trigger: function(event, altdate){
			var date = altdate || this.dates.get(-1),
				local_date = this._utc_to_local(date);

			this.element.trigger({
				type: event,
				date: local_date,
				dates: $.map(this.dates, this._utc_to_local),
				format: $.proxy(function(ix, format){
					if (arguments.length === 0){
						ix = this.dates.length - 1;
						format = this.o.format;
					}
					else if (typeof ix === 'string'){
						format = ix;
						ix = this.dates.length - 1;
					}
					format = format || this.o.format;
					var date = this.dates.get(ix);
					return DPGlobal.formatDate(date, format, this.o.language);
				}, this)
			});
		},

		show: function(){
			if (this.inputField.prop('disabled') || (this.inputField.prop('readonly') && this.o.enableOnReadonly === false))
				return;
			if (!this.isInline)
				this.picker.appendTo(this.o.container);
			this.place();
			this.picker.show();
			this._attachSecondaryEvents();
			this._trigger('show');
			if ((window.navigator.msMaxTouchPoints || 'ontouchstart' in document) && this.o.disableTouchKeyboard) {
				$(this.element).blur();
			}
			return this;
		},

		hide: function(){
			if (this.isInline || !this.picker.is(':visible'))
				return this;
			this.focusDate = null;
			this.picker.hide().detach();
			this._detachSecondaryEvents();
			this.viewMode = this.o.startView;
			this.showMode();

			if (this.o.forceParse && this.inputField.val())
				this.setValue();
			this._trigger('hide');
			return this;
		},

		destroy: function(){
			this.hide();
			this._detachEvents();
			this._detachSecondaryEvents();
			this.picker.remove();
			delete this.element.data().datepicker;
			if (!this.isInput){
				delete this.element.data().date;
			}
			return this;
		},

		paste: function(evt){
			var dateString;
			if (evt.originalEvent.clipboardData && evt.originalEvent.clipboardData.types
				&& $.inArray('text/plain', evt.originalEvent.clipboardData.types) !== -1) {
				dateString = evt.originalEvent.clipboardData.getData('text/plain');
			}
			else if (window.clipboardData) {
				dateString = window.clipboardData.getData('Text');
			}
			else {
				return;
			}
			this.setDate(dateString);
			this.update();
			evt.preventDefault();
		},

		_utc_to_local: function(utc){
			return utc && new Date(utc.getTime() + (utc.getTimezoneOffset()*60000));
		},
		_local_to_utc: function(local){
			return local && new Date(local.getTime() - (local.getTimezoneOffset()*60000));
		},
		_zero_time: function(local){
			return local && new Date(local.getFullYear(), local.getMonth(), local.getDate());
		},
		_zero_utc_time: function(utc){
			return utc && new Date(Date.UTC(utc.getUTCFullYear(), utc.getUTCMonth(), utc.getUTCDate()));
		},

		getDates: function(){
			return $.map(this.dates, this._utc_to_local);
		},

		getUTCDates: function(){
			return $.map(this.dates, function(d){
				return new Date(d);
			});
		},

		getDate: function(){
			return this._utc_to_local(this.getUTCDate());
		},

		getUTCDate: function(){
			var selected_date = this.dates.get(-1);
			if (typeof selected_date !== 'undefined') {
				return new Date(selected_date);
			} else {
				return null;
			}
		},

		clearDates: function(){
			if (this.inputField) {
				this.inputField.val('');
			}

			this.update();
			this._trigger('changeDate');

			if (this.o.autoclose) {
				this.hide();
			}
		},
		setDates: function(){
			var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
			this.update.apply(this, args);
			this._trigger('changeDate');
			this.setValue();
			return this;
		},

		setUTCDates: function(){
			var args = $.isArray(arguments[0]) ? arguments[0] : arguments;
			this.update.apply(this, $.map(args, this._utc_to_local));
			this._trigger('changeDate');
			this.setValue();
			return this;
		},

		setDate: alias('setDates'),
		setUTCDate: alias('setUTCDates'),
		remove: alias('destroy'),

		setValue: function(){
			var formatted = this.getFormattedDate();
			this.inputField.val(formatted);
			return this;
		},

		getFormattedDate: function(format){
			if (format === undefined)
				format = this.o.format;

			var lang = this.o.language;
			return $.map(this.dates, function(d){
				return DPGlobal.formatDate(d, format, lang);
			}).join(this.o.multidateSeparator);
		},

		getStartDate: function(){
			return this.o.startDate;
		},

		setStartDate: function(startDate){
			this._process_options({startDate: startDate});
			this.update();
			this.updateNavArrows();
			return this;
		},

		getEndDate: function(){
			return this.o.endDate;
		},

		setEndDate: function(endDate){
			this._process_options({endDate: endDate});
			this.update();
			this.updateNavArrows();
			return this;
		},

		setDaysOfWeekDisabled: function(daysOfWeekDisabled){
			this._process_options({daysOfWeekDisabled: daysOfWeekDisabled});
			this.update();
			this.updateNavArrows();
			return this;
		},

		setDaysOfWeekHighlighted: function(daysOfWeekHighlighted){
			this._process_options({daysOfWeekHighlighted: daysOfWeekHighlighted});
			this.update();
			return this;
		},

		setDatesDisabled: function(datesDisabled){
			this._process_options({datesDisabled: datesDisabled});
			this.update();
			this.updateNavArrows();
		},

		place: function(){
			if (this.isInline)
				return this;
			var calendarWidth = this.picker.outerWidth(),
				calendarHeight = this.picker.outerHeight(),
				visualPadding = 10,
				container = $(this.o.container),
				windowWidth = container.width(),
				scrollTop = this.o.container === 'body' ? $(document).scrollTop() : container.scrollTop(),
				appendOffset = container.offset();

			var parentsZindex = [];
			this.element.parents().each(function(){
				var itemZIndex = $(this).css('z-index');
				if (itemZIndex !== 'auto' && itemZIndex !== 0) parentsZindex.push(parseInt(itemZIndex));
			});
			var zIndex = Math.max.apply(Math, parentsZindex) + this.o.zIndexOffset;
			var offset = this.component ? this.component.parent().offset() : this.element.offset();
			var height = this.component ? this.component.outerHeight(true) : this.element.outerHeight(false);
			var width = this.component ? this.component.outerWidth(true) : this.element.outerWidth(false);
			var left = offset.left - appendOffset.left,
				top = offset.top - appendOffset.top;

			if (this.o.container !== 'body') {
				top += scrollTop;
			}

			this.picker.removeClass(
				'datepicker-orient-top datepicker-orient-bottom '+
				'datepicker-orient-right datepicker-orient-left'
			);

			if (this.o.orientation.x !== 'auto'){
				this.picker.addClass('datepicker-orient-' + this.o.orientation.x);
				if (this.o.orientation.x === 'right')
					left -= calendarWidth - width;
			}
			// auto x orientation is best-placement: if it crosses a window
			// edge, fudge it sideways
			else {
				if (offset.left < 0) {
					// component is outside the window on the left side. Move it into visible range
					this.picker.addClass('datepicker-orient-left');
					left -= offset.left - visualPadding;
				} else if (left + calendarWidth > windowWidth) {
					// the calendar passes the widow right edge. Align it to component right side
					this.picker.addClass('datepicker-orient-right');
					left += width - calendarWidth;
				} else {
					// Default to left
					this.picker.addClass('datepicker-orient-left');
				}
			}

			// auto y orientation is best-situation: top or bottom, no fudging,
			// decision based on which shows more of the calendar
			var yorient = this.o.orientation.y,
				top_overflow;
			if (yorient === 'auto'){
				top_overflow = -scrollTop + top - calendarHeight;
				yorient = top_overflow < 0 ? 'bottom' : 'top';
			}

			this.picker.addClass('datepicker-orient-' + yorient);
			if (yorient === 'top')
				top -= calendarHeight + parseInt(this.picker.css('padding-top'));
			else
				top += height;

			if (this.o.rtl) {
				var right = windowWidth - (left + width);
				this.picker.css({
					top: top,
					right: right,
					zIndex: zIndex
				});
			} else {
				this.picker.css({
					top: top,
					left: left,
					zIndex: zIndex
				});
			}
			return this;
		},

		_allow_update: true,
		update: function(){
			if (!this._allow_update)
				return this;

			var oldDates = this.dates.copy(),
				dates = [],
				fromArgs = false;
			if (arguments.length){
				$.each(arguments, $.proxy(function(i, date){
					if (date instanceof Date)
						date = this._local_to_utc(date);
					dates.push(date);
				}, this));
				fromArgs = true;
			}
			else {
				dates = this.isInput
						? this.element.val()
						: this.element.data('date') || this.inputField.val();
				if (dates && this.o.multidate)
					dates = dates.split(this.o.multidateSeparator);
				else
					dates = [dates];
				delete this.element.data().date;
			}

			dates = $.map(dates, $.proxy(function(date){
				return DPGlobal.parseDate(date, this.o.format, this.o.language, this.o.assumeNearbyYear);
			}, this));
			dates = $.grep(dates, $.proxy(function(date){
				return (
					!this.dateWithinRange(date) ||
					!date
				);
			}, this), true);
			this.dates.replace(dates);

			if (this.dates.length)
				this.viewDate = new Date(this.dates.get(-1));
			else if (this.viewDate < this.o.startDate)
				this.viewDate = new Date(this.o.startDate);
			else if (this.viewDate > this.o.endDate)
				this.viewDate = new Date(this.o.endDate);
			else
				this.viewDate = this.o.defaultViewDate;

			if (fromArgs){
				// setting date by clicking
				this.setValue();
			}
			else if (dates.length){
				// setting date by typing
				if (String(oldDates) !== String(this.dates))
					this._trigger('changeDate');
			}
			if (!this.dates.length && oldDates.length)
				this._trigger('clearDate');

			this.fill();
			this.element.change();
			return this;
		},

		fillDow: function(){
			var dowCnt = this.o.weekStart,
				html = '<tr>';
			if (this.o.calendarWeeks){
				this.picker.find('.datepicker-days .datepicker-switch')
					.attr('colspan', function(i, val){
						return parseInt(val) + 1;
					});
				html += '<th class="cw">&#160;</th>';
			}
			while (dowCnt < this.o.weekStart + 7){
				html += '<th class="dow';
        if ($.inArray(dowCnt, this.o.daysOfWeekDisabled) > -1)
          html += ' disabled';
        html += '">'+dates[this.o.language].daysMin[(dowCnt++)%7]+'</th>';
			}
			html += '</tr>';
			this.picker.find('.datepicker-days thead').append(html);
		},

		fillMonths: function(){
      var localDate = this._utc_to_local(this.viewDate);
			var html = '',
			i = 0;
			while (i < 12){
        var focused = localDate && localDate.getMonth() === i ? ' focused' : '';
				html += '<span class="month' + focused + '">' + dates[this.o.language].monthsShort[i++]+'</span>';
			}
			this.picker.find('.datepicker-months td').html(html);
		},

		setRange: function(range){
			if (!range || !range.length)
				delete this.range;
			else
				this.range = $.map(range, function(d){
					return d.valueOf();
				});
			this.fill();
		},

		getClassNames: function(date){
			var cls = [],
				year = this.viewDate.getUTCFullYear(),
				month = this.viewDate.getUTCMonth(),
				today = new Date();
			if (date.getUTCFullYear() < year || (date.getUTCFullYear() === year && date.getUTCMonth() < month)){
				cls.push('old');
			}
			else if (date.getUTCFullYear() > year || (date.getUTCFullYear() === year && date.getUTCMonth() > month)){
				cls.push('new');
			}
			if (this.focusDate && date.valueOf() === this.focusDate.valueOf())
				cls.push('focused');
			// Compare internal UTC date with local today, not UTC today
			if (this.o.todayHighlight &&
				date.getUTCFullYear() === today.getFullYear() &&
				date.getUTCMonth() === today.getMonth() &&
				date.getUTCDate() === today.getDate()){
				cls.push('today');
			}
			if (this.dates.contains(date) !== -1)
				cls.push('active');
			if (!this.dateWithinRange(date)){
				cls.push('disabled');
			}
			if (this.dateIsDisabled(date)){
				cls.push('disabled', 'disabled-date');	
			} 
			if ($.inArray(date.getUTCDay(), this.o.daysOfWeekHighlighted) !== -1){
				cls.push('highlighted');
			}

			if (this.range){
				if (date > this.range[0] && date < this.range[this.range.length-1]){
					cls.push('range');
				}
				if ($.inArray(date.valueOf(), this.range) !== -1){
					cls.push('selected');
				}
				if (date.valueOf() === this.range[0]){
          cls.push('range-start');
        }
        if (date.valueOf() === this.range[this.range.length-1]){
          cls.push('range-end');
        }
			}
			return cls;
		},

		_fill_yearsView: function(selector, cssClass, factor, step, currentYear, startYear, endYear, callback){
			var html, view, year, steps, startStep, endStep, thisYear, i, classes, tooltip, before;

			html      = '';
			view      = this.picker.find(selector);
			year      = parseInt(currentYear / factor, 10) * factor;
			startStep = parseInt(startYear / step, 10) * step;
			endStep   = parseInt(endYear / step, 10) * step;
			steps     = $.map(this.dates, function(d){
				return parseInt(d.getUTCFullYear() / step, 10) * step;
			});

			view.find('.datepicker-switch').text(year + '-' + (year + step * 9));

			thisYear = year - step;
			for (i = -1; i < 11; i += 1) {
				classes = [cssClass];
				tooltip = null;

				if (i === -1) {
					classes.push('old');
				} else if (i === 10) {
					classes.push('new');
				}
				if ($.inArray(thisYear, steps) !== -1) {
					classes.push('active');
				}
				if (thisYear < startStep || thisYear > endStep) {
					classes.push('disabled');
				}
        if (thisYear === this.viewDate.getFullYear()) {
				  classes.push('focused');
        }

				if (callback !== $.noop) {
					before = callback(new Date(thisYear, 0, 1));
					if (before === undefined) {
						before = {};
					} else if (typeof(before) === 'boolean') {
						before = {enabled: before};
					} else if (typeof(before) === 'string') {
						before = {classes: before};
					}
					if (before.enabled === false) {
						classes.push('disabled');
					}
					if (before.classes) {
						classes = classes.concat(before.classes.split(/\s+/));
					}
					if (before.tooltip) {
						tooltip = before.tooltip;
					}
				}

				html += '<span class="' + classes.join(' ') + '"' + (tooltip ? ' title="' + tooltip + '"' : '') + '>' + thisYear + '</span>';
				thisYear += step;
			}
			view.find('td').html(html);
		},

		fill: function(){
			var d = new Date(this.viewDate),
				year = d.getUTCFullYear(),
				month = d.getUTCMonth(),
				startYear = this.o.startDate !== -Infinity ? this.o.startDate.getUTCFullYear() : -Infinity,
				startMonth = this.o.startDate !== -Infinity ? this.o.startDate.getUTCMonth() : -Infinity,
				endYear = this.o.endDate !== Infinity ? this.o.endDate.getUTCFullYear() : Infinity,
				endMonth = this.o.endDate !== Infinity ? this.o.endDate.getUTCMonth() : Infinity,
				todaytxt = dates[this.o.language].today || dates['en'].today || '',
				cleartxt = dates[this.o.language].clear || dates['en'].clear || '',
				titleFormat = dates[this.o.language].titleFormat || dates['en'].titleFormat,
				tooltip,
				before;
			if (isNaN(year) || isNaN(month))
				return;
			this.picker.find('.datepicker-days .datepicker-switch')
						.text(DPGlobal.formatDate(d, titleFormat, this.o.language));
			this.picker.find('tfoot .today')
						.text(todaytxt)
						.toggle(this.o.todayBtn !== false);
			this.picker.find('tfoot .clear')
						.text(cleartxt)
						.toggle(this.o.clearBtn !== false);
			this.picker.find('thead .datepicker-title')
						.text(this.o.title)
						.toggle(this.o.title !== '');
			this.updateNavArrows();
			this.fillMonths();
			var prevMonth = UTCDate(year, month-1, 28),
				day = DPGlobal.getDaysInMonth(prevMonth.getUTCFullYear(), prevMonth.getUTCMonth());
			prevMonth.setUTCDate(day);
			prevMonth.setUTCDate(day - (prevMonth.getUTCDay() - this.o.weekStart + 7)%7);
			var nextMonth = new Date(prevMonth);
			if (prevMonth.getUTCFullYear() < 100){
        nextMonth.setUTCFullYear(prevMonth.getUTCFullYear());
      }
			nextMonth.setUTCDate(nextMonth.getUTCDate() + 42);
			nextMonth = nextMonth.valueOf();
			var html = [];
			var clsName;
			while (prevMonth.valueOf() < nextMonth){
				if (prevMonth.getUTCDay() === this.o.weekStart){
					html.push('<tr>');
					if (this.o.calendarWeeks){
						// ISO 8601: First week contains first thursday.
						// ISO also states week starts on Monday, but we can be more abstract here.
						var
							// Start of current week: based on weekstart/current date
							ws = new Date(+prevMonth + (this.o.weekStart - prevMonth.getUTCDay() - 7) % 7 * 864e5),
							// Thursday of this week
							th = new Date(Number(ws) + (7 + 4 - ws.getUTCDay()) % 7 * 864e5),
							// First Thursday of year, year from thursday
							yth = new Date(Number(yth = UTCDate(th.getUTCFullYear(), 0, 1)) + (7 + 4 - yth.getUTCDay())%7*864e5),
							// Calendar week: ms between thursdays, div ms per day, div 7 days
							calWeek =  (th - yth) / 864e5 / 7 + 1;
						html.push('<td class="cw">'+ calWeek +'</td>');
					}
				}
				clsName = this.getClassNames(prevMonth);
				clsName.push('day');

				if (this.o.beforeShowDay !== $.noop){
					before = this.o.beforeShowDay(this._utc_to_local(prevMonth));
					if (before === undefined)
						before = {};
					else if (typeof(before) === 'boolean')
						before = {enabled: before};
					else if (typeof(before) === 'string')
						before = {classes: before};
					if (before.enabled === false)
						clsName.push('disabled');
					if (before.classes)
						clsName = clsName.concat(before.classes.split(/\s+/));
					if (before.tooltip)
						tooltip = before.tooltip;
				}

				//Check if uniqueSort exists (supported by jquery >=1.12 and >=2.2)
				//Fallback to unique function for older jquery versions
				if ($.isFunction($.uniqueSort)) {
					clsName = $.uniqueSort(clsName);
				} else {
					clsName = $.unique(clsName);
				}

				html.push('<td class="'+clsName.join(' ')+'"' + (tooltip ? ' title="'+tooltip+'"' : '') + '>'+prevMonth.getUTCDate() + '</td>');
				tooltip = null;
				if (prevMonth.getUTCDay() === this.o.weekEnd){
					html.push('</tr>');
				}
				prevMonth.setUTCDate(prevMonth.getUTCDate()+1);
			}
			this.picker.find('.datepicker-days tbody').empty().append(html.join(''));

			var monthsTitle = dates[this.o.language].monthsTitle || dates['en'].monthsTitle || 'Months';
			var months = this.picker.find('.datepicker-months')
						.find('.datepicker-switch')
							.text(this.o.maxViewMode < 2 ? monthsTitle : year)
							.end()
						.find('span').removeClass('active');

			$.each(this.dates, function(i, d){
				if (d.getUTCFullYear() === year)
					months.eq(d.getUTCMonth()).addClass('active');
			});

			if (year < startYear || year > endYear){
				months.addClass('disabled');
			}
			if (year === startYear){
				months.slice(0, startMonth).addClass('disabled');
			}
			if (year === endYear){
				months.slice(endMonth+1).addClass('disabled');
			}

			if (this.o.beforeShowMonth !== $.noop){
				var that = this;
				$.each(months, function(i, month){
          var moDate = new Date(year, i, 1);
          var before = that.o.beforeShowMonth(moDate);
					if (before === undefined)
						before = {};
					else if (typeof(before) === 'boolean')
						before = {enabled: before};
					else if (typeof(before) === 'string')
						before = {classes: before};
					if (before.enabled === false && !$(month).hasClass('disabled'))
					    $(month).addClass('disabled');
					if (before.classes)
					    $(month).addClass(before.classes);
					if (before.tooltip)
					    $(month).prop('title', before.tooltip);
				});
			}

			// Generating decade/years picker
			this._fill_yearsView(
				'.datepicker-years',
				'year',
				10,
				1,
				year,
				startYear,
				endYear,
				this.o.beforeShowYear
			);

			// Generating century/decades picker
			this._fill_yearsView(
				'.datepicker-decades',
				'decade',
				100,
				10,
				year,
				startYear,
				endYear,
				this.o.beforeShowDecade
			);

			// Generating millennium/centuries picker
			this._fill_yearsView(
				'.datepicker-centuries',
				'century',
				1000,
				100,
				year,
				startYear,
				endYear,
				this.o.beforeShowCentury
			);
		},

		updateNavArrows: function(){
			if (!this._allow_update)
				return;

			var d = new Date(this.viewDate),
				year = d.getUTCFullYear(),
				month = d.getUTCMonth();
			switch (this.viewMode){
				case 0:
					if (this.o.startDate !== -Infinity && year <= this.o.startDate.getUTCFullYear() && month <= this.o.startDate.getUTCMonth()){
						this.picker.find('.prev').css({visibility: 'hidden'});
					}
					else {
						this.picker.find('.prev').css({visibility: 'visible'});
					}
					if (this.o.endDate !== Infinity && year >= this.o.endDate.getUTCFullYear() && month >= this.o.endDate.getUTCMonth()){
						this.picker.find('.next').css({visibility: 'hidden'});
					}
					else {
						this.picker.find('.next').css({visibility: 'visible'});
					}
					break;
				case 1:
				case 2:
				case 3:
				case 4:
					if (this.o.startDate !== -Infinity && year <= this.o.startDate.getUTCFullYear() || this.o.maxViewMode < 2){
						this.picker.find('.prev').css({visibility: 'hidden'});
					}
					else {
						this.picker.find('.prev').css({visibility: 'visible'});
					}
					if (this.o.endDate !== Infinity && year >= this.o.endDate.getUTCFullYear() || this.o.maxViewMode < 2){
						this.picker.find('.next').css({visibility: 'hidden'});
					}
					else {
						this.picker.find('.next').css({visibility: 'visible'});
					}
					break;
			}
		},

		click: function(e){
			e.preventDefault();
			e.stopPropagation();

			var target, dir, day, year, month, monthChanged, yearChanged;
			target = $(e.target);

			// Clicked on the switch
			if (target.hasClass('datepicker-switch')){
				this.showMode(1);
			}

			// Clicked on prev or next
			var navArrow = target.closest('.prev, .next');
			if (navArrow.length > 0) {
				dir = DPGlobal.modes[this.viewMode].navStep * (navArrow.hasClass('prev') ? -1 : 1);
				if (this.viewMode === 0){
					this.viewDate = this.moveMonth(this.viewDate, dir);
					this._trigger('changeMonth', this.viewDate);
				} else {
					this.viewDate = this.moveYear(this.viewDate, dir);
					if (this.viewMode === 1){
						this._trigger('changeYear', this.viewDate);
					}
				}
				this.fill();
			}

			// Clicked on today button
			if (target.hasClass('today') && !target.hasClass('day')){
				this.showMode(-2);
				this._setDate(UTCToday(), this.o.todayBtn === 'linked' ? null : 'view');
			}

			// Clicked on clear button
			if (target.hasClass('clear')){
				this.clearDates();
			}

			if (!target.hasClass('disabled')){
				// Clicked on a day
				if (target.hasClass('day')){
					day = parseInt(target.text(), 10) || 1;
					year = this.viewDate.getUTCFullYear();
					month = this.viewDate.getUTCMonth();

					// From last month
					if (target.hasClass('old')){
						if (month === 0) {
							month = 11;
							year = year - 1;
							monthChanged = true;
							yearChanged = true;
						} else {
							month = month - 1;
							monthChanged = true;
 						}
 					}

					// From next month
					if (target.hasClass('new')) {
						if (month === 11){
							month = 0;
							year = year + 1;
							monthChanged = true;
							yearChanged = true;
 						} else {
							month = month + 1;
							monthChanged = true;
 						}
					}
					this._setDate(UTCDate(year, month, day));
					if (yearChanged) {
						this._trigger('changeYear', this.viewDate);
					}
					if (monthChanged) {
						this._trigger('changeMonth', this.viewDate);
					}
				}

				// Clicked on a month
				if (target.hasClass('month')) {
					this.viewDate.setUTCDate(1);
					day = 1;
					month = target.parent().find('span').index(target);
					year = this.viewDate.getUTCFullYear();
					this.viewDate.setUTCMonth(month);
					this._trigger('changeMonth', this.viewDate);
					if (this.o.minViewMode === 1){
						this._setDate(UTCDate(year, month, day));
						this.showMode();
					} else {
						this.showMode(-1);
					}
					this.fill();
				}

				// Clicked on a year
				if (target.hasClass('year')
						|| target.hasClass('decade')
						|| target.hasClass('century')) {
					this.viewDate.setUTCDate(1);

					day = 1;
					month = 0;
					year = parseInt(target.text(), 10)||0;
					this.viewDate.setUTCFullYear(year);

					if (target.hasClass('year')){
						this._trigger('changeYear', this.viewDate);
						if (this.o.minViewMode === 2){
							this._setDate(UTCDate(year, month, day));
						}
					}
					if (target.hasClass('decade')){
						this._trigger('changeDecade', this.viewDate);
						if (this.o.minViewMode === 3){
							this._setDate(UTCDate(year, month, day));
						}
					}
					if (target.hasClass('century')){
						this._trigger('changeCentury', this.viewDate);
						if (this.o.minViewMode === 4){
							this._setDate(UTCDate(year, month, day));
						}
					}

					this.showMode(-1);
					this.fill();
				}
			}

			if (this.picker.is(':visible') && this._focused_from){
				$(this._focused_from).focus();
			}
			delete this._focused_from;
		},

		_toggle_multidate: function(date){
			var ix = this.dates.contains(date);
			if (!date){
				this.dates.clear();
			}

			if (ix !== -1){
				if (this.o.multidate === true || this.o.multidate > 1 || this.o.toggleActive){
					this.dates.remove(ix);
				}
			} else if (this.o.multidate === false) {
				this.dates.clear();
				this.dates.push(date);
			}
			else {
				this.dates.push(date);
			}

			if (typeof this.o.multidate === 'number')
				while (this.dates.length > this.o.multidate)
					this.dates.remove(0);
		},

		_setDate: function(date, which){
			if (!which || which === 'date')
				this._toggle_multidate(date && new Date(date));
			if (!which || which === 'view')
				this.viewDate = date && new Date(date);

			this.fill();
			this.setValue();
			if (!which || which !== 'view') {
				this._trigger('changeDate');
			}
			if (this.inputField){
				this.inputField.change();
			}
			if (this.o.autoclose && (!which || which === 'date')){
				this.hide();
			}
		},

		moveDay: function(date, dir){
			var newDate = new Date(date);
			newDate.setUTCDate(date.getUTCDate() + dir);

			return newDate;
		},

		moveWeek: function(date, dir){
			return this.moveDay(date, dir * 7);
		},

		moveMonth: function(date, dir){
			if (!isValidDate(date))
				return this.o.defaultViewDate;
			if (!dir)
				return date;
			var new_date = new Date(date.valueOf()),
				day = new_date.getUTCDate(),
				month = new_date.getUTCMonth(),
				mag = Math.abs(dir),
				new_month, test;
			dir = dir > 0 ? 1 : -1;
			if (mag === 1){
				test = dir === -1
					// If going back one month, make sure month is not current month
					// (eg, Mar 31 -> Feb 31 == Feb 28, not Mar 02)
					? function(){
						return new_date.getUTCMonth() === month;
					}
					// If going forward one month, make sure month is as expected
					// (eg, Jan 31 -> Feb 31 == Feb 28, not Mar 02)
					: function(){
						return new_date.getUTCMonth() !== new_month;
					};
				new_month = month + dir;
				new_date.setUTCMonth(new_month);
				// Dec -> Jan (12) or Jan -> Dec (-1) -- limit expected date to 0-11
				if (new_month < 0 || new_month > 11)
					new_month = (new_month + 12) % 12;
			}
			else {
				// For magnitudes >1, move one month at a time...
				for (var i=0; i < mag; i++)
					// ...which might decrease the day (eg, Jan 31 to Feb 28, etc)...
					new_date = this.moveMonth(new_date, dir);
				// ...then reset the day, keeping it in the new month
				new_month = new_date.getUTCMonth();
				new_date.setUTCDate(day);
				test = function(){
					return new_month !== new_date.getUTCMonth();
				};
			}
			// Common date-resetting loop -- if date is beyond end of month, make it
			// end of month
			while (test()){
				new_date.setUTCDate(--day);
				new_date.setUTCMonth(new_month);
			}
			return new_date;
		},

		moveYear: function(date, dir){
			return this.moveMonth(date, dir*12);
		},

		moveAvailableDate: function(date, dir, fn){
			do {
				date = this[fn](date, dir);

				if (!this.dateWithinRange(date))
					return false;

				fn = 'moveDay';
			}
			while (this.dateIsDisabled(date));

			return date;
		},

		weekOfDateIsDisabled: function(date){
			return $.inArray(date.getUTCDay(), this.o.daysOfWeekDisabled) !== -1;
		},

		dateIsDisabled: function(date){
			return (
				this.weekOfDateIsDisabled(date) ||
				$.grep(this.o.datesDisabled, function(d){
					return isUTCEquals(date, d);
				}).length > 0
			);
		},

		dateWithinRange: function(date){
			return date >= this.o.startDate && date <= this.o.endDate;
		},

		keydown: function(e){
			if (!this.picker.is(':visible')){
				if (e.keyCode === 40 || e.keyCode === 27) { // allow down to re-show picker
					this.show();
					e.stopPropagation();
        }
				return;
			}
			var dateChanged = false,
				dir, newViewDate,
				focusDate = this.focusDate || this.viewDate;
			switch (e.keyCode){
				case 27: // escape
					if (this.focusDate){
						this.focusDate = null;
						this.viewDate = this.dates.get(-1) || this.viewDate;
						this.fill();
					}
					else
						this.hide();
					e.preventDefault();
					e.stopPropagation();
					break;
				case 37: // left
				case 38: // up
				case 39: // right
				case 40: // down
					if (!this.o.keyboardNavigation || this.o.daysOfWeekDisabled.length === 7)
						break;
					dir = e.keyCode === 37 || e.keyCode === 38 ? -1 : 1;
          if (this.viewMode === 0) {
  					if (e.ctrlKey){
  						newViewDate = this.moveAvailableDate(focusDate, dir, 'moveYear');

  						if (newViewDate)
  							this._trigger('changeYear', this.viewDate);
  					}
  					else if (e.shiftKey){
  						newViewDate = this.moveAvailableDate(focusDate, dir, 'moveMonth');

  						if (newViewDate)
  							this._trigger('changeMonth', this.viewDate);
  					}
  					else if (e.keyCode === 37 || e.keyCode === 39){
  						newViewDate = this.moveAvailableDate(focusDate, dir, 'moveDay');
  					}
  					else if (!this.weekOfDateIsDisabled(focusDate)){
  						newViewDate = this.moveAvailableDate(focusDate, dir, 'moveWeek');
  					}
          } else if (this.viewMode === 1) {
            if (e.keyCode === 38 || e.keyCode === 40) {
              dir = dir * 4;
            }
            newViewDate = this.moveAvailableDate(focusDate, dir, 'moveMonth');
          } else if (this.viewMode === 2) {
            if (e.keyCode === 38 || e.keyCode === 40) {
              dir = dir * 4;
            }
            newViewDate = this.moveAvailableDate(focusDate, dir, 'moveYear');
          }
					if (newViewDate){
						this.focusDate = this.viewDate = newViewDate;
						this.setValue();
						this.fill();
						e.preventDefault();
					}
					break;
				case 13: // enter
					if (!this.o.forceParse)
						break;
					focusDate = this.focusDate || this.dates.get(-1) || this.viewDate;
					if (this.o.keyboardNavigation) {
						this._toggle_multidate(focusDate);
						dateChanged = true;
					}
					this.focusDate = null;
					this.viewDate = this.dates.get(-1) || this.viewDate;
					this.setValue();
					this.fill();
					if (this.picker.is(':visible')){
						e.preventDefault();
						e.stopPropagation();
						if (this.o.autoclose)
							this.hide();
					}
					break;
				case 9: // tab
					this.focusDate = null;
					this.viewDate = this.dates.get(-1) || this.viewDate;
					this.fill();
					this.hide();
					break;
			}
			if (dateChanged){
				if (this.dates.length)
					this._trigger('changeDate');
				else
					this._trigger('clearDate');
				if (this.inputField){
					this.inputField.change();
				}
			}
		},

		showMode: function(dir){
			if (dir){
				this.viewMode = Math.max(this.o.minViewMode, Math.min(this.o.maxViewMode, this.viewMode + dir));
			}
			this.picker
				.children('div')
				.hide()
				.filter('.datepicker-' + DPGlobal.modes[this.viewMode].clsName)
					.show();
			this.updateNavArrows();
		}
	};

	var DateRangePicker = function(element, options){
		$(element).data('datepicker', this);
		this.element = $(element);
		this.inputs = $.map(options.inputs, function(i){
			return i.jquery ? i[0] : i;
		});
		delete options.inputs;

		datepickerPlugin.call($(this.inputs), options)
			.on('changeDate', $.proxy(this.dateUpdated, this));

		this.pickers = $.map(this.inputs, function(i){
			return $(i).data('datepicker');
		});
		this.updateDates();
	};
	DateRangePicker.prototype = {
		updateDates: function(){
			this.dates = $.map(this.pickers, function(i){
				return i.getUTCDate();
			});
			this.updateRanges();
		},
		updateRanges: function(){
			var range = $.map(this.dates, function(d){
				return d.valueOf();
			});
			$.each(this.pickers, function(i, p){
				p.setRange(range);
			});
		},
		dateUpdated: function(e){
			// `this.updating` is a workaround for preventing infinite recursion
			// between `changeDate` triggering and `setUTCDate` calling.  Until
			// there is a better mechanism.
			if (this.updating)
				return;
			this.updating = true;

			var dp = $(e.target).data('datepicker');

			if (typeof(dp) === "undefined") {
				return;
			}

			var new_date = dp.getUTCDate(),
				i = $.inArray(e.target, this.inputs),
				j = i - 1,
				k = i + 1,
				l = this.inputs.length;
			if (i === -1)
				return;

			$.each(this.pickers, function(i, p){
				if (!p.getUTCDate())
					p.setUTCDate(new_date);
			});

			if (new_date < this.dates[j]){
				// Date being moved earlier/left
				while (j >= 0 && new_date < this.dates[j]){
					this.pickers[j--].setUTCDate(new_date);
				}
			}
			else if (new_date > this.dates[k]){
				// Date being moved later/right
				while (k < l && new_date > this.dates[k]){
					this.pickers[k++].setUTCDate(new_date);
				}
			}
			this.updateDates();

			delete this.updating;
		},
		remove: function(){
			$.map(this.pickers, function(p){ p.remove(); });
			delete this.element.data().datepicker;
		}
	};

	function opts_from_el(el, prefix){
		// Derive options from element data-attrs
		var data = $(el).data(),
			out = {}, inkey,
			replace = new RegExp('^' + prefix.toLowerCase() + '([A-Z])');
		prefix = new RegExp('^' + prefix.toLowerCase());
		function re_lower(_,a){
			return a.toLowerCase();
		}
		for (var key in data)
			if (prefix.test(key)){
				inkey = key.replace(replace, re_lower);
				out[inkey] = data[key];
			}
		return out;
	}

	function opts_from_locale(lang){
		// Derive options from locale plugins
		var out = {};
		// Check if "de-DE" style date is available, if not language should
		// fallback to 2 letter code eg "de"
		if (!dates[lang]){
			lang = lang.split('-')[0];
			if (!dates[lang])
				return;
		}
		var d = dates[lang];
		$.each(locale_opts, function(i,k){
			if (k in d)
				out[k] = d[k];
		});
		return out;
	}

	var old = $.fn.datepicker;
	var datepickerPlugin = function(option){
		var args = Array.apply(null, arguments);
		args.shift();
		var internal_return;
		this.each(function(){
			var $this = $(this),
				data = $this.data('datepicker'),
				options = typeof option === 'object' && option;
			if (!data){
				var elopts = opts_from_el(this, 'date'),
					// Preliminary otions
					xopts = $.extend({}, defaults, elopts, options),
					locopts = opts_from_locale(xopts.language),
					// Options priority: js args, data-attrs, locales, defaults
					opts = $.extend({}, defaults, locopts, elopts, options);
				if ($this.hasClass('input-daterange') || opts.inputs){
					$.extend(opts, {
						inputs: opts.inputs || $this.find('input').toArray()
					});
					data = new DateRangePicker(this, opts);
				}
				else {
					data = new Datepicker(this, opts);
				}
				$this.data('datepicker', data);
			}
			if (typeof option === 'string' && typeof data[option] === 'function'){
				internal_return = data[option].apply(data, args);
			}
		});

		if (
			internal_return === undefined ||
			internal_return instanceof Datepicker ||
			internal_return instanceof DateRangePicker
		)
			return this;

		if (this.length > 1)
			throw new Error('Using only allowed for the collection of a single element (' + option + ' function)');
		else
			return internal_return;
	};
	$.fn.datepicker = datepickerPlugin;

	var defaults = $.fn.datepicker.defaults = {
		assumeNearbyYear: false,
		autoclose: false,
		beforeShowDay: $.noop,
		beforeShowMonth: $.noop,
		beforeShowYear: $.noop,
		beforeShowDecade: $.noop,
		beforeShowCentury: $.noop,
		calendarWeeks: false,
		clearBtn: false,
		toggleActive: false,
		daysOfWeekDisabled: [],
		daysOfWeekHighlighted: [],
		datesDisabled: [],
		endDate: Infinity,
		forceParse: true,
		format: 'mm/dd/yyyy',
		keyboardNavigation: true,
		language: 'en',
		minViewMode: 0,
		maxViewMode: 4,
		multidate: false,
		multidateSeparator: ',',
		orientation: "auto",
		rtl: false,
		startDate: -Infinity,
		startView: 0,
		todayBtn: false,
		todayHighlight: false,
		weekStart: 0,
		disableTouchKeyboard: false,
		enableOnReadonly: true,
		showOnFocus: true,
		zIndexOffset: 10,
		container: 'body',
		immediateUpdates: false,
		title: '',
		templates: {
			leftArrow: '&laquo;',
			rightArrow: '&raquo;'
		}
	};
	var locale_opts = $.fn.datepicker.locale_opts = [
		'format',
		'rtl',
		'weekStart'
	];
	$.fn.datepicker.Constructor = Datepicker;
	var dates = $.fn.datepicker.dates = {
		en: {
			days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			daysMin: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
			months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			today: "Today",
			clear: "Clear",
			titleFormat: "MM yyyy"
		}
	};

	var DPGlobal = {
		modes: [
			{
				clsName: 'days',
				navFnc: 'Month',
				navStep: 1
			},
			{
				clsName: 'months',
				navFnc: 'FullYear',
				navStep: 1
			},
			{
				clsName: 'years',
				navFnc: 'FullYear',
				navStep: 10
			},
			{
				clsName: 'decades',
				navFnc: 'FullDecade',
				navStep: 100
			},
			{
				clsName: 'centuries',
				navFnc: 'FullCentury',
				navStep: 1000
		}],
		isLeapYear: function(year){
			return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
		},
		getDaysInMonth: function(year, month){
			return [31, (DPGlobal.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
		},
		validParts: /dd?|DD?|mm?|MM?|yy(?:yy)?/g,
		nonpunctuation: /[^ -\/:-@\u5e74\u6708\u65e5\[-`{-~\t\n\r]+/g,
		parseFormat: function(format){
			if (typeof format.toValue === 'function' && typeof format.toDisplay === 'function')
                return format;
            // IE treats \0 as a string end in inputs (truncating the value),
			// so it's a bad format delimiter, anyway
			var separators = format.replace(this.validParts, '\0').split('\0'),
				parts = format.match(this.validParts);
			if (!separators || !separators.length || !parts || parts.length === 0){
				throw new Error("Invalid date format.");
			}
			return {separators: separators, parts: parts};
		},
		parseDate: function(date, format, language, assumeNearby){
			if (!date)
				return undefined;
			if (date instanceof Date)
				return date;
			if (typeof format === 'string')
				format = DPGlobal.parseFormat(format);
			if (format.toValue)
                return format.toValue(date, format, language);
            var part_re = /([\-+]\d+)([dmwy])/,
				parts = date.match(/([\-+]\d+)([dmwy])/g),
				fn_map = {
					d: 'moveDay',
					m: 'moveMonth',
					w: 'moveWeek',
					y: 'moveYear'
				},
				dateAliases = {
					yesterday: '-1d',
					today: '+0d',
					tomorrow: '+1d'
				},
				part, dir, i, fn;
			if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(date)){
				date = new Date();
				for (i=0; i < parts.length; i++){
					part = part_re.exec(parts[i]);
					dir = parseInt(part[1]);
					fn = fn_map[part[2]];
					date = Datepicker.prototype[fn](date, dir);
				}
				return UTCDate(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate());
			}

			if (typeof dateAliases[date] !== 'undefined') {
				date = dateAliases[date];
				parts = date.match(/([\-+]\d+)([dmwy])/g);

				if (/^[\-+]\d+[dmwy]([\s,]+[\-+]\d+[dmwy])*$/.test(date)){
					date = new Date();
				  	for (i=0; i < parts.length; i++){
						part = part_re.exec(parts[i]);
						dir = parseInt(part[1]);
						fn = fn_map[part[2]];
						date = Datepicker.prototype[fn](date, dir);
				  	}

			  		return UTCDate(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate());
				}
			}

			parts = date && date.match(this.nonpunctuation) || [];
			date = new Date();

			function applyNearbyYear(year, threshold){
				if (threshold === true)
					threshold = 10;

				// if year is 2 digits or less, than the user most likely is trying to get a recent century
				if (year < 100){
					year += 2000;
					// if the new year is more than threshold years in advance, use last century
					if (year > ((new Date()).getFullYear()+threshold)){
						year -= 100;
					}
				}

				return year;
			}

			var parsed = {},
				setters_order = ['yyyy', 'yy', 'M', 'MM', 'm', 'mm', 'd', 'dd'],
				setters_map = {
					yyyy: function(d,v){
						return d.setUTCFullYear(assumeNearby ? applyNearbyYear(v, assumeNearby) : v);
					},
					yy: function(d,v){
						return d.setUTCFullYear(assumeNearby ? applyNearbyYear(v, assumeNearby) : v);
					},
					m: function(d,v){
						if (isNaN(d))
							return d;
						v -= 1;
						while (v < 0) v += 12;
						v %= 12;
						d.setUTCMonth(v);
						while (d.getUTCMonth() !== v)
							d.setUTCDate(d.getUTCDate()-1);
						return d;
					},
					d: function(d,v){
						return d.setUTCDate(v);
					}
				},
				val, filtered;
			setters_map['M'] = setters_map['MM'] = setters_map['mm'] = setters_map['m'];
			setters_map['dd'] = setters_map['d'];
			date = UTCToday();
			var fparts = format.parts.slice();
			// Remove noop parts
			if (parts.length !== fparts.length){
				fparts = $(fparts).filter(function(i,p){
					return $.inArray(p, setters_order) !== -1;
				}).toArray();
			}
			// Process remainder
			function match_part(){
				var m = this.slice(0, parts[i].length),
					p = parts[i].slice(0, m.length);
				return m.toLowerCase() === p.toLowerCase();
			}
			if (parts.length === fparts.length){
				var cnt;
				for (i=0, cnt = fparts.length; i < cnt; i++){
					val = parseInt(parts[i], 10);
					part = fparts[i];
					if (isNaN(val)){
						switch (part){
							case 'MM':
								filtered = $(dates[language].months).filter(match_part);
								val = $.inArray(filtered[0], dates[language].months) + 1;
								break;
							case 'M':
								filtered = $(dates[language].monthsShort).filter(match_part);
								val = $.inArray(filtered[0], dates[language].monthsShort) + 1;
								break;
						}
					}
					parsed[part] = val;
				}
				var _date, s;
				for (i=0; i < setters_order.length; i++){
					s = setters_order[i];
					if (s in parsed && !isNaN(parsed[s])){
						_date = new Date(date);
						setters_map[s](_date, parsed[s]);
						if (!isNaN(_date))
							date = _date;
					}
				}
			}
			return date;
		},
		formatDate: function(date, format, language){
			if (!date)
				return '';
			if (typeof format === 'string')
				format = DPGlobal.parseFormat(format);
			if (format.toDisplay)
                return format.toDisplay(date, format, language);
            var val = {
				d: date.getUTCDate(),
				D: dates[language].daysShort[date.getUTCDay()],
				DD: dates[language].days[date.getUTCDay()],
				m: date.getUTCMonth() + 1,
				M: dates[language].monthsShort[date.getUTCMonth()],
				MM: dates[language].months[date.getUTCMonth()],
				yy: date.getUTCFullYear().toString().substring(2),
				yyyy: date.getUTCFullYear()
			};
			val.dd = (val.d < 10 ? '0' : '') + val.d;
			val.mm = (val.m < 10 ? '0' : '') + val.m;
			date = [];
			var seps = $.extend([], format.separators);
			for (var i=0, cnt = format.parts.length; i <= cnt; i++){
				if (seps.length)
					date.push(seps.shift());
				date.push(val[format.parts[i]]);
			}
			return date.join('');
		},
		headTemplate: '<thead>'+
			              '<tr>'+
			                '<th colspan="7" class="datepicker-title"></th>'+
			              '</tr>'+
							'<tr>'+
								'<th class="prev"><span class="es-icon es-icon-previous"></span></th>'+
								'<th colspan="5" class="datepicker-switch"></th>'+
								'<th class="next"><span class="es-icon es-icon-next"></span></th>'+
							'</tr>'+
						'</thead>',
		contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>',
		footTemplate: '<tfoot>'+
							'<tr>'+
								'<th colspan="7" class="today"></th>'+
							'</tr>'+
							'<tr>'+
								'<th colspan="7" class="clear"></th>'+
							'</tr>'+
						'</tfoot>'
	};
	DPGlobal.template = '<div class="datepicker">'+
							'<div class="datepicker-days">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									'<tbody></tbody>'+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-months">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-years">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-decades">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
							'<div class="datepicker-centuries">'+
								'<table class="table-condensed">'+
									DPGlobal.headTemplate+
									DPGlobal.contTemplate+
									DPGlobal.footTemplate+
								'</table>'+
							'</div>'+
						'</div>';

	$.fn.datepicker.DPGlobal = DPGlobal;


	/* DATEPICKER NO CONFLICT
	* =================== */

	$.fn.datepicker.noConflict = function(){
		$.fn.datepicker = old;
		return this;
	};

	/* DATEPICKER VERSION
	 * =================== */
	$.fn.datepicker.version = '1.6.4';

	/* DATEPICKER DATA-API
	* ================== */

	$(document).on(
		'focus.datepicker.data-api click.datepicker.data-api',
		'[data-provide="datepicker"]',
		function(e){
			var $this = $(this);
			if ($this.data('datepicker'))
				return;
			e.preventDefault();
			// component click requires us to explicitly show it
			datepickerPlugin.call($this, 'show');
		}
	);
	$(function(){
		datepickerPlugin.call($('[data-provide="datepicker-inline"]'));
	});

}));

!function(a){a.fn.datepicker.dates.de={days:["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"],daysShort:["Son","Mon","Die","Mit","Don","Fre","Sam"],daysMin:["So","Mo","Di","Mi","Do","Fr","Sa"],months:["Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"],monthsShort:["Jan","Feb","Mär","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez"],today:"Heute",monthsTitle:"Monate",clear:"Löschen",weekStart:1,format:"dd.mm.yyyy"}}(jQuery);
!function(a){a.fn.datepicker.dates["en-GB"]={days:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],daysShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],daysMin:["Su","Mo","Tu","We","Th","Fr","Sa"],months:["January","February","March","April","May","June","July","August","September","October","November","December"],monthsShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],today:"Today",monthsTitle:"Months",clear:"Clear",weekStart:1,format:"dd/mm/yyyy"}}(jQuery);
// Fly-out / Dropdown Menu
(function($){
    $(function(){
        $('[data-es-dropdown] > button, [data-es-dropdown] > a').on('click', function(){
            var $p = $(this).parent(), o = $p.hasClass('-es-open');
            $('[data-es-dropdown]').removeClass('-es-open');
            !o && $p.addClass('-es-open');
        });
        $(document).on('click', function(e){
            ! $(e.target).closest('[data-es-dropdown] > button, [data-es-dropdown] > a').length &&
            $('[data-es-dropdown]').removeClass('-es-open');
        })
    });
})(jQuery);

(function (root, factory) {
    var document = this.document;
    if (typeof module === 'object' && module.exports) {
        module.exports = factory(document);
    } else {
        var ns = root.ert || (root.ert = {});
        ns.notifications = factory(document);
    }
}(this, function (document) {

    var body;
    var notificationsOptions = {
        containerClass: '',
        appearancePosition: 'right',
        appearanceWidth: ''
    };

    /**
     * Configure notification options.
     * appearancePosition: left, center or right
     * appearanceWidth: small
     * @return {void}
     */
    function configureNotifications(options) {
        if (!options) options = {};
        body = options.body || document.body;
        defaultMessageOptions.timeout = options.timeout || defaultMessageOptions.timeout;
        defaultMessageOptions.animation = options.animation || defaultMessageOptions.animation;
        notificationsOptions.appearancePosition = options.appearancePosition || notificationsOptions.appearancePosition;
        notificationsOptions.appearanceWidth = options.appearanceWidth || notificationsOptions.appearanceWidth;
        notificationsOptions.containerClass = options.containerClass || notificationsOptions.containerClass;
    }

    /**
     * Show notification message.
     * @param  {Object} options Options
     * @return {void}
     */
    function notificationMessage(options) {
        var message = new Message(options);
        var notificationsBarElement = createNotificationsBar(options);
        notificationsBarElement.insertBefore(message.element, notificationsBarElement.firstChild);
        if (message.type() !== 'danger') {
            setTimeout(function () {
                removeElement(message.element);
            }, message.option('timeout', 15000));
        }
        return message;
    }

    /**
     * Create or get existing notifications bar container.
     * @param  {Object} options Options for notifications bar:
     * appearancePosition: left, center or right
     * appearanceWidth: full or small
     * @return {HTMLElement} Notifications bar container element.
     */
    function createNotificationsBar(options) {
        var notificationsBarElement = body.querySelector('.es-notification-center');
        var appearancePosition = getValue(options, 'appearancePosition', notificationsOptions.appearancePosition);
        var appearanceWidth = getValue(options, 'appearanceWidth', notificationsOptions.appearanceWidth);
        if (notificationsBarElement
            && dataset(notificationsBarElement, 'position') === appearancePosition
            && dataset(notificationsBarElement, 'width') === appearanceWidth) {
            return notificationsBarElement;
        } else {
            if (notificationsBarElement) {
                body.removeChild(notificationsBarElement);
            }
        }
        var notificationBarTemplate = '<div class="es-notification-center"></div>';
        notificationsBarElement = createElement(notificationBarTemplate);

        dataset(notificationsBarElement, 'position', appearancePosition);
        addClass(notificationsBarElement, es(appearancePosition));

        dataset(notificationsBarElement, 'width', appearanceWidth);
        appearanceWidth && addClass(notificationsBarElement, es(appearanceWidth));

        if (notificationsOptions.containerClass) {
            addClass(notificationsBarElement, notificationsOptions.containerClass);
        }

        body.appendChild(notificationsBarElement);
        initializeEvents(notificationsBarElement);
        return notificationsBarElement;
    }

    /**
     * Initialize events delegation.
     * @param  {HTMLElement} container
     * @return {void}
     */
    function initializeEvents(container) {

        function traverse(node) {
            if (node.matches('.es-notification')) {
                // Messages should close when you click on them and danger messages are only closing if you dismiss them.
                if (dataset(node, 'type') !== 'danger') {
                    removeElement(node);
                }
            } else if (node.matches('.es-notification .es-close')) {
                removeElement(node.parentNode);
            } else {
                var parentNode = node.parentNode;
                if (parentNode) {
                    traverse(parentNode);
                }
            }
        }

        container.addEventListener('click', function (e) {
            traverse(e.target);
        });

    }

    var defaultMessageOptions = {
        animation: 'slide-in-right',
        priority: 'normal',
        timeout: 15000,
        type: ''
    };

    var typeIcons = {
        info: 'info',
        success: 'alert-success',
        warning: 'alert-warning',
        danger: 'alert-danger'
    };

    /**
     * Create message object.
     * @param {Object} options:
     * animation: slide-in-right or fade-in-down (default: slide-in-right)
     * priority: high, normal or low (default: normal)
     * type: success, info, warning or danger (default: none)
     */
    function Message(options) {
        this.options = options;
        this.element = createElement('<div class="es-notification"><span class="es-icon"></span><p></p><span class="es-icon es-icon-close es-close"></span></div>');
        this.textElement = this.element.querySelector('p');
        this.iconElement = this.element.querySelector('.es-icon');
        this.closeElement = this.element.querySelector('.close');

        this.text(this.option('text', ''));
        this.type(this.option('type'));
        this.icon(this.option('icon'));
        this.iconSize(this.option('iconSize'));
        this.priority(this.option('priority'));
        this.animation(this.option('animation'));

        dataset(this.element, 'type', this.type());
    }

    Message.prototype.option = function (name, defaultValue) {
        if (defaultValue === undefined) {
            defaultValue = defaultMessageOptions[name];
        }
        return getValue(this.options, name, defaultValue);
    };

    Message.prototype.text = function (text) {
        this.textElement.innerHTML = text;
    };

    Message.prototype.type = function (type) {
        if (type === undefined) {
            return this.typeValue;
        }
        this.typeValue = type;
        addClass(this.element, es(type));

        this.icon(typeIcons[type]);
    };

    Message.prototype.icon = function (icon) {
        if (icon) {
            this.iconElement.className = this.iconElement.className
                .replace(/es-icon-\S+/g, '')
                .replace(/\s+/g, ' ')
                .replace(/(^\s+|\s+$)/g, '');
            addClass(this.iconElement, 'es-icon-' + icon);
        }
    };

    Message.prototype.iconSize = function (size) {
        if (size) {
            addClass(this.iconElement, es(size));
        }
    };

    Message.prototype.priority = function (p) {
        addClass(this.element, es('priority-' + p));
    };

    Message.prototype.animation = function (animation) {
        addClass(this.element, '-es-animated ' + es(animation));
    };

    // ============================================================
    // Helpers.
    // ============================================================

    function createElement(html) {
        var template = document.createElement('template');
        template.innerHTML = html;
        if (template.content && template.content.firstChild) {
            return template.content.firstChild;
        }
        return template.firstChild;
    }

    function addClass(node, className) {
        node.className += ' ' + className;
    }

    function es(value) {
        return '-es-' + value;
    }

    function dataset(element, name, value) {
        var attrName = 'data-' + name;
        return (value === undefined) ? element.getAttribute(attrName) : element.setAttribute(attrName, value);
    }

    function removeElement(element) {
        var node = element && element.parentNode;
        if (node) {
            node.removeChild(element);
        }
    }

    function getValue(data, key, defaultValue, remove) {
        var result = defaultValue;
        if (typeof data[key] !== 'undefined') {
            result = data[key];
        }
        if (remove) {
            delete data[key];
        }
        return result;
    }

    return {
        message: notificationMessage,
        configure: configureNotifications
    };
}));

// https://developer.mozilla.org/en/docs/Web/API/Element/matches
if (!Element.prototype.matches) {
    Element.prototype.matches =
        Element.prototype.matchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector ||
        Element.prototype.oMatchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        function(s) {
            var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                i = matches.length;
            while (--i >= 0 && matches.item(i) !== this);
            return i > -1;
        };
}
// collapsible panels
(function($){
    $(function(){
        $('[data-toggle="panel"]').on('click', function(e){
            $(e.target).closest('.es-collapsible-panel').toggleClass('-es-open');
        });
    });
})(jQuery);

// Dashboard widget
(function ($) {
    $(function () {
        $('[data-es-expand-collapse]').on('click', function (e) {
            var $target = $(e.target),
                $widget = $target.closest('.es-widget'),
                $header = $widget.find('.es-widget-header'),
                $icon = $header.find('.es-icon'),
                $content = $widget.find('.es-widget-body');

            if ($widget.hasClass('-es-collapsed')) {
                $content.show();
            } else {
                $content.hide();
            }

            $widget.toggleClass('-es-collapsed');
            $icon.toggleClass('es-icon-chevron-right es-icon-chevron-down');
        });

        $(window).on('resize', close);
        $(document).on('click', close);

        function close() {
            var $widget = $(document).find('.es-widget'),
                $nav = $widget.find('nav');

            $nav.removeClass('-es-open');
        }
    });
})(jQuery);

// Tabs
(function ($) {
    $('.es-tab a').click(function(e){
        e.preventDefault();

        var $this = $(this);
        var $tabs = $this.parents('.es-tabs');
        var $tabPanels = $('#'+$this.parents('.es-tabs').data('panel-group'));
        var target = $this.attr('href');
        var $targetPanel = $(target);
        var $targetTab = $tabs.not('.-es-more').find('li a[href="'+target+'"]');
            
        // set embedded tab panels inactive
        $tabPanels.find('.es-panel').removeClass('-es-active');
        // set navigation elements inactive
        $tabs.find('li').removeClass('-es-active');

        // set target panel active 
        $targetPanel.addClass('-es-active');
        // mark target panel navigation element active
        $targetTab.parent().addClass('-es-active');
    });
})(jQuery);


// Date Picker popup
(function ($) {
    $(function () {
        $('.es-date-input > input').datepicker({
            autoclose: true,
            format: "dd-M-yyyy",
            language: "en",
            orientation: "bottom auto",
            todayBtn: "linked",
            todayHighlight: true
        }).on('click', function () {
            var $i = $(this);
            if ($i.hasClass('opened')) {
                console.log('Already opened.')
            }
            else {
                $i.addClass('opened');
            }
        });
    });
})(jQuery);

// File input
(function ($) {
    $(function () {
        $('.es-file-control .es-button').on('click', function (e) {
            $('.es-hidden-file-input input', $(this).parent().parent()).click();
        });
        $('.es-file-control .es-hidden-file-input input').on('change', function (e) {
            var files = $(this)[0].files,
                fileNames = files ? Array.prototype.map.call(files, function (file) {
                    return file.name;
                }).join(', ') : $(this).val().split(/(\\|\/)/g).pop();
            $('.es-file-name input', $(this).parent().parent()).val(fileNames);
        });
    });
})(jQuery);

// Time picker time function
(function ($) {
    $(function () {
        $('.es-time-input > button').on('click', function () {
            //get the current time
            var now = new Date(Date.now());
            var $time = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
            
            var $input = $(this).prev();
            $input.val($time);
        });
    });
})(jQuery);

// Grid sections
(function($){
    $(function(){
        $('.es-grid .es-actionbar > .es-checkbox > input').on('click', function (e) {
            $(e.target).closest('tr').toggleClass("-es-selected");
        });
    });
})(jQuery);

// Responsive Breadcrumbs
(function ($) {
    $(function () {
        var adjustForMobile = function () {
            $('.es-breadcrumbs').each(function () {
                var $this = $(this);
                var $moreElement = $this.find('.es-more');
                if (!$moreElement) {
                    return;
                }
                if ($moreElement.is(':visible') && $this.find("> nav > ul").children().length >= 3) {
                    var $elements = $this.find('> nav > ul').children().slice(1, -2);
                    $elements.each(function () {
                        var $navList = $moreElement.find('nav > ul');
                        if ($navList) {
                            $navList.append($(this));
                        }
                    });

                }
                else if (!$moreElement.is(':visible') && $moreElement.find("div > nav > ul").children().length > 0) {
                    var $elements = $moreElement.find("div > nav > ul").children();
                    $moreElement.after($elements);
                }
                else {

                }
            })
        };
        adjustForMobile();
        $(window).resize(adjustForMobile);
        $(window).on('orientationchange', adjustForMobile());
    });
})(jQuery);
// Modal Pop-up
(function ($) {
    $(function () {
        var $popup,
            focusableSelector = 'a[href], area[href], input:not([disabled]), button:not([disabled]), select:not([disabled]), textarea:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable=true]';

        function onClick(e) {
             ($(e.target).closest('[data-dismiss="modal"]').length) && close();
        }

        //let tab navigation cycle in the popup body
        function cycle(e) {
            var focusChanged = false;
            var focusable = [].slice.call($popup[0].querySelectorAll(focusableSelector));

            function isFocusInFirst() {
                return !!(focusable && focusable.length && (e.target || e.srcElement) === focusable[0]);
            }

            function isFocusInLast() {
                return !!(focusable && focusable.length && (e.target || e.srcElement) === focusable[focusable.length - 1]);
            }

            function isFocusInside() {
                return $.contains($popup[0], e.target || e.srcElement);
            }

            function focusFirst() {
                if (focusable && focusable.length) {
                    focusable[0].focus();
                    return true;
                }
                return false;
            }

            function focusLast() {
                if (focusable && focusable.length) {
                    focusable[focusable.length - 1].focus();
                    return true;
                }
                return false;
            }

            if (e.shiftKey) {
                if (!isFocusInside() || isFocusInFirst()) {
                    focusChanged = focusLast();
                }
            } else {
                if (!isFocusInside() || isFocusInLast()) {
                    focusChanged = focusFirst();
                }
            }
            if (focusChanged) {
                e.preventDefault();
                e.stopPropagation();
            }
        }

        function onKeyDown(e) {
            if (e.key) {
                switch (e.key) {
                    case 'Esc':
                    case 'Escape':
                        close();
                        break;
                    case 'Tab':
                        cycle(e);
                        break;
                }
            }
        }

        function initEvents() {
            $(document).on('keydown', onKeyDown);
            $popup.on('click', onClick);
        }

        function removeEvents() {
            $(document).off('keydown', onKeyDown);
            $popup.off('click', onclick);
        }

        function close() {
            $popup.removeClass('-es-open');
            $('html body').removeClass('-es-popup-opened');
            removeEvents();
        }

        function open() {
            $popup.addClass('-es-open');
            $('html body').addClass('-es-popup-opened');
            initEvents();
        }

        function isOpen() {
            return $popup.hasClass('.-es-open');
        }

        $(document).on('click', '[data-es-open-popup]', function (e) {
            var $p = $('#' + $(this).data('es-open-popup'));
            $popup = $p.length && $p;
            $popup && open();
        });
    });
})(jQuery);

// Sorting Table
(function ($) {
    $(function () {
        $('th.-es-sort, th.-es-sort-descending, th.-es-sort-ascending').click(function () {
           $(this).each(function(){
                var classes = ['-es-sort-descending','-es-sort-ascending','-es-sort'];
                this.className = classes[($.inArray(this.className, classes)+1)%classes.length];
            });
        });
    });
})(jQuery);

// Selectable Table
(function ($) {
    $(function () {
        $('.-es-selectable > tbody > tr').on('click', function (e) {

            if ( $(e.target).closest(".es-grid-body").length > 0 ) {

            }
            else {
                $($(this)).toggleClass("-es-selected");
            }
        });
    });
})(jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZlbmRvci9kYXRlLXBpY2tlci9kYXRlLXBpY2tlci5qcyIsInZlbmRvci9kYXRlLXBpY2tlci9kYXRlLXBpY2tlci1sb2NhbGVzL2RhdGUtcGlja2VyLmRlLm1pbi5qcyIsInZlbmRvci9kYXRlLXBpY2tlci9kYXRlLXBpY2tlci1sb2NhbGVzL2RhdGUtcGlja2VyLmVuLUdCLm1pbi5qcyIsInNyYy9jb21wb25lbnRzL2Jhc2ljcy9kcm9wZG93bi5qcyIsInNyYy9jb21wb25lbnRzL2Jhc2ljcy9ub3RpZmljYXRpb25zLmpzIiwic3JjL2NvbXBvbmVudHMvY29udGFpbmVycy9jb2xsYXBzaWJsZS1wYW5lbC5qcyIsInNyYy9jb21wb25lbnRzL2NvbnRhaW5lcnMvZGFzaGJvYXJkLXdpZGdldC5qcyIsInNyYy9jb21wb25lbnRzL2NvbnRhaW5lcnMvdGFicy5qcyIsInNyYy9jb21wb25lbnRzL2Zvcm0tY29udHJvbHMvZGF0ZS1waWNrZXIuanMiLCJzcmMvY29tcG9uZW50cy9mb3JtLWNvbnRyb2xzL2ZpbGUtaW5wdXQuanMiLCJzcmMvY29tcG9uZW50cy9mb3JtLWNvbnRyb2xzL3RpbWUtcGlja2VyLmpzIiwic3JjL2NvbXBvbmVudHMvZ3JpZC9ncmlkLmpzIiwic3JjL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9icmVhZGNydW1icy5qcyIsInNyYy9jb21wb25lbnRzL3BvcHVwL3BvcHVwLmpzIiwic3JjL2NvbXBvbmVudHMvdGFibGVzL3RhYmxlLXNvcnRpbmcuanMiLCJzcmMvY29tcG9uZW50cy90YWJsZXMvdGFibGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDaGpFQTtBQ0FBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDZEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUN4UUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQy9CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3hCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3JCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNiQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNSQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ2pDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQ3pHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNYQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZXJ0LXdlYnN0eWxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyohXG4gKiBEYXRlcGlja2VyIGZvciBCb290c3RyYXAgdjEuNi40IChodHRwczovL2dpdGh1Yi5jb20vZXRlcm5pY29kZS9ib290c3RyYXAtZGF0ZXBpY2tlcilcbiAqXG4gKiBDb3B5cmlnaHQgMjAxMiBTdGVmYW4gUGV0cmVcbiAqIEltcHJvdmVtZW50cyBieSBBbmRyZXcgUm93bHNcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSB2Mi4wIChodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjApXG4gKi8oZnVuY3Rpb24oZmFjdG9yeSl7XG4gICAgaWYgKHR5cGVvZiBkZWZpbmUgPT09IFwiZnVuY3Rpb25cIiAmJiBkZWZpbmUuYW1kKSB7XG4gICAgICAgIGRlZmluZShbXCJqcXVlcnlcIl0sIGZhY3RvcnkpO1xuICAgIH0gZWxzZSBpZiAodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKSB7XG4gICAgICAgIGZhY3RvcnkocmVxdWlyZSgnanF1ZXJ5JykpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGZhY3RvcnkoalF1ZXJ5KTtcbiAgICB9XG59KGZ1bmN0aW9uKCQsIHVuZGVmaW5lZCl7XG5cblx0ZnVuY3Rpb24gVVRDRGF0ZSgpe1xuXHRcdHJldHVybiBuZXcgRGF0ZShEYXRlLlVUQy5hcHBseShEYXRlLCBhcmd1bWVudHMpKTtcblx0fVxuXHRmdW5jdGlvbiBVVENUb2RheSgpe1xuXHRcdHZhciB0b2RheSA9IG5ldyBEYXRlKCk7XG5cdFx0cmV0dXJuIFVUQ0RhdGUodG9kYXkuZ2V0RnVsbFllYXIoKSwgdG9kYXkuZ2V0TW9udGgoKSwgdG9kYXkuZ2V0RGF0ZSgpKTtcblx0fVxuXHRmdW5jdGlvbiBpc1VUQ0VxdWFscyhkYXRlMSwgZGF0ZTIpIHtcblx0XHRyZXR1cm4gKFxuXHRcdFx0ZGF0ZTEuZ2V0VVRDRnVsbFllYXIoKSA9PT0gZGF0ZTIuZ2V0VVRDRnVsbFllYXIoKSAmJlxuXHRcdFx0ZGF0ZTEuZ2V0VVRDTW9udGgoKSA9PT0gZGF0ZTIuZ2V0VVRDTW9udGgoKSAmJlxuXHRcdFx0ZGF0ZTEuZ2V0VVRDRGF0ZSgpID09PSBkYXRlMi5nZXRVVENEYXRlKClcblx0XHQpO1xuXHR9XG5cdGZ1bmN0aW9uIGFsaWFzKG1ldGhvZCl7XG5cdFx0cmV0dXJuIGZ1bmN0aW9uKCl7XG5cdFx0XHRyZXR1cm4gdGhpc1ttZXRob2RdLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG5cdFx0fTtcblx0fVxuXHRmdW5jdGlvbiBpc1ZhbGlkRGF0ZShkKSB7XG5cdFx0cmV0dXJuIGQgJiYgIWlzTmFOKGQuZ2V0VGltZSgpKTtcblx0fVxuXG5cdHZhciBEYXRlQXJyYXkgPSAoZnVuY3Rpb24oKXtcblx0XHR2YXIgZXh0cmFzID0ge1xuXHRcdFx0Z2V0OiBmdW5jdGlvbihpKXtcblx0XHRcdFx0cmV0dXJuIHRoaXMuc2xpY2UoaSlbMF07XG5cdFx0XHR9LFxuXHRcdFx0Y29udGFpbnM6IGZ1bmN0aW9uKGQpe1xuXHRcdFx0XHQvLyBBcnJheS5pbmRleE9mIGlzIG5vdCBjcm9zcy1icm93c2VyO1xuXHRcdFx0XHQvLyAkLmluQXJyYXkgZG9lc24ndCB3b3JrIHdpdGggRGF0ZXNcblx0XHRcdFx0dmFyIHZhbCA9IGQgJiYgZC52YWx1ZU9mKCk7XG5cdFx0XHRcdGZvciAodmFyIGk9MCwgbD10aGlzLmxlbmd0aDsgaSA8IGw7IGkrKylcblx0XHRcdFx0XHRpZiAodGhpc1tpXS52YWx1ZU9mKCkgPT09IHZhbClcblx0XHRcdFx0XHRcdHJldHVybiBpO1xuXHRcdFx0XHRyZXR1cm4gLTE7XG5cdFx0XHR9LFxuXHRcdFx0cmVtb3ZlOiBmdW5jdGlvbihpKXtcblx0XHRcdFx0dGhpcy5zcGxpY2UoaSwxKTtcblx0XHRcdH0sXG5cdFx0XHRyZXBsYWNlOiBmdW5jdGlvbihuZXdfYXJyYXkpe1xuXHRcdFx0XHRpZiAoIW5ld19hcnJheSlcblx0XHRcdFx0XHRyZXR1cm47XG5cdFx0XHRcdGlmICghJC5pc0FycmF5KG5ld19hcnJheSkpXG5cdFx0XHRcdFx0bmV3X2FycmF5ID0gW25ld19hcnJheV07XG5cdFx0XHRcdHRoaXMuY2xlYXIoKTtcblx0XHRcdFx0dGhpcy5wdXNoLmFwcGx5KHRoaXMsIG5ld19hcnJheSk7XG5cdFx0XHR9LFxuXHRcdFx0Y2xlYXI6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdHRoaXMubGVuZ3RoID0gMDtcblx0XHRcdH0sXG5cdFx0XHRjb3B5OiBmdW5jdGlvbigpe1xuXHRcdFx0XHR2YXIgYSA9IG5ldyBEYXRlQXJyYXkoKTtcblx0XHRcdFx0YS5yZXBsYWNlKHRoaXMpO1xuXHRcdFx0XHRyZXR1cm4gYTtcblx0XHRcdH1cblx0XHR9O1xuXG5cdFx0cmV0dXJuIGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgYSA9IFtdO1xuXHRcdFx0YS5wdXNoLmFwcGx5KGEsIGFyZ3VtZW50cyk7XG5cdFx0XHQkLmV4dGVuZChhLCBleHRyYXMpO1xuXHRcdFx0cmV0dXJuIGE7XG5cdFx0fTtcblx0fSkoKTtcblxuXG5cdC8vIFBpY2tlciBvYmplY3RcblxuXHR2YXIgRGF0ZXBpY2tlciA9IGZ1bmN0aW9uKGVsZW1lbnQsIG9wdGlvbnMpe1xuXHRcdCQoZWxlbWVudCkuZGF0YSgnZGF0ZXBpY2tlcicsIHRoaXMpO1xuXHRcdHRoaXMuX3Byb2Nlc3Nfb3B0aW9ucyhvcHRpb25zKTtcblxuXHRcdHRoaXMuZGF0ZXMgPSBuZXcgRGF0ZUFycmF5KCk7XG5cdFx0dGhpcy52aWV3RGF0ZSA9IHRoaXMuby5kZWZhdWx0Vmlld0RhdGU7XG5cdFx0dGhpcy5mb2N1c0RhdGUgPSBudWxsO1xuXG5cdFx0dGhpcy5lbGVtZW50ID0gJChlbGVtZW50KTtcblx0XHR0aGlzLmlzSW5wdXQgPSB0aGlzLmVsZW1lbnQuaXMoJ2lucHV0Jyk7XG5cdFx0dGhpcy5pbnB1dEZpZWxkID0gdGhpcy5pc0lucHV0ID8gdGhpcy5lbGVtZW50IDogdGhpcy5lbGVtZW50LmZpbmQoJ2lucHV0Jyk7XG5cdFx0dGhpcy5jb21wb25lbnQgPSB0aGlzLmVsZW1lbnQuaGFzQ2xhc3MoJ2RhdGUnKSA/IHRoaXMuZWxlbWVudC5maW5kKCcuYWRkLW9uLCAuaW5wdXQtZ3JvdXAtYWRkb24sIC5idG4nKSA6IGZhbHNlO1xuXHRcdHRoaXMuaGFzSW5wdXQgPSB0aGlzLmNvbXBvbmVudCAmJiB0aGlzLmlucHV0RmllbGQubGVuZ3RoO1xuXHRcdGlmICh0aGlzLmNvbXBvbmVudCAmJiB0aGlzLmNvbXBvbmVudC5sZW5ndGggPT09IDApXG5cdFx0XHR0aGlzLmNvbXBvbmVudCA9IGZhbHNlO1xuXHRcdHRoaXMuaXNJbmxpbmUgPSAhdGhpcy5jb21wb25lbnQgJiYgdGhpcy5lbGVtZW50LmlzKCdkaXYnKTtcblxuXHRcdHRoaXMucGlja2VyID0gJChEUEdsb2JhbC50ZW1wbGF0ZSk7XG5cblx0XHQvLyBDaGVja2luZyB0ZW1wbGF0ZXMgYW5kIGluc2VydGluZ1xuXHRcdGlmICh0aGlzLl9jaGVja190ZW1wbGF0ZSh0aGlzLm8udGVtcGxhdGVzLmxlZnRBcnJvdykpIHtcblx0XHRcdHRoaXMucGlja2VyLmZpbmQoJy5wcmV2JykuaHRtbCh0aGlzLm8udGVtcGxhdGVzLmxlZnRBcnJvdyk7XG5cdFx0fVxuXHRcdGlmICh0aGlzLl9jaGVja190ZW1wbGF0ZSh0aGlzLm8udGVtcGxhdGVzLnJpZ2h0QXJyb3cpKSB7XG5cdFx0XHR0aGlzLnBpY2tlci5maW5kKCcubmV4dCcpLmh0bWwodGhpcy5vLnRlbXBsYXRlcy5yaWdodEFycm93KTtcblx0XHR9XG5cblx0XHR0aGlzLl9idWlsZEV2ZW50cygpO1xuXHRcdHRoaXMuX2F0dGFjaEV2ZW50cygpO1xuXG5cdFx0aWYgKHRoaXMuaXNJbmxpbmUpe1xuXHRcdFx0dGhpcy5waWNrZXIuYWRkQ2xhc3MoJ2RhdGVwaWNrZXItaW5saW5lJykuYXBwZW5kVG8odGhpcy5lbGVtZW50KTtcblx0XHR9XG5cdFx0ZWxzZSB7XG5cdFx0XHR0aGlzLnBpY2tlci5hZGRDbGFzcygnZGF0ZXBpY2tlci1kcm9wZG93biBkcm9wZG93bi1tZW51Jyk7XG5cdFx0fVxuXG5cdFx0aWYgKHRoaXMuby5ydGwpe1xuXHRcdFx0dGhpcy5waWNrZXIuYWRkQ2xhc3MoJ2RhdGVwaWNrZXItcnRsJyk7XG5cdFx0fVxuXG5cdFx0dGhpcy52aWV3TW9kZSA9IHRoaXMuby5zdGFydFZpZXc7XG5cblx0XHRpZiAodGhpcy5vLmNhbGVuZGFyV2Vla3MpXG5cdFx0XHR0aGlzLnBpY2tlci5maW5kKCd0aGVhZCAuZGF0ZXBpY2tlci10aXRsZSwgdGZvb3QgLnRvZGF5LCB0Zm9vdCAuY2xlYXInKVxuXHRcdFx0XHRcdFx0LmF0dHIoJ2NvbHNwYW4nLCBmdW5jdGlvbihpLCB2YWwpe1xuXHRcdFx0XHRcdFx0XHRyZXR1cm4gcGFyc2VJbnQodmFsKSArIDE7XG5cdFx0XHRcdFx0XHR9KTtcblxuXHRcdHRoaXMuX2FsbG93X3VwZGF0ZSA9IGZhbHNlO1xuXG5cdFx0dGhpcy5zZXRTdGFydERhdGUodGhpcy5fby5zdGFydERhdGUpO1xuXHRcdHRoaXMuc2V0RW5kRGF0ZSh0aGlzLl9vLmVuZERhdGUpO1xuXHRcdHRoaXMuc2V0RGF5c09mV2Vla0Rpc2FibGVkKHRoaXMuby5kYXlzT2ZXZWVrRGlzYWJsZWQpO1xuXHRcdHRoaXMuc2V0RGF5c09mV2Vla0hpZ2hsaWdodGVkKHRoaXMuby5kYXlzT2ZXZWVrSGlnaGxpZ2h0ZWQpO1xuXHRcdHRoaXMuc2V0RGF0ZXNEaXNhYmxlZCh0aGlzLm8uZGF0ZXNEaXNhYmxlZCk7XG5cblx0XHR0aGlzLmZpbGxEb3coKTtcblx0XHR0aGlzLmZpbGxNb250aHMoKTtcblxuXHRcdHRoaXMuX2FsbG93X3VwZGF0ZSA9IHRydWU7XG5cblx0XHR0aGlzLnVwZGF0ZSgpO1xuXHRcdHRoaXMuc2hvd01vZGUoKTtcblxuXHRcdGlmICh0aGlzLmlzSW5saW5lKXtcblx0XHRcdHRoaXMuc2hvdygpO1xuXHRcdH1cblx0fTtcblxuXHREYXRlcGlja2VyLnByb3RvdHlwZSA9IHtcblx0XHRjb25zdHJ1Y3RvcjogRGF0ZXBpY2tlcixcblxuXHRcdF9yZXNvbHZlVmlld05hbWU6IGZ1bmN0aW9uKHZpZXcsIGRlZmF1bHRfdmFsdWUpe1xuXHRcdFx0aWYgKHZpZXcgPT09IDAgfHwgdmlldyA9PT0gJ2RheXMnIHx8IHZpZXcgPT09ICdtb250aCcpIHtcblx0XHRcdFx0cmV0dXJuIDA7XG5cdFx0XHR9XG5cdFx0XHRpZiAodmlldyA9PT0gMSB8fCB2aWV3ID09PSAnbW9udGhzJyB8fCB2aWV3ID09PSAneWVhcicpIHtcblx0XHRcdFx0cmV0dXJuIDE7XG5cdFx0XHR9XG5cdFx0XHRpZiAodmlldyA9PT0gMiB8fCB2aWV3ID09PSAneWVhcnMnIHx8IHZpZXcgPT09ICdkZWNhZGUnKSB7XG5cdFx0XHRcdHJldHVybiAyO1xuXHRcdFx0fVxuXHRcdFx0aWYgKHZpZXcgPT09IDMgfHwgdmlldyA9PT0gJ2RlY2FkZXMnIHx8IHZpZXcgPT09ICdjZW50dXJ5Jykge1xuXHRcdFx0XHRyZXR1cm4gMztcblx0XHRcdH1cblx0XHRcdGlmICh2aWV3ID09PSA0IHx8IHZpZXcgPT09ICdjZW50dXJpZXMnIHx8IHZpZXcgPT09ICdtaWxsZW5uaXVtJykge1xuXHRcdFx0XHRyZXR1cm4gNDtcblx0XHRcdH1cblx0XHRcdHJldHVybiBkZWZhdWx0X3ZhbHVlID09PSB1bmRlZmluZWQgPyBmYWxzZSA6IGRlZmF1bHRfdmFsdWU7XG5cdFx0fSxcblxuXHRcdF9jaGVja190ZW1wbGF0ZTogZnVuY3Rpb24odG1wKXtcblx0XHRcdHRyeSB7XG5cdFx0XHRcdC8vIElmIGVtcHR5XG5cdFx0XHRcdGlmICh0bXAgPT09IHVuZGVmaW5lZCB8fCB0bXAgPT09IFwiXCIpIHtcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdH1cblx0XHRcdFx0Ly8gSWYgbm8gaHRtbCwgZXZlcnl0aGluZyBva1xuXHRcdFx0XHRpZiAoKHRtcC5tYXRjaCgvWzw+XS9nKSB8fCBbXSkubGVuZ3RoIDw9IDApIHtcblx0XHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdFx0fVxuXHRcdFx0XHQvLyBDaGVja2luZyBpZiBodG1sIGlzIGZpbmVcblx0XHRcdFx0dmFyIGpEb20gPSAkKHRtcCk7XG5cdFx0XHRcdHJldHVybiBqRG9tLmxlbmd0aCA+IDA7XG5cdFx0XHR9XG5cdFx0XHRjYXRjaCAoZXgpIHtcblx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0fVxuXHRcdH0sXG5cblx0XHRfcHJvY2Vzc19vcHRpb25zOiBmdW5jdGlvbihvcHRzKXtcblx0XHRcdC8vIFN0b3JlIHJhdyBvcHRpb25zIGZvciByZWZlcmVuY2Vcblx0XHRcdHRoaXMuX28gPSAkLmV4dGVuZCh7fSwgdGhpcy5fbywgb3B0cyk7XG5cdFx0XHQvLyBQcm9jZXNzZWQgb3B0aW9uc1xuXHRcdFx0dmFyIG8gPSB0aGlzLm8gPSAkLmV4dGVuZCh7fSwgdGhpcy5fbyk7XG5cblx0XHRcdC8vIENoZWNrIGlmIFwiZGUtREVcIiBzdHlsZSBkYXRlIGlzIGF2YWlsYWJsZSwgaWYgbm90IGxhbmd1YWdlIHNob3VsZFxuXHRcdFx0Ly8gZmFsbGJhY2sgdG8gMiBsZXR0ZXIgY29kZSBlZyBcImRlXCJcblx0XHRcdHZhciBsYW5nID0gby5sYW5ndWFnZTtcblx0XHRcdGlmICghZGF0ZXNbbGFuZ10pe1xuXHRcdFx0XHRsYW5nID0gbGFuZy5zcGxpdCgnLScpWzBdO1xuXHRcdFx0XHRpZiAoIWRhdGVzW2xhbmddKVxuXHRcdFx0XHRcdGxhbmcgPSBkZWZhdWx0cy5sYW5ndWFnZTtcblx0XHRcdH1cblx0XHRcdG8ubGFuZ3VhZ2UgPSBsYW5nO1xuXG5cdFx0XHQvLyBSZXRyaWV2ZSB2aWV3IGluZGV4IGZyb20gYW55IGFsaWFzZXNcblx0XHRcdG8uc3RhcnRWaWV3ID0gdGhpcy5fcmVzb2x2ZVZpZXdOYW1lKG8uc3RhcnRWaWV3LCAwKTtcblx0XHRcdG8ubWluVmlld01vZGUgPSB0aGlzLl9yZXNvbHZlVmlld05hbWUoby5taW5WaWV3TW9kZSwgMCk7XG5cdFx0XHRvLm1heFZpZXdNb2RlID0gdGhpcy5fcmVzb2x2ZVZpZXdOYW1lKG8ubWF4Vmlld01vZGUsIDQpO1xuXG5cdFx0XHQvLyBDaGVjayB0aGF0IHRoZSBzdGFydCB2aWV3IGlzIGJldHdlZW4gbWluIGFuZCBtYXhcblx0XHRcdG8uc3RhcnRWaWV3ID0gTWF0aC5taW4oby5zdGFydFZpZXcsIG8ubWF4Vmlld01vZGUpO1xuXHRcdFx0by5zdGFydFZpZXcgPSBNYXRoLm1heChvLnN0YXJ0Vmlldywgby5taW5WaWV3TW9kZSk7XG5cblx0XHRcdC8vIHRydWUsIGZhbHNlLCBvciBOdW1iZXIgPiAwXG5cdFx0XHRpZiAoby5tdWx0aWRhdGUgIT09IHRydWUpe1xuXHRcdFx0XHRvLm11bHRpZGF0ZSA9IE51bWJlcihvLm11bHRpZGF0ZSkgfHwgZmFsc2U7XG5cdFx0XHRcdGlmIChvLm11bHRpZGF0ZSAhPT0gZmFsc2UpXG5cdFx0XHRcdFx0by5tdWx0aWRhdGUgPSBNYXRoLm1heCgwLCBvLm11bHRpZGF0ZSk7XG5cdFx0XHR9XG5cdFx0XHRvLm11bHRpZGF0ZVNlcGFyYXRvciA9IFN0cmluZyhvLm11bHRpZGF0ZVNlcGFyYXRvcik7XG5cblx0XHRcdG8ud2Vla1N0YXJ0ICU9IDc7XG5cdFx0XHRvLndlZWtFbmQgPSAoby53ZWVrU3RhcnQgKyA2KSAlIDc7XG5cblx0XHRcdHZhciBmb3JtYXQgPSBEUEdsb2JhbC5wYXJzZUZvcm1hdChvLmZvcm1hdCk7XG5cdFx0XHRpZiAoby5zdGFydERhdGUgIT09IC1JbmZpbml0eSl7XG5cdFx0XHRcdGlmICghIW8uc3RhcnREYXRlKXtcblx0XHRcdFx0XHRpZiAoby5zdGFydERhdGUgaW5zdGFuY2VvZiBEYXRlKVxuXHRcdFx0XHRcdFx0by5zdGFydERhdGUgPSB0aGlzLl9sb2NhbF90b191dGModGhpcy5femVyb190aW1lKG8uc3RhcnREYXRlKSk7XG5cdFx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdFx0by5zdGFydERhdGUgPSBEUEdsb2JhbC5wYXJzZURhdGUoby5zdGFydERhdGUsIGZvcm1hdCwgby5sYW5ndWFnZSwgby5hc3N1bWVOZWFyYnlZZWFyKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRvLnN0YXJ0RGF0ZSA9IC1JbmZpbml0eTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0aWYgKG8uZW5kRGF0ZSAhPT0gSW5maW5pdHkpe1xuXHRcdFx0XHRpZiAoISFvLmVuZERhdGUpe1xuXHRcdFx0XHRcdGlmIChvLmVuZERhdGUgaW5zdGFuY2VvZiBEYXRlKVxuXHRcdFx0XHRcdFx0by5lbmREYXRlID0gdGhpcy5fbG9jYWxfdG9fdXRjKHRoaXMuX3plcm9fdGltZShvLmVuZERhdGUpKTtcblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHRvLmVuZERhdGUgPSBEUEdsb2JhbC5wYXJzZURhdGUoby5lbmREYXRlLCBmb3JtYXQsIG8ubGFuZ3VhZ2UsIG8uYXNzdW1lTmVhcmJ5WWVhcik7XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0by5lbmREYXRlID0gSW5maW5pdHk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0by5kYXlzT2ZXZWVrRGlzYWJsZWQgPSBvLmRheXNPZldlZWtEaXNhYmxlZHx8W107XG5cdFx0XHRpZiAoISQuaXNBcnJheShvLmRheXNPZldlZWtEaXNhYmxlZCkpXG5cdFx0XHRcdG8uZGF5c09mV2Vla0Rpc2FibGVkID0gby5kYXlzT2ZXZWVrRGlzYWJsZWQuc3BsaXQoL1ssXFxzXSovKTtcblx0XHRcdG8uZGF5c09mV2Vla0Rpc2FibGVkID0gJC5tYXAoby5kYXlzT2ZXZWVrRGlzYWJsZWQsIGZ1bmN0aW9uKGQpe1xuXHRcdFx0XHRyZXR1cm4gcGFyc2VJbnQoZCwgMTApO1xuXHRcdFx0fSk7XG5cblx0XHRcdG8uZGF5c09mV2Vla0hpZ2hsaWdodGVkID0gby5kYXlzT2ZXZWVrSGlnaGxpZ2h0ZWR8fFtdO1xuXHRcdFx0aWYgKCEkLmlzQXJyYXkoby5kYXlzT2ZXZWVrSGlnaGxpZ2h0ZWQpKVxuXHRcdFx0XHRvLmRheXNPZldlZWtIaWdobGlnaHRlZCA9IG8uZGF5c09mV2Vla0hpZ2hsaWdodGVkLnNwbGl0KC9bLFxcc10qLyk7XG5cdFx0XHRvLmRheXNPZldlZWtIaWdobGlnaHRlZCA9ICQubWFwKG8uZGF5c09mV2Vla0hpZ2hsaWdodGVkLCBmdW5jdGlvbihkKXtcblx0XHRcdFx0cmV0dXJuIHBhcnNlSW50KGQsIDEwKTtcblx0XHRcdH0pO1xuXG5cdFx0XHRvLmRhdGVzRGlzYWJsZWQgPSBvLmRhdGVzRGlzYWJsZWR8fFtdO1xuXHRcdFx0aWYgKCEkLmlzQXJyYXkoby5kYXRlc0Rpc2FibGVkKSkge1xuXHRcdFx0XHRvLmRhdGVzRGlzYWJsZWQgPSBbXG5cdFx0XHRcdFx0by5kYXRlc0Rpc2FibGVkXG5cdFx0XHRcdF07XG5cdFx0XHR9XG5cdFx0XHRvLmRhdGVzRGlzYWJsZWQgPSAkLm1hcChvLmRhdGVzRGlzYWJsZWQsZnVuY3Rpb24oZCl7XG5cdFx0XHRcdHJldHVybiBEUEdsb2JhbC5wYXJzZURhdGUoZCwgZm9ybWF0LCBvLmxhbmd1YWdlLCBvLmFzc3VtZU5lYXJieVllYXIpO1xuXHRcdFx0fSk7XG5cblx0XHRcdHZhciBwbGMgPSBTdHJpbmcoby5vcmllbnRhdGlvbikudG9Mb3dlckNhc2UoKS5zcGxpdCgvXFxzKy9nKSxcblx0XHRcdFx0X3BsYyA9IG8ub3JpZW50YXRpb24udG9Mb3dlckNhc2UoKTtcblx0XHRcdHBsYyA9ICQuZ3JlcChwbGMsIGZ1bmN0aW9uKHdvcmQpe1xuXHRcdFx0XHRyZXR1cm4gL15hdXRvfGxlZnR8cmlnaHR8dG9wfGJvdHRvbSQvLnRlc3Qod29yZCk7XG5cdFx0XHR9KTtcblx0XHRcdG8ub3JpZW50YXRpb24gPSB7eDogJ2F1dG8nLCB5OiAnYXV0byd9O1xuXHRcdFx0aWYgKCFfcGxjIHx8IF9wbGMgPT09ICdhdXRvJylcblx0XHRcdFx0OyAvLyBubyBhY3Rpb25cblx0XHRcdGVsc2UgaWYgKHBsYy5sZW5ndGggPT09IDEpe1xuXHRcdFx0XHRzd2l0Y2ggKHBsY1swXSl7XG5cdFx0XHRcdFx0Y2FzZSAndG9wJzpcblx0XHRcdFx0XHRjYXNlICdib3R0b20nOlxuXHRcdFx0XHRcdFx0by5vcmllbnRhdGlvbi55ID0gcGxjWzBdO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0Y2FzZSAnbGVmdCc6XG5cdFx0XHRcdFx0Y2FzZSAncmlnaHQnOlxuXHRcdFx0XHRcdFx0by5vcmllbnRhdGlvbi54ID0gcGxjWzBdO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHRcdGVsc2Uge1xuXHRcdFx0XHRfcGxjID0gJC5ncmVwKHBsYywgZnVuY3Rpb24od29yZCl7XG5cdFx0XHRcdFx0cmV0dXJuIC9ebGVmdHxyaWdodCQvLnRlc3Qod29yZCk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRvLm9yaWVudGF0aW9uLnggPSBfcGxjWzBdIHx8ICdhdXRvJztcblxuXHRcdFx0XHRfcGxjID0gJC5ncmVwKHBsYywgZnVuY3Rpb24od29yZCl7XG5cdFx0XHRcdFx0cmV0dXJuIC9edG9wfGJvdHRvbSQvLnRlc3Qod29yZCk7XG5cdFx0XHRcdH0pO1xuXHRcdFx0XHRvLm9yaWVudGF0aW9uLnkgPSBfcGxjWzBdIHx8ICdhdXRvJztcblx0XHRcdH1cblx0XHRcdGlmIChvLmRlZmF1bHRWaWV3RGF0ZSkge1xuXHRcdFx0XHR2YXIgeWVhciA9IG8uZGVmYXVsdFZpZXdEYXRlLnllYXIgfHwgbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpO1xuXHRcdFx0XHR2YXIgbW9udGggPSBvLmRlZmF1bHRWaWV3RGF0ZS5tb250aCB8fCAwO1xuXHRcdFx0XHR2YXIgZGF5ID0gby5kZWZhdWx0Vmlld0RhdGUuZGF5IHx8IDE7XG5cdFx0XHRcdG8uZGVmYXVsdFZpZXdEYXRlID0gVVRDRGF0ZSh5ZWFyLCBtb250aCwgZGF5KTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdG8uZGVmYXVsdFZpZXdEYXRlID0gVVRDVG9kYXkoKTtcblx0XHRcdH1cblx0XHR9LFxuXHRcdF9ldmVudHM6IFtdLFxuXHRcdF9zZWNvbmRhcnlFdmVudHM6IFtdLFxuXHRcdF9hcHBseUV2ZW50czogZnVuY3Rpb24oZXZzKXtcblx0XHRcdGZvciAodmFyIGk9MCwgZWwsIGNoLCBldjsgaSA8IGV2cy5sZW5ndGg7IGkrKyl7XG5cdFx0XHRcdGVsID0gZXZzW2ldWzBdO1xuXHRcdFx0XHRpZiAoZXZzW2ldLmxlbmd0aCA9PT0gMil7XG5cdFx0XHRcdFx0Y2ggPSB1bmRlZmluZWQ7XG5cdFx0XHRcdFx0ZXYgPSBldnNbaV1bMV07XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWxzZSBpZiAoZXZzW2ldLmxlbmd0aCA9PT0gMyl7XG5cdFx0XHRcdFx0Y2ggPSBldnNbaV1bMV07XG5cdFx0XHRcdFx0ZXYgPSBldnNbaV1bMl07XG5cdFx0XHRcdH1cblx0XHRcdFx0ZWwub24oZXYsIGNoKTtcblx0XHRcdH1cblx0XHR9LFxuXHRcdF91bmFwcGx5RXZlbnRzOiBmdW5jdGlvbihldnMpe1xuXHRcdFx0Zm9yICh2YXIgaT0wLCBlbCwgZXYsIGNoOyBpIDwgZXZzLmxlbmd0aDsgaSsrKXtcblx0XHRcdFx0ZWwgPSBldnNbaV1bMF07XG5cdFx0XHRcdGlmIChldnNbaV0ubGVuZ3RoID09PSAyKXtcblx0XHRcdFx0XHRjaCA9IHVuZGVmaW5lZDtcblx0XHRcdFx0XHRldiA9IGV2c1tpXVsxXTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIGlmIChldnNbaV0ubGVuZ3RoID09PSAzKXtcblx0XHRcdFx0XHRjaCA9IGV2c1tpXVsxXTtcblx0XHRcdFx0XHRldiA9IGV2c1tpXVsyXTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbC5vZmYoZXYsIGNoKTtcblx0XHRcdH1cblx0XHR9LFxuXHRcdF9idWlsZEV2ZW50czogZnVuY3Rpb24oKXtcbiAgICAgICAgICAgIHZhciBldmVudHMgPSB7XG4gICAgICAgICAgICAgICAga2V5dXA6ICQucHJveHkoZnVuY3Rpb24oZSl7XG4gICAgICAgICAgICAgICAgICAgIGlmICgkLmluQXJyYXkoZS5rZXlDb2RlLCBbMjcsIDM3LCAzOSwgMzgsIDQwLCAzMiwgMTMsIDldKSA9PT0gLTEpXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZSgpO1xuICAgICAgICAgICAgICAgIH0sIHRoaXMpLFxuICAgICAgICAgICAgICAgIGtleWRvd246ICQucHJveHkodGhpcy5rZXlkb3duLCB0aGlzKSxcbiAgICAgICAgICAgICAgICBwYXN0ZTogJC5wcm94eSh0aGlzLnBhc3RlLCB0aGlzKVxuICAgICAgICAgICAgfTtcblxuICAgICAgICAgICAgaWYgKHRoaXMuby5zaG93T25Gb2N1cyA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgICAgIGV2ZW50cy5mb2N1cyA9ICQucHJveHkodGhpcy5zaG93LCB0aGlzKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuaXNJbnB1dCkgeyAvLyBzaW5nbGUgaW5wdXRcbiAgICAgICAgICAgICAgICB0aGlzLl9ldmVudHMgPSBbXG4gICAgICAgICAgICAgICAgICAgIFt0aGlzLmVsZW1lbnQsIGV2ZW50c11cbiAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSBpZiAodGhpcy5jb21wb25lbnQgJiYgdGhpcy5oYXNJbnB1dCkgeyAvLyBjb21wb25lbnQ6IGlucHV0ICsgYnV0dG9uXG4gICAgICAgICAgICAgICAgdGhpcy5fZXZlbnRzID0gW1xuICAgICAgICAgICAgICAgICAgICAvLyBGb3IgY29tcG9uZW50cyB0aGF0IGFyZSBub3QgcmVhZG9ubHksIGFsbG93IGtleWJvYXJkIG5hdlxuICAgICAgICAgICAgICAgICAgICBbdGhpcy5pbnB1dEZpZWxkLCBldmVudHNdLFxuICAgICAgICAgICAgICAgICAgICBbdGhpcy5jb21wb25lbnQsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsaWNrOiAkLnByb3h5KHRoaXMuc2hvdywgdGhpcylcbiAgICAgICAgICAgICAgICAgICAgfV1cbiAgICAgICAgICAgICAgICBdO1xuICAgICAgICAgICAgfVxuXHRcdFx0ZWxzZSB7XG5cdFx0XHRcdHRoaXMuX2V2ZW50cyA9IFtcblx0XHRcdFx0XHRbdGhpcy5lbGVtZW50LCB7XG5cdFx0XHRcdFx0XHRjbGljazogJC5wcm94eSh0aGlzLnNob3csIHRoaXMpLFxuXHRcdFx0XHRcdFx0a2V5ZG93bjogJC5wcm94eSh0aGlzLmtleWRvd24sIHRoaXMpXG5cdFx0XHRcdFx0fV1cblx0XHRcdFx0XTtcblx0XHRcdH1cblx0XHRcdHRoaXMuX2V2ZW50cy5wdXNoKFxuXHRcdFx0XHQvLyBDb21wb25lbnQ6IGxpc3RlbiBmb3IgYmx1ciBvbiBlbGVtZW50IGRlc2NlbmRhbnRzXG5cdFx0XHRcdFt0aGlzLmVsZW1lbnQsICcqJywge1xuXHRcdFx0XHRcdGJsdXI6ICQucHJveHkoZnVuY3Rpb24oZSl7XG5cdFx0XHRcdFx0XHR0aGlzLl9mb2N1c2VkX2Zyb20gPSBlLnRhcmdldDtcblx0XHRcdFx0XHR9LCB0aGlzKVxuXHRcdFx0XHR9XSxcblx0XHRcdFx0Ly8gSW5wdXQ6IGxpc3RlbiBmb3IgYmx1ciBvbiBlbGVtZW50XG5cdFx0XHRcdFt0aGlzLmVsZW1lbnQsIHtcblx0XHRcdFx0XHRibHVyOiAkLnByb3h5KGZ1bmN0aW9uKGUpe1xuXHRcdFx0XHRcdFx0dGhpcy5fZm9jdXNlZF9mcm9tID0gZS50YXJnZXQ7XG5cdFx0XHRcdFx0fSwgdGhpcylcblx0XHRcdFx0fV1cblx0XHRcdCk7XG5cblx0XHRcdGlmICh0aGlzLm8uaW1tZWRpYXRlVXBkYXRlcykge1xuXHRcdFx0XHQvLyBUcmlnZ2VyIGlucHV0IHVwZGF0ZXMgaW1tZWRpYXRlbHkgb24gY2hhbmdlZCB5ZWFyL21vbnRoXG5cdFx0XHRcdHRoaXMuX2V2ZW50cy5wdXNoKFt0aGlzLmVsZW1lbnQsIHtcblx0XHRcdFx0XHQnY2hhbmdlWWVhciBjaGFuZ2VNb250aCc6ICQucHJveHkoZnVuY3Rpb24oZSl7XG5cdFx0XHRcdFx0XHR0aGlzLnVwZGF0ZShlLmRhdGUpO1xuXHRcdFx0XHRcdH0sIHRoaXMpXG5cdFx0XHRcdH1dKTtcblx0XHRcdH1cblxuXHRcdFx0dGhpcy5fc2Vjb25kYXJ5RXZlbnRzID0gW1xuXHRcdFx0XHRbdGhpcy5waWNrZXIsIHtcblx0XHRcdFx0XHRjbGljazogJC5wcm94eSh0aGlzLmNsaWNrLCB0aGlzKVxuXHRcdFx0XHR9XSxcblx0XHRcdFx0WyQod2luZG93KSwge1xuXHRcdFx0XHRcdHJlc2l6ZTogJC5wcm94eSh0aGlzLnBsYWNlLCB0aGlzKVxuXHRcdFx0XHR9XSxcblx0XHRcdFx0WyQoZG9jdW1lbnQpLCB7XG5cdFx0XHRcdFx0bW91c2Vkb3duOiAkLnByb3h5KGZ1bmN0aW9uKGUpe1xuXHRcdFx0XHRcdFx0Ly8gQ2xpY2tlZCBvdXRzaWRlIHRoZSBkYXRlcGlja2VyLCBoaWRlIGl0XG5cdFx0XHRcdFx0XHRpZiAoIShcblx0XHRcdFx0XHRcdFx0dGhpcy5lbGVtZW50LmlzKGUudGFyZ2V0KSB8fFxuXHRcdFx0XHRcdFx0XHR0aGlzLmVsZW1lbnQuZmluZChlLnRhcmdldCkubGVuZ3RoIHx8XG5cdFx0XHRcdFx0XHRcdHRoaXMucGlja2VyLmlzKGUudGFyZ2V0KSB8fFxuXHRcdFx0XHRcdFx0XHR0aGlzLnBpY2tlci5maW5kKGUudGFyZ2V0KS5sZW5ndGggfHxcblx0XHRcdFx0XHRcdFx0dGhpcy5pc0lubGluZVxuXHRcdFx0XHRcdFx0KSl7XG5cdFx0XHRcdFx0XHRcdHRoaXMuaGlkZSgpO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH0sIHRoaXMpXG5cdFx0XHRcdH1dXG5cdFx0XHRdO1xuXHRcdH0sXG5cdFx0X2F0dGFjaEV2ZW50czogZnVuY3Rpb24oKXtcblx0XHRcdHRoaXMuX2RldGFjaEV2ZW50cygpO1xuXHRcdFx0dGhpcy5fYXBwbHlFdmVudHModGhpcy5fZXZlbnRzKTtcblx0XHR9LFxuXHRcdF9kZXRhY2hFdmVudHM6IGZ1bmN0aW9uKCl7XG5cdFx0XHR0aGlzLl91bmFwcGx5RXZlbnRzKHRoaXMuX2V2ZW50cyk7XG5cdFx0fSxcblx0XHRfYXR0YWNoU2Vjb25kYXJ5RXZlbnRzOiBmdW5jdGlvbigpe1xuXHRcdFx0dGhpcy5fZGV0YWNoU2Vjb25kYXJ5RXZlbnRzKCk7XG5cdFx0XHR0aGlzLl9hcHBseUV2ZW50cyh0aGlzLl9zZWNvbmRhcnlFdmVudHMpO1xuXHRcdH0sXG5cdFx0X2RldGFjaFNlY29uZGFyeUV2ZW50czogZnVuY3Rpb24oKXtcblx0XHRcdHRoaXMuX3VuYXBwbHlFdmVudHModGhpcy5fc2Vjb25kYXJ5RXZlbnRzKTtcblx0XHR9LFxuXHRcdF90cmlnZ2VyOiBmdW5jdGlvbihldmVudCwgYWx0ZGF0ZSl7XG5cdFx0XHR2YXIgZGF0ZSA9IGFsdGRhdGUgfHwgdGhpcy5kYXRlcy5nZXQoLTEpLFxuXHRcdFx0XHRsb2NhbF9kYXRlID0gdGhpcy5fdXRjX3RvX2xvY2FsKGRhdGUpO1xuXG5cdFx0XHR0aGlzLmVsZW1lbnQudHJpZ2dlcih7XG5cdFx0XHRcdHR5cGU6IGV2ZW50LFxuXHRcdFx0XHRkYXRlOiBsb2NhbF9kYXRlLFxuXHRcdFx0XHRkYXRlczogJC5tYXAodGhpcy5kYXRlcywgdGhpcy5fdXRjX3RvX2xvY2FsKSxcblx0XHRcdFx0Zm9ybWF0OiAkLnByb3h5KGZ1bmN0aW9uKGl4LCBmb3JtYXQpe1xuXHRcdFx0XHRcdGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKXtcblx0XHRcdFx0XHRcdGl4ID0gdGhpcy5kYXRlcy5sZW5ndGggLSAxO1xuXHRcdFx0XHRcdFx0Zm9ybWF0ID0gdGhpcy5vLmZvcm1hdDtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSBpZiAodHlwZW9mIGl4ID09PSAnc3RyaW5nJyl7XG5cdFx0XHRcdFx0XHRmb3JtYXQgPSBpeDtcblx0XHRcdFx0XHRcdGl4ID0gdGhpcy5kYXRlcy5sZW5ndGggLSAxO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRmb3JtYXQgPSBmb3JtYXQgfHwgdGhpcy5vLmZvcm1hdDtcblx0XHRcdFx0XHR2YXIgZGF0ZSA9IHRoaXMuZGF0ZXMuZ2V0KGl4KTtcblx0XHRcdFx0XHRyZXR1cm4gRFBHbG9iYWwuZm9ybWF0RGF0ZShkYXRlLCBmb3JtYXQsIHRoaXMuby5sYW5ndWFnZSk7XG5cdFx0XHRcdH0sIHRoaXMpXG5cdFx0XHR9KTtcblx0XHR9LFxuXG5cdFx0c2hvdzogZnVuY3Rpb24oKXtcblx0XHRcdGlmICh0aGlzLmlucHV0RmllbGQucHJvcCgnZGlzYWJsZWQnKSB8fCAodGhpcy5pbnB1dEZpZWxkLnByb3AoJ3JlYWRvbmx5JykgJiYgdGhpcy5vLmVuYWJsZU9uUmVhZG9ubHkgPT09IGZhbHNlKSlcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0aWYgKCF0aGlzLmlzSW5saW5lKVxuXHRcdFx0XHR0aGlzLnBpY2tlci5hcHBlbmRUbyh0aGlzLm8uY29udGFpbmVyKTtcblx0XHRcdHRoaXMucGxhY2UoKTtcblx0XHRcdHRoaXMucGlja2VyLnNob3coKTtcblx0XHRcdHRoaXMuX2F0dGFjaFNlY29uZGFyeUV2ZW50cygpO1xuXHRcdFx0dGhpcy5fdHJpZ2dlcignc2hvdycpO1xuXHRcdFx0aWYgKCh3aW5kb3cubmF2aWdhdG9yLm1zTWF4VG91Y2hQb2ludHMgfHwgJ29udG91Y2hzdGFydCcgaW4gZG9jdW1lbnQpICYmIHRoaXMuby5kaXNhYmxlVG91Y2hLZXlib2FyZCkge1xuXHRcdFx0XHQkKHRoaXMuZWxlbWVudCkuYmx1cigpO1xuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fSxcblxuXHRcdGhpZGU6IGZ1bmN0aW9uKCl7XG5cdFx0XHRpZiAodGhpcy5pc0lubGluZSB8fCAhdGhpcy5waWNrZXIuaXMoJzp2aXNpYmxlJykpXG5cdFx0XHRcdHJldHVybiB0aGlzO1xuXHRcdFx0dGhpcy5mb2N1c0RhdGUgPSBudWxsO1xuXHRcdFx0dGhpcy5waWNrZXIuaGlkZSgpLmRldGFjaCgpO1xuXHRcdFx0dGhpcy5fZGV0YWNoU2Vjb25kYXJ5RXZlbnRzKCk7XG5cdFx0XHR0aGlzLnZpZXdNb2RlID0gdGhpcy5vLnN0YXJ0Vmlldztcblx0XHRcdHRoaXMuc2hvd01vZGUoKTtcblxuXHRcdFx0aWYgKHRoaXMuby5mb3JjZVBhcnNlICYmIHRoaXMuaW5wdXRGaWVsZC52YWwoKSlcblx0XHRcdFx0dGhpcy5zZXRWYWx1ZSgpO1xuXHRcdFx0dGhpcy5fdHJpZ2dlcignaGlkZScpO1xuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fSxcblxuXHRcdGRlc3Ryb3k6IGZ1bmN0aW9uKCl7XG5cdFx0XHR0aGlzLmhpZGUoKTtcblx0XHRcdHRoaXMuX2RldGFjaEV2ZW50cygpO1xuXHRcdFx0dGhpcy5fZGV0YWNoU2Vjb25kYXJ5RXZlbnRzKCk7XG5cdFx0XHR0aGlzLnBpY2tlci5yZW1vdmUoKTtcblx0XHRcdGRlbGV0ZSB0aGlzLmVsZW1lbnQuZGF0YSgpLmRhdGVwaWNrZXI7XG5cdFx0XHRpZiAoIXRoaXMuaXNJbnB1dCl7XG5cdFx0XHRcdGRlbGV0ZSB0aGlzLmVsZW1lbnQuZGF0YSgpLmRhdGU7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9LFxuXG5cdFx0cGFzdGU6IGZ1bmN0aW9uKGV2dCl7XG5cdFx0XHR2YXIgZGF0ZVN0cmluZztcblx0XHRcdGlmIChldnQub3JpZ2luYWxFdmVudC5jbGlwYm9hcmREYXRhICYmIGV2dC5vcmlnaW5hbEV2ZW50LmNsaXBib2FyZERhdGEudHlwZXNcblx0XHRcdFx0JiYgJC5pbkFycmF5KCd0ZXh0L3BsYWluJywgZXZ0Lm9yaWdpbmFsRXZlbnQuY2xpcGJvYXJkRGF0YS50eXBlcykgIT09IC0xKSB7XG5cdFx0XHRcdGRhdGVTdHJpbmcgPSBldnQub3JpZ2luYWxFdmVudC5jbGlwYm9hcmREYXRhLmdldERhdGEoJ3RleHQvcGxhaW4nKTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYgKHdpbmRvdy5jbGlwYm9hcmREYXRhKSB7XG5cdFx0XHRcdGRhdGVTdHJpbmcgPSB3aW5kb3cuY2xpcGJvYXJkRGF0YS5nZXREYXRhKCdUZXh0Jyk7XG5cdFx0XHR9XG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0cmV0dXJuO1xuXHRcdFx0fVxuXHRcdFx0dGhpcy5zZXREYXRlKGRhdGVTdHJpbmcpO1xuXHRcdFx0dGhpcy51cGRhdGUoKTtcblx0XHRcdGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdH0sXG5cblx0XHRfdXRjX3RvX2xvY2FsOiBmdW5jdGlvbih1dGMpe1xuXHRcdFx0cmV0dXJuIHV0YyAmJiBuZXcgRGF0ZSh1dGMuZ2V0VGltZSgpICsgKHV0Yy5nZXRUaW1lem9uZU9mZnNldCgpKjYwMDAwKSk7XG5cdFx0fSxcblx0XHRfbG9jYWxfdG9fdXRjOiBmdW5jdGlvbihsb2NhbCl7XG5cdFx0XHRyZXR1cm4gbG9jYWwgJiYgbmV3IERhdGUobG9jYWwuZ2V0VGltZSgpIC0gKGxvY2FsLmdldFRpbWV6b25lT2Zmc2V0KCkqNjAwMDApKTtcblx0XHR9LFxuXHRcdF96ZXJvX3RpbWU6IGZ1bmN0aW9uKGxvY2FsKXtcblx0XHRcdHJldHVybiBsb2NhbCAmJiBuZXcgRGF0ZShsb2NhbC5nZXRGdWxsWWVhcigpLCBsb2NhbC5nZXRNb250aCgpLCBsb2NhbC5nZXREYXRlKCkpO1xuXHRcdH0sXG5cdFx0X3plcm9fdXRjX3RpbWU6IGZ1bmN0aW9uKHV0Yyl7XG5cdFx0XHRyZXR1cm4gdXRjICYmIG5ldyBEYXRlKERhdGUuVVRDKHV0Yy5nZXRVVENGdWxsWWVhcigpLCB1dGMuZ2V0VVRDTW9udGgoKSwgdXRjLmdldFVUQ0RhdGUoKSkpO1xuXHRcdH0sXG5cblx0XHRnZXREYXRlczogZnVuY3Rpb24oKXtcblx0XHRcdHJldHVybiAkLm1hcCh0aGlzLmRhdGVzLCB0aGlzLl91dGNfdG9fbG9jYWwpO1xuXHRcdH0sXG5cblx0XHRnZXRVVENEYXRlczogZnVuY3Rpb24oKXtcblx0XHRcdHJldHVybiAkLm1hcCh0aGlzLmRhdGVzLCBmdW5jdGlvbihkKXtcblx0XHRcdFx0cmV0dXJuIG5ldyBEYXRlKGQpO1xuXHRcdFx0fSk7XG5cdFx0fSxcblxuXHRcdGdldERhdGU6IGZ1bmN0aW9uKCl7XG5cdFx0XHRyZXR1cm4gdGhpcy5fdXRjX3RvX2xvY2FsKHRoaXMuZ2V0VVRDRGF0ZSgpKTtcblx0XHR9LFxuXG5cdFx0Z2V0VVRDRGF0ZTogZnVuY3Rpb24oKXtcblx0XHRcdHZhciBzZWxlY3RlZF9kYXRlID0gdGhpcy5kYXRlcy5nZXQoLTEpO1xuXHRcdFx0aWYgKHR5cGVvZiBzZWxlY3RlZF9kYXRlICE9PSAndW5kZWZpbmVkJykge1xuXHRcdFx0XHRyZXR1cm4gbmV3IERhdGUoc2VsZWN0ZWRfZGF0ZSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRyZXR1cm4gbnVsbDtcblx0XHRcdH1cblx0XHR9LFxuXG5cdFx0Y2xlYXJEYXRlczogZnVuY3Rpb24oKXtcblx0XHRcdGlmICh0aGlzLmlucHV0RmllbGQpIHtcblx0XHRcdFx0dGhpcy5pbnB1dEZpZWxkLnZhbCgnJyk7XG5cdFx0XHR9XG5cblx0XHRcdHRoaXMudXBkYXRlKCk7XG5cdFx0XHR0aGlzLl90cmlnZ2VyKCdjaGFuZ2VEYXRlJyk7XG5cblx0XHRcdGlmICh0aGlzLm8uYXV0b2Nsb3NlKSB7XG5cdFx0XHRcdHRoaXMuaGlkZSgpO1xuXHRcdFx0fVxuXHRcdH0sXG5cdFx0c2V0RGF0ZXM6IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgYXJncyA9ICQuaXNBcnJheShhcmd1bWVudHNbMF0pID8gYXJndW1lbnRzWzBdIDogYXJndW1lbnRzO1xuXHRcdFx0dGhpcy51cGRhdGUuYXBwbHkodGhpcywgYXJncyk7XG5cdFx0XHR0aGlzLl90cmlnZ2VyKCdjaGFuZ2VEYXRlJyk7XG5cdFx0XHR0aGlzLnNldFZhbHVlKCk7XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9LFxuXG5cdFx0c2V0VVRDRGF0ZXM6IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgYXJncyA9ICQuaXNBcnJheShhcmd1bWVudHNbMF0pID8gYXJndW1lbnRzWzBdIDogYXJndW1lbnRzO1xuXHRcdFx0dGhpcy51cGRhdGUuYXBwbHkodGhpcywgJC5tYXAoYXJncywgdGhpcy5fdXRjX3RvX2xvY2FsKSk7XG5cdFx0XHR0aGlzLl90cmlnZ2VyKCdjaGFuZ2VEYXRlJyk7XG5cdFx0XHR0aGlzLnNldFZhbHVlKCk7XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9LFxuXG5cdFx0c2V0RGF0ZTogYWxpYXMoJ3NldERhdGVzJyksXG5cdFx0c2V0VVRDRGF0ZTogYWxpYXMoJ3NldFVUQ0RhdGVzJyksXG5cdFx0cmVtb3ZlOiBhbGlhcygnZGVzdHJveScpLFxuXG5cdFx0c2V0VmFsdWU6IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgZm9ybWF0dGVkID0gdGhpcy5nZXRGb3JtYXR0ZWREYXRlKCk7XG5cdFx0XHR0aGlzLmlucHV0RmllbGQudmFsKGZvcm1hdHRlZCk7XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9LFxuXG5cdFx0Z2V0Rm9ybWF0dGVkRGF0ZTogZnVuY3Rpb24oZm9ybWF0KXtcblx0XHRcdGlmIChmb3JtYXQgPT09IHVuZGVmaW5lZClcblx0XHRcdFx0Zm9ybWF0ID0gdGhpcy5vLmZvcm1hdDtcblxuXHRcdFx0dmFyIGxhbmcgPSB0aGlzLm8ubGFuZ3VhZ2U7XG5cdFx0XHRyZXR1cm4gJC5tYXAodGhpcy5kYXRlcywgZnVuY3Rpb24oZCl7XG5cdFx0XHRcdHJldHVybiBEUEdsb2JhbC5mb3JtYXREYXRlKGQsIGZvcm1hdCwgbGFuZyk7XG5cdFx0XHR9KS5qb2luKHRoaXMuby5tdWx0aWRhdGVTZXBhcmF0b3IpO1xuXHRcdH0sXG5cblx0XHRnZXRTdGFydERhdGU6IGZ1bmN0aW9uKCl7XG5cdFx0XHRyZXR1cm4gdGhpcy5vLnN0YXJ0RGF0ZTtcblx0XHR9LFxuXG5cdFx0c2V0U3RhcnREYXRlOiBmdW5jdGlvbihzdGFydERhdGUpe1xuXHRcdFx0dGhpcy5fcHJvY2Vzc19vcHRpb25zKHtzdGFydERhdGU6IHN0YXJ0RGF0ZX0pO1xuXHRcdFx0dGhpcy51cGRhdGUoKTtcblx0XHRcdHRoaXMudXBkYXRlTmF2QXJyb3dzKCk7XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9LFxuXG5cdFx0Z2V0RW5kRGF0ZTogZnVuY3Rpb24oKXtcblx0XHRcdHJldHVybiB0aGlzLm8uZW5kRGF0ZTtcblx0XHR9LFxuXG5cdFx0c2V0RW5kRGF0ZTogZnVuY3Rpb24oZW5kRGF0ZSl7XG5cdFx0XHR0aGlzLl9wcm9jZXNzX29wdGlvbnMoe2VuZERhdGU6IGVuZERhdGV9KTtcblx0XHRcdHRoaXMudXBkYXRlKCk7XG5cdFx0XHR0aGlzLnVwZGF0ZU5hdkFycm93cygpO1xuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fSxcblxuXHRcdHNldERheXNPZldlZWtEaXNhYmxlZDogZnVuY3Rpb24oZGF5c09mV2Vla0Rpc2FibGVkKXtcblx0XHRcdHRoaXMuX3Byb2Nlc3Nfb3B0aW9ucyh7ZGF5c09mV2Vla0Rpc2FibGVkOiBkYXlzT2ZXZWVrRGlzYWJsZWR9KTtcblx0XHRcdHRoaXMudXBkYXRlKCk7XG5cdFx0XHR0aGlzLnVwZGF0ZU5hdkFycm93cygpO1xuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fSxcblxuXHRcdHNldERheXNPZldlZWtIaWdobGlnaHRlZDogZnVuY3Rpb24oZGF5c09mV2Vla0hpZ2hsaWdodGVkKXtcblx0XHRcdHRoaXMuX3Byb2Nlc3Nfb3B0aW9ucyh7ZGF5c09mV2Vla0hpZ2hsaWdodGVkOiBkYXlzT2ZXZWVrSGlnaGxpZ2h0ZWR9KTtcblx0XHRcdHRoaXMudXBkYXRlKCk7XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9LFxuXG5cdFx0c2V0RGF0ZXNEaXNhYmxlZDogZnVuY3Rpb24oZGF0ZXNEaXNhYmxlZCl7XG5cdFx0XHR0aGlzLl9wcm9jZXNzX29wdGlvbnMoe2RhdGVzRGlzYWJsZWQ6IGRhdGVzRGlzYWJsZWR9KTtcblx0XHRcdHRoaXMudXBkYXRlKCk7XG5cdFx0XHR0aGlzLnVwZGF0ZU5hdkFycm93cygpO1xuXHRcdH0sXG5cblx0XHRwbGFjZTogZnVuY3Rpb24oKXtcblx0XHRcdGlmICh0aGlzLmlzSW5saW5lKVxuXHRcdFx0XHRyZXR1cm4gdGhpcztcblx0XHRcdHZhciBjYWxlbmRhcldpZHRoID0gdGhpcy5waWNrZXIub3V0ZXJXaWR0aCgpLFxuXHRcdFx0XHRjYWxlbmRhckhlaWdodCA9IHRoaXMucGlja2VyLm91dGVySGVpZ2h0KCksXG5cdFx0XHRcdHZpc3VhbFBhZGRpbmcgPSAxMCxcblx0XHRcdFx0Y29udGFpbmVyID0gJCh0aGlzLm8uY29udGFpbmVyKSxcblx0XHRcdFx0d2luZG93V2lkdGggPSBjb250YWluZXIud2lkdGgoKSxcblx0XHRcdFx0c2Nyb2xsVG9wID0gdGhpcy5vLmNvbnRhaW5lciA9PT0gJ2JvZHknID8gJChkb2N1bWVudCkuc2Nyb2xsVG9wKCkgOiBjb250YWluZXIuc2Nyb2xsVG9wKCksXG5cdFx0XHRcdGFwcGVuZE9mZnNldCA9IGNvbnRhaW5lci5vZmZzZXQoKTtcblxuXHRcdFx0dmFyIHBhcmVudHNaaW5kZXggPSBbXTtcblx0XHRcdHRoaXMuZWxlbWVudC5wYXJlbnRzKCkuZWFjaChmdW5jdGlvbigpe1xuXHRcdFx0XHR2YXIgaXRlbVpJbmRleCA9ICQodGhpcykuY3NzKCd6LWluZGV4Jyk7XG5cdFx0XHRcdGlmIChpdGVtWkluZGV4ICE9PSAnYXV0bycgJiYgaXRlbVpJbmRleCAhPT0gMCkgcGFyZW50c1ppbmRleC5wdXNoKHBhcnNlSW50KGl0ZW1aSW5kZXgpKTtcblx0XHRcdH0pO1xuXHRcdFx0dmFyIHpJbmRleCA9IE1hdGgubWF4LmFwcGx5KE1hdGgsIHBhcmVudHNaaW5kZXgpICsgdGhpcy5vLnpJbmRleE9mZnNldDtcblx0XHRcdHZhciBvZmZzZXQgPSB0aGlzLmNvbXBvbmVudCA/IHRoaXMuY29tcG9uZW50LnBhcmVudCgpLm9mZnNldCgpIDogdGhpcy5lbGVtZW50Lm9mZnNldCgpO1xuXHRcdFx0dmFyIGhlaWdodCA9IHRoaXMuY29tcG9uZW50ID8gdGhpcy5jb21wb25lbnQub3V0ZXJIZWlnaHQodHJ1ZSkgOiB0aGlzLmVsZW1lbnQub3V0ZXJIZWlnaHQoZmFsc2UpO1xuXHRcdFx0dmFyIHdpZHRoID0gdGhpcy5jb21wb25lbnQgPyB0aGlzLmNvbXBvbmVudC5vdXRlcldpZHRoKHRydWUpIDogdGhpcy5lbGVtZW50Lm91dGVyV2lkdGgoZmFsc2UpO1xuXHRcdFx0dmFyIGxlZnQgPSBvZmZzZXQubGVmdCAtIGFwcGVuZE9mZnNldC5sZWZ0LFxuXHRcdFx0XHR0b3AgPSBvZmZzZXQudG9wIC0gYXBwZW5kT2Zmc2V0LnRvcDtcblxuXHRcdFx0aWYgKHRoaXMuby5jb250YWluZXIgIT09ICdib2R5Jykge1xuXHRcdFx0XHR0b3AgKz0gc2Nyb2xsVG9wO1xuXHRcdFx0fVxuXG5cdFx0XHR0aGlzLnBpY2tlci5yZW1vdmVDbGFzcyhcblx0XHRcdFx0J2RhdGVwaWNrZXItb3JpZW50LXRvcCBkYXRlcGlja2VyLW9yaWVudC1ib3R0b20gJytcblx0XHRcdFx0J2RhdGVwaWNrZXItb3JpZW50LXJpZ2h0IGRhdGVwaWNrZXItb3JpZW50LWxlZnQnXG5cdFx0XHQpO1xuXG5cdFx0XHRpZiAodGhpcy5vLm9yaWVudGF0aW9uLnggIT09ICdhdXRvJyl7XG5cdFx0XHRcdHRoaXMucGlja2VyLmFkZENsYXNzKCdkYXRlcGlja2VyLW9yaWVudC0nICsgdGhpcy5vLm9yaWVudGF0aW9uLngpO1xuXHRcdFx0XHRpZiAodGhpcy5vLm9yaWVudGF0aW9uLnggPT09ICdyaWdodCcpXG5cdFx0XHRcdFx0bGVmdCAtPSBjYWxlbmRhcldpZHRoIC0gd2lkdGg7XG5cdFx0XHR9XG5cdFx0XHQvLyBhdXRvIHggb3JpZW50YXRpb24gaXMgYmVzdC1wbGFjZW1lbnQ6IGlmIGl0IGNyb3NzZXMgYSB3aW5kb3dcblx0XHRcdC8vIGVkZ2UsIGZ1ZGdlIGl0IHNpZGV3YXlzXG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0aWYgKG9mZnNldC5sZWZ0IDwgMCkge1xuXHRcdFx0XHRcdC8vIGNvbXBvbmVudCBpcyBvdXRzaWRlIHRoZSB3aW5kb3cgb24gdGhlIGxlZnQgc2lkZS4gTW92ZSBpdCBpbnRvIHZpc2libGUgcmFuZ2Vcblx0XHRcdFx0XHR0aGlzLnBpY2tlci5hZGRDbGFzcygnZGF0ZXBpY2tlci1vcmllbnQtbGVmdCcpO1xuXHRcdFx0XHRcdGxlZnQgLT0gb2Zmc2V0LmxlZnQgLSB2aXN1YWxQYWRkaW5nO1xuXHRcdFx0XHR9IGVsc2UgaWYgKGxlZnQgKyBjYWxlbmRhcldpZHRoID4gd2luZG93V2lkdGgpIHtcblx0XHRcdFx0XHQvLyB0aGUgY2FsZW5kYXIgcGFzc2VzIHRoZSB3aWRvdyByaWdodCBlZGdlLiBBbGlnbiBpdCB0byBjb21wb25lbnQgcmlnaHQgc2lkZVxuXHRcdFx0XHRcdHRoaXMucGlja2VyLmFkZENsYXNzKCdkYXRlcGlja2VyLW9yaWVudC1yaWdodCcpO1xuXHRcdFx0XHRcdGxlZnQgKz0gd2lkdGggLSBjYWxlbmRhcldpZHRoO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdC8vIERlZmF1bHQgdG8gbGVmdFxuXHRcdFx0XHRcdHRoaXMucGlja2VyLmFkZENsYXNzKCdkYXRlcGlja2VyLW9yaWVudC1sZWZ0Jyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblxuXHRcdFx0Ly8gYXV0byB5IG9yaWVudGF0aW9uIGlzIGJlc3Qtc2l0dWF0aW9uOiB0b3Agb3IgYm90dG9tLCBubyBmdWRnaW5nLFxuXHRcdFx0Ly8gZGVjaXNpb24gYmFzZWQgb24gd2hpY2ggc2hvd3MgbW9yZSBvZiB0aGUgY2FsZW5kYXJcblx0XHRcdHZhciB5b3JpZW50ID0gdGhpcy5vLm9yaWVudGF0aW9uLnksXG5cdFx0XHRcdHRvcF9vdmVyZmxvdztcblx0XHRcdGlmICh5b3JpZW50ID09PSAnYXV0bycpe1xuXHRcdFx0XHR0b3Bfb3ZlcmZsb3cgPSAtc2Nyb2xsVG9wICsgdG9wIC0gY2FsZW5kYXJIZWlnaHQ7XG5cdFx0XHRcdHlvcmllbnQgPSB0b3Bfb3ZlcmZsb3cgPCAwID8gJ2JvdHRvbScgOiAndG9wJztcblx0XHRcdH1cblxuXHRcdFx0dGhpcy5waWNrZXIuYWRkQ2xhc3MoJ2RhdGVwaWNrZXItb3JpZW50LScgKyB5b3JpZW50KTtcblx0XHRcdGlmICh5b3JpZW50ID09PSAndG9wJylcblx0XHRcdFx0dG9wIC09IGNhbGVuZGFySGVpZ2h0ICsgcGFyc2VJbnQodGhpcy5waWNrZXIuY3NzKCdwYWRkaW5nLXRvcCcpKTtcblx0XHRcdGVsc2Vcblx0XHRcdFx0dG9wICs9IGhlaWdodDtcblxuXHRcdFx0aWYgKHRoaXMuby5ydGwpIHtcblx0XHRcdFx0dmFyIHJpZ2h0ID0gd2luZG93V2lkdGggLSAobGVmdCArIHdpZHRoKTtcblx0XHRcdFx0dGhpcy5waWNrZXIuY3NzKHtcblx0XHRcdFx0XHR0b3A6IHRvcCxcblx0XHRcdFx0XHRyaWdodDogcmlnaHQsXG5cdFx0XHRcdFx0ekluZGV4OiB6SW5kZXhcblx0XHRcdFx0fSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHR0aGlzLnBpY2tlci5jc3Moe1xuXHRcdFx0XHRcdHRvcDogdG9wLFxuXHRcdFx0XHRcdGxlZnQ6IGxlZnQsXG5cdFx0XHRcdFx0ekluZGV4OiB6SW5kZXhcblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gdGhpcztcblx0XHR9LFxuXG5cdFx0X2FsbG93X3VwZGF0ZTogdHJ1ZSxcblx0XHR1cGRhdGU6IGZ1bmN0aW9uKCl7XG5cdFx0XHRpZiAoIXRoaXMuX2FsbG93X3VwZGF0ZSlcblx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cblx0XHRcdHZhciBvbGREYXRlcyA9IHRoaXMuZGF0ZXMuY29weSgpLFxuXHRcdFx0XHRkYXRlcyA9IFtdLFxuXHRcdFx0XHRmcm9tQXJncyA9IGZhbHNlO1xuXHRcdFx0aWYgKGFyZ3VtZW50cy5sZW5ndGgpe1xuXHRcdFx0XHQkLmVhY2goYXJndW1lbnRzLCAkLnByb3h5KGZ1bmN0aW9uKGksIGRhdGUpe1xuXHRcdFx0XHRcdGlmIChkYXRlIGluc3RhbmNlb2YgRGF0ZSlcblx0XHRcdFx0XHRcdGRhdGUgPSB0aGlzLl9sb2NhbF90b191dGMoZGF0ZSk7XG5cdFx0XHRcdFx0ZGF0ZXMucHVzaChkYXRlKTtcblx0XHRcdFx0fSwgdGhpcykpO1xuXHRcdFx0XHRmcm9tQXJncyA9IHRydWU7XG5cdFx0XHR9XG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0ZGF0ZXMgPSB0aGlzLmlzSW5wdXRcblx0XHRcdFx0XHRcdD8gdGhpcy5lbGVtZW50LnZhbCgpXG5cdFx0XHRcdFx0XHQ6IHRoaXMuZWxlbWVudC5kYXRhKCdkYXRlJykgfHwgdGhpcy5pbnB1dEZpZWxkLnZhbCgpO1xuXHRcdFx0XHRpZiAoZGF0ZXMgJiYgdGhpcy5vLm11bHRpZGF0ZSlcblx0XHRcdFx0XHRkYXRlcyA9IGRhdGVzLnNwbGl0KHRoaXMuby5tdWx0aWRhdGVTZXBhcmF0b3IpO1xuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0ZGF0ZXMgPSBbZGF0ZXNdO1xuXHRcdFx0XHRkZWxldGUgdGhpcy5lbGVtZW50LmRhdGEoKS5kYXRlO1xuXHRcdFx0fVxuXG5cdFx0XHRkYXRlcyA9ICQubWFwKGRhdGVzLCAkLnByb3h5KGZ1bmN0aW9uKGRhdGUpe1xuXHRcdFx0XHRyZXR1cm4gRFBHbG9iYWwucGFyc2VEYXRlKGRhdGUsIHRoaXMuby5mb3JtYXQsIHRoaXMuby5sYW5ndWFnZSwgdGhpcy5vLmFzc3VtZU5lYXJieVllYXIpO1xuXHRcdFx0fSwgdGhpcykpO1xuXHRcdFx0ZGF0ZXMgPSAkLmdyZXAoZGF0ZXMsICQucHJveHkoZnVuY3Rpb24oZGF0ZSl7XG5cdFx0XHRcdHJldHVybiAoXG5cdFx0XHRcdFx0IXRoaXMuZGF0ZVdpdGhpblJhbmdlKGRhdGUpIHx8XG5cdFx0XHRcdFx0IWRhdGVcblx0XHRcdFx0KTtcblx0XHRcdH0sIHRoaXMpLCB0cnVlKTtcblx0XHRcdHRoaXMuZGF0ZXMucmVwbGFjZShkYXRlcyk7XG5cblx0XHRcdGlmICh0aGlzLmRhdGVzLmxlbmd0aClcblx0XHRcdFx0dGhpcy52aWV3RGF0ZSA9IG5ldyBEYXRlKHRoaXMuZGF0ZXMuZ2V0KC0xKSk7XG5cdFx0XHRlbHNlIGlmICh0aGlzLnZpZXdEYXRlIDwgdGhpcy5vLnN0YXJ0RGF0ZSlcblx0XHRcdFx0dGhpcy52aWV3RGF0ZSA9IG5ldyBEYXRlKHRoaXMuby5zdGFydERhdGUpO1xuXHRcdFx0ZWxzZSBpZiAodGhpcy52aWV3RGF0ZSA+IHRoaXMuby5lbmREYXRlKVxuXHRcdFx0XHR0aGlzLnZpZXdEYXRlID0gbmV3IERhdGUodGhpcy5vLmVuZERhdGUpO1xuXHRcdFx0ZWxzZVxuXHRcdFx0XHR0aGlzLnZpZXdEYXRlID0gdGhpcy5vLmRlZmF1bHRWaWV3RGF0ZTtcblxuXHRcdFx0aWYgKGZyb21BcmdzKXtcblx0XHRcdFx0Ly8gc2V0dGluZyBkYXRlIGJ5IGNsaWNraW5nXG5cdFx0XHRcdHRoaXMuc2V0VmFsdWUoKTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYgKGRhdGVzLmxlbmd0aCl7XG5cdFx0XHRcdC8vIHNldHRpbmcgZGF0ZSBieSB0eXBpbmdcblx0XHRcdFx0aWYgKFN0cmluZyhvbGREYXRlcykgIT09IFN0cmluZyh0aGlzLmRhdGVzKSlcblx0XHRcdFx0XHR0aGlzLl90cmlnZ2VyKCdjaGFuZ2VEYXRlJyk7XG5cdFx0XHR9XG5cdFx0XHRpZiAoIXRoaXMuZGF0ZXMubGVuZ3RoICYmIG9sZERhdGVzLmxlbmd0aClcblx0XHRcdFx0dGhpcy5fdHJpZ2dlcignY2xlYXJEYXRlJyk7XG5cblx0XHRcdHRoaXMuZmlsbCgpO1xuXHRcdFx0dGhpcy5lbGVtZW50LmNoYW5nZSgpO1xuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fSxcblxuXHRcdGZpbGxEb3c6IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgZG93Q250ID0gdGhpcy5vLndlZWtTdGFydCxcblx0XHRcdFx0aHRtbCA9ICc8dHI+Jztcblx0XHRcdGlmICh0aGlzLm8uY2FsZW5kYXJXZWVrcyl7XG5cdFx0XHRcdHRoaXMucGlja2VyLmZpbmQoJy5kYXRlcGlja2VyLWRheXMgLmRhdGVwaWNrZXItc3dpdGNoJylcblx0XHRcdFx0XHQuYXR0cignY29sc3BhbicsIGZ1bmN0aW9uKGksIHZhbCl7XG5cdFx0XHRcdFx0XHRyZXR1cm4gcGFyc2VJbnQodmFsKSArIDE7XG5cdFx0XHRcdFx0fSk7XG5cdFx0XHRcdGh0bWwgKz0gJzx0aCBjbGFzcz1cImN3XCI+JiMxNjA7PC90aD4nO1xuXHRcdFx0fVxuXHRcdFx0d2hpbGUgKGRvd0NudCA8IHRoaXMuby53ZWVrU3RhcnQgKyA3KXtcblx0XHRcdFx0aHRtbCArPSAnPHRoIGNsYXNzPVwiZG93JztcbiAgICAgICAgaWYgKCQuaW5BcnJheShkb3dDbnQsIHRoaXMuby5kYXlzT2ZXZWVrRGlzYWJsZWQpID4gLTEpXG4gICAgICAgICAgaHRtbCArPSAnIGRpc2FibGVkJztcbiAgICAgICAgaHRtbCArPSAnXCI+JytkYXRlc1t0aGlzLm8ubGFuZ3VhZ2VdLmRheXNNaW5bKGRvd0NudCsrKSU3XSsnPC90aD4nO1xuXHRcdFx0fVxuXHRcdFx0aHRtbCArPSAnPC90cj4nO1xuXHRcdFx0dGhpcy5waWNrZXIuZmluZCgnLmRhdGVwaWNrZXItZGF5cyB0aGVhZCcpLmFwcGVuZChodG1sKTtcblx0XHR9LFxuXG5cdFx0ZmlsbE1vbnRoczogZnVuY3Rpb24oKXtcbiAgICAgIHZhciBsb2NhbERhdGUgPSB0aGlzLl91dGNfdG9fbG9jYWwodGhpcy52aWV3RGF0ZSk7XG5cdFx0XHR2YXIgaHRtbCA9ICcnLFxuXHRcdFx0aSA9IDA7XG5cdFx0XHR3aGlsZSAoaSA8IDEyKXtcbiAgICAgICAgdmFyIGZvY3VzZWQgPSBsb2NhbERhdGUgJiYgbG9jYWxEYXRlLmdldE1vbnRoKCkgPT09IGkgPyAnIGZvY3VzZWQnIDogJyc7XG5cdFx0XHRcdGh0bWwgKz0gJzxzcGFuIGNsYXNzPVwibW9udGgnICsgZm9jdXNlZCArICdcIj4nICsgZGF0ZXNbdGhpcy5vLmxhbmd1YWdlXS5tb250aHNTaG9ydFtpKytdKyc8L3NwYW4+Jztcblx0XHRcdH1cblx0XHRcdHRoaXMucGlja2VyLmZpbmQoJy5kYXRlcGlja2VyLW1vbnRocyB0ZCcpLmh0bWwoaHRtbCk7XG5cdFx0fSxcblxuXHRcdHNldFJhbmdlOiBmdW5jdGlvbihyYW5nZSl7XG5cdFx0XHRpZiAoIXJhbmdlIHx8ICFyYW5nZS5sZW5ndGgpXG5cdFx0XHRcdGRlbGV0ZSB0aGlzLnJhbmdlO1xuXHRcdFx0ZWxzZVxuXHRcdFx0XHR0aGlzLnJhbmdlID0gJC5tYXAocmFuZ2UsIGZ1bmN0aW9uKGQpe1xuXHRcdFx0XHRcdHJldHVybiBkLnZhbHVlT2YoKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR0aGlzLmZpbGwoKTtcblx0XHR9LFxuXG5cdFx0Z2V0Q2xhc3NOYW1lczogZnVuY3Rpb24oZGF0ZSl7XG5cdFx0XHR2YXIgY2xzID0gW10sXG5cdFx0XHRcdHllYXIgPSB0aGlzLnZpZXdEYXRlLmdldFVUQ0Z1bGxZZWFyKCksXG5cdFx0XHRcdG1vbnRoID0gdGhpcy52aWV3RGF0ZS5nZXRVVENNb250aCgpLFxuXHRcdFx0XHR0b2RheSA9IG5ldyBEYXRlKCk7XG5cdFx0XHRpZiAoZGF0ZS5nZXRVVENGdWxsWWVhcigpIDwgeWVhciB8fCAoZGF0ZS5nZXRVVENGdWxsWWVhcigpID09PSB5ZWFyICYmIGRhdGUuZ2V0VVRDTW9udGgoKSA8IG1vbnRoKSl7XG5cdFx0XHRcdGNscy5wdXNoKCdvbGQnKTtcblx0XHRcdH1cblx0XHRcdGVsc2UgaWYgKGRhdGUuZ2V0VVRDRnVsbFllYXIoKSA+IHllYXIgfHwgKGRhdGUuZ2V0VVRDRnVsbFllYXIoKSA9PT0geWVhciAmJiBkYXRlLmdldFVUQ01vbnRoKCkgPiBtb250aCkpe1xuXHRcdFx0XHRjbHMucHVzaCgnbmV3Jyk7XG5cdFx0XHR9XG5cdFx0XHRpZiAodGhpcy5mb2N1c0RhdGUgJiYgZGF0ZS52YWx1ZU9mKCkgPT09IHRoaXMuZm9jdXNEYXRlLnZhbHVlT2YoKSlcblx0XHRcdFx0Y2xzLnB1c2goJ2ZvY3VzZWQnKTtcblx0XHRcdC8vIENvbXBhcmUgaW50ZXJuYWwgVVRDIGRhdGUgd2l0aCBsb2NhbCB0b2RheSwgbm90IFVUQyB0b2RheVxuXHRcdFx0aWYgKHRoaXMuby50b2RheUhpZ2hsaWdodCAmJlxuXHRcdFx0XHRkYXRlLmdldFVUQ0Z1bGxZZWFyKCkgPT09IHRvZGF5LmdldEZ1bGxZZWFyKCkgJiZcblx0XHRcdFx0ZGF0ZS5nZXRVVENNb250aCgpID09PSB0b2RheS5nZXRNb250aCgpICYmXG5cdFx0XHRcdGRhdGUuZ2V0VVRDRGF0ZSgpID09PSB0b2RheS5nZXREYXRlKCkpe1xuXHRcdFx0XHRjbHMucHVzaCgndG9kYXknKTtcblx0XHRcdH1cblx0XHRcdGlmICh0aGlzLmRhdGVzLmNvbnRhaW5zKGRhdGUpICE9PSAtMSlcblx0XHRcdFx0Y2xzLnB1c2goJ2FjdGl2ZScpO1xuXHRcdFx0aWYgKCF0aGlzLmRhdGVXaXRoaW5SYW5nZShkYXRlKSl7XG5cdFx0XHRcdGNscy5wdXNoKCdkaXNhYmxlZCcpO1xuXHRcdFx0fVxuXHRcdFx0aWYgKHRoaXMuZGF0ZUlzRGlzYWJsZWQoZGF0ZSkpe1xuXHRcdFx0XHRjbHMucHVzaCgnZGlzYWJsZWQnLCAnZGlzYWJsZWQtZGF0ZScpO1x0XG5cdFx0XHR9IFxuXHRcdFx0aWYgKCQuaW5BcnJheShkYXRlLmdldFVUQ0RheSgpLCB0aGlzLm8uZGF5c09mV2Vla0hpZ2hsaWdodGVkKSAhPT0gLTEpe1xuXHRcdFx0XHRjbHMucHVzaCgnaGlnaGxpZ2h0ZWQnKTtcblx0XHRcdH1cblxuXHRcdFx0aWYgKHRoaXMucmFuZ2Upe1xuXHRcdFx0XHRpZiAoZGF0ZSA+IHRoaXMucmFuZ2VbMF0gJiYgZGF0ZSA8IHRoaXMucmFuZ2VbdGhpcy5yYW5nZS5sZW5ndGgtMV0pe1xuXHRcdFx0XHRcdGNscy5wdXNoKCdyYW5nZScpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGlmICgkLmluQXJyYXkoZGF0ZS52YWx1ZU9mKCksIHRoaXMucmFuZ2UpICE9PSAtMSl7XG5cdFx0XHRcdFx0Y2xzLnB1c2goJ3NlbGVjdGVkJyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0aWYgKGRhdGUudmFsdWVPZigpID09PSB0aGlzLnJhbmdlWzBdKXtcbiAgICAgICAgICBjbHMucHVzaCgncmFuZ2Utc3RhcnQnKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZGF0ZS52YWx1ZU9mKCkgPT09IHRoaXMucmFuZ2VbdGhpcy5yYW5nZS5sZW5ndGgtMV0pe1xuICAgICAgICAgIGNscy5wdXNoKCdyYW5nZS1lbmQnKTtcbiAgICAgICAgfVxuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIGNscztcblx0XHR9LFxuXG5cdFx0X2ZpbGxfeWVhcnNWaWV3OiBmdW5jdGlvbihzZWxlY3RvciwgY3NzQ2xhc3MsIGZhY3Rvciwgc3RlcCwgY3VycmVudFllYXIsIHN0YXJ0WWVhciwgZW5kWWVhciwgY2FsbGJhY2spe1xuXHRcdFx0dmFyIGh0bWwsIHZpZXcsIHllYXIsIHN0ZXBzLCBzdGFydFN0ZXAsIGVuZFN0ZXAsIHRoaXNZZWFyLCBpLCBjbGFzc2VzLCB0b29sdGlwLCBiZWZvcmU7XG5cblx0XHRcdGh0bWwgICAgICA9ICcnO1xuXHRcdFx0dmlldyAgICAgID0gdGhpcy5waWNrZXIuZmluZChzZWxlY3Rvcik7XG5cdFx0XHR5ZWFyICAgICAgPSBwYXJzZUludChjdXJyZW50WWVhciAvIGZhY3RvciwgMTApICogZmFjdG9yO1xuXHRcdFx0c3RhcnRTdGVwID0gcGFyc2VJbnQoc3RhcnRZZWFyIC8gc3RlcCwgMTApICogc3RlcDtcblx0XHRcdGVuZFN0ZXAgICA9IHBhcnNlSW50KGVuZFllYXIgLyBzdGVwLCAxMCkgKiBzdGVwO1xuXHRcdFx0c3RlcHMgICAgID0gJC5tYXAodGhpcy5kYXRlcywgZnVuY3Rpb24oZCl7XG5cdFx0XHRcdHJldHVybiBwYXJzZUludChkLmdldFVUQ0Z1bGxZZWFyKCkgLyBzdGVwLCAxMCkgKiBzdGVwO1xuXHRcdFx0fSk7XG5cblx0XHRcdHZpZXcuZmluZCgnLmRhdGVwaWNrZXItc3dpdGNoJykudGV4dCh5ZWFyICsgJy0nICsgKHllYXIgKyBzdGVwICogOSkpO1xuXG5cdFx0XHR0aGlzWWVhciA9IHllYXIgLSBzdGVwO1xuXHRcdFx0Zm9yIChpID0gLTE7IGkgPCAxMTsgaSArPSAxKSB7XG5cdFx0XHRcdGNsYXNzZXMgPSBbY3NzQ2xhc3NdO1xuXHRcdFx0XHR0b29sdGlwID0gbnVsbDtcblxuXHRcdFx0XHRpZiAoaSA9PT0gLTEpIHtcblx0XHRcdFx0XHRjbGFzc2VzLnB1c2goJ29sZCcpO1xuXHRcdFx0XHR9IGVsc2UgaWYgKGkgPT09IDEwKSB7XG5cdFx0XHRcdFx0Y2xhc3Nlcy5wdXNoKCduZXcnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoJC5pbkFycmF5KHRoaXNZZWFyLCBzdGVwcykgIT09IC0xKSB7XG5cdFx0XHRcdFx0Y2xhc3Nlcy5wdXNoKCdhY3RpdmUnKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAodGhpc1llYXIgPCBzdGFydFN0ZXAgfHwgdGhpc1llYXIgPiBlbmRTdGVwKSB7XG5cdFx0XHRcdFx0Y2xhc3Nlcy5wdXNoKCdkaXNhYmxlZCcpO1xuXHRcdFx0XHR9XG4gICAgICAgIGlmICh0aGlzWWVhciA9PT0gdGhpcy52aWV3RGF0ZS5nZXRGdWxsWWVhcigpKSB7XG5cdFx0XHRcdCAgY2xhc3Nlcy5wdXNoKCdmb2N1c2VkJyk7XG4gICAgICAgIH1cblxuXHRcdFx0XHRpZiAoY2FsbGJhY2sgIT09ICQubm9vcCkge1xuXHRcdFx0XHRcdGJlZm9yZSA9IGNhbGxiYWNrKG5ldyBEYXRlKHRoaXNZZWFyLCAwLCAxKSk7XG5cdFx0XHRcdFx0aWYgKGJlZm9yZSA9PT0gdW5kZWZpbmVkKSB7XG5cdFx0XHRcdFx0XHRiZWZvcmUgPSB7fTtcblx0XHRcdFx0XHR9IGVsc2UgaWYgKHR5cGVvZihiZWZvcmUpID09PSAnYm9vbGVhbicpIHtcblx0XHRcdFx0XHRcdGJlZm9yZSA9IHtlbmFibGVkOiBiZWZvcmV9O1xuXHRcdFx0XHRcdH0gZWxzZSBpZiAodHlwZW9mKGJlZm9yZSkgPT09ICdzdHJpbmcnKSB7XG5cdFx0XHRcdFx0XHRiZWZvcmUgPSB7Y2xhc3NlczogYmVmb3JlfTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKGJlZm9yZS5lbmFibGVkID09PSBmYWxzZSkge1xuXHRcdFx0XHRcdFx0Y2xhc3Nlcy5wdXNoKCdkaXNhYmxlZCcpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRpZiAoYmVmb3JlLmNsYXNzZXMpIHtcblx0XHRcdFx0XHRcdGNsYXNzZXMgPSBjbGFzc2VzLmNvbmNhdChiZWZvcmUuY2xhc3Nlcy5zcGxpdCgvXFxzKy8pKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKGJlZm9yZS50b29sdGlwKSB7XG5cdFx0XHRcdFx0XHR0b29sdGlwID0gYmVmb3JlLnRvb2x0aXA7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0aHRtbCArPSAnPHNwYW4gY2xhc3M9XCInICsgY2xhc3Nlcy5qb2luKCcgJykgKyAnXCInICsgKHRvb2x0aXAgPyAnIHRpdGxlPVwiJyArIHRvb2x0aXAgKyAnXCInIDogJycpICsgJz4nICsgdGhpc1llYXIgKyAnPC9zcGFuPic7XG5cdFx0XHRcdHRoaXNZZWFyICs9IHN0ZXA7XG5cdFx0XHR9XG5cdFx0XHR2aWV3LmZpbmQoJ3RkJykuaHRtbChodG1sKTtcblx0XHR9LFxuXG5cdFx0ZmlsbDogZnVuY3Rpb24oKXtcblx0XHRcdHZhciBkID0gbmV3IERhdGUodGhpcy52aWV3RGF0ZSksXG5cdFx0XHRcdHllYXIgPSBkLmdldFVUQ0Z1bGxZZWFyKCksXG5cdFx0XHRcdG1vbnRoID0gZC5nZXRVVENNb250aCgpLFxuXHRcdFx0XHRzdGFydFllYXIgPSB0aGlzLm8uc3RhcnREYXRlICE9PSAtSW5maW5pdHkgPyB0aGlzLm8uc3RhcnREYXRlLmdldFVUQ0Z1bGxZZWFyKCkgOiAtSW5maW5pdHksXG5cdFx0XHRcdHN0YXJ0TW9udGggPSB0aGlzLm8uc3RhcnREYXRlICE9PSAtSW5maW5pdHkgPyB0aGlzLm8uc3RhcnREYXRlLmdldFVUQ01vbnRoKCkgOiAtSW5maW5pdHksXG5cdFx0XHRcdGVuZFllYXIgPSB0aGlzLm8uZW5kRGF0ZSAhPT0gSW5maW5pdHkgPyB0aGlzLm8uZW5kRGF0ZS5nZXRVVENGdWxsWWVhcigpIDogSW5maW5pdHksXG5cdFx0XHRcdGVuZE1vbnRoID0gdGhpcy5vLmVuZERhdGUgIT09IEluZmluaXR5ID8gdGhpcy5vLmVuZERhdGUuZ2V0VVRDTW9udGgoKSA6IEluZmluaXR5LFxuXHRcdFx0XHR0b2RheXR4dCA9IGRhdGVzW3RoaXMuby5sYW5ndWFnZV0udG9kYXkgfHwgZGF0ZXNbJ2VuJ10udG9kYXkgfHwgJycsXG5cdFx0XHRcdGNsZWFydHh0ID0gZGF0ZXNbdGhpcy5vLmxhbmd1YWdlXS5jbGVhciB8fCBkYXRlc1snZW4nXS5jbGVhciB8fCAnJyxcblx0XHRcdFx0dGl0bGVGb3JtYXQgPSBkYXRlc1t0aGlzLm8ubGFuZ3VhZ2VdLnRpdGxlRm9ybWF0IHx8IGRhdGVzWydlbiddLnRpdGxlRm9ybWF0LFxuXHRcdFx0XHR0b29sdGlwLFxuXHRcdFx0XHRiZWZvcmU7XG5cdFx0XHRpZiAoaXNOYU4oeWVhcikgfHwgaXNOYU4obW9udGgpKVxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR0aGlzLnBpY2tlci5maW5kKCcuZGF0ZXBpY2tlci1kYXlzIC5kYXRlcGlja2VyLXN3aXRjaCcpXG5cdFx0XHRcdFx0XHQudGV4dChEUEdsb2JhbC5mb3JtYXREYXRlKGQsIHRpdGxlRm9ybWF0LCB0aGlzLm8ubGFuZ3VhZ2UpKTtcblx0XHRcdHRoaXMucGlja2VyLmZpbmQoJ3Rmb290IC50b2RheScpXG5cdFx0XHRcdFx0XHQudGV4dCh0b2RheXR4dClcblx0XHRcdFx0XHRcdC50b2dnbGUodGhpcy5vLnRvZGF5QnRuICE9PSBmYWxzZSk7XG5cdFx0XHR0aGlzLnBpY2tlci5maW5kKCd0Zm9vdCAuY2xlYXInKVxuXHRcdFx0XHRcdFx0LnRleHQoY2xlYXJ0eHQpXG5cdFx0XHRcdFx0XHQudG9nZ2xlKHRoaXMuby5jbGVhckJ0biAhPT0gZmFsc2UpO1xuXHRcdFx0dGhpcy5waWNrZXIuZmluZCgndGhlYWQgLmRhdGVwaWNrZXItdGl0bGUnKVxuXHRcdFx0XHRcdFx0LnRleHQodGhpcy5vLnRpdGxlKVxuXHRcdFx0XHRcdFx0LnRvZ2dsZSh0aGlzLm8udGl0bGUgIT09ICcnKTtcblx0XHRcdHRoaXMudXBkYXRlTmF2QXJyb3dzKCk7XG5cdFx0XHR0aGlzLmZpbGxNb250aHMoKTtcblx0XHRcdHZhciBwcmV2TW9udGggPSBVVENEYXRlKHllYXIsIG1vbnRoLTEsIDI4KSxcblx0XHRcdFx0ZGF5ID0gRFBHbG9iYWwuZ2V0RGF5c0luTW9udGgocHJldk1vbnRoLmdldFVUQ0Z1bGxZZWFyKCksIHByZXZNb250aC5nZXRVVENNb250aCgpKTtcblx0XHRcdHByZXZNb250aC5zZXRVVENEYXRlKGRheSk7XG5cdFx0XHRwcmV2TW9udGguc2V0VVRDRGF0ZShkYXkgLSAocHJldk1vbnRoLmdldFVUQ0RheSgpIC0gdGhpcy5vLndlZWtTdGFydCArIDcpJTcpO1xuXHRcdFx0dmFyIG5leHRNb250aCA9IG5ldyBEYXRlKHByZXZNb250aCk7XG5cdFx0XHRpZiAocHJldk1vbnRoLmdldFVUQ0Z1bGxZZWFyKCkgPCAxMDApe1xuICAgICAgICBuZXh0TW9udGguc2V0VVRDRnVsbFllYXIocHJldk1vbnRoLmdldFVUQ0Z1bGxZZWFyKCkpO1xuICAgICAgfVxuXHRcdFx0bmV4dE1vbnRoLnNldFVUQ0RhdGUobmV4dE1vbnRoLmdldFVUQ0RhdGUoKSArIDQyKTtcblx0XHRcdG5leHRNb250aCA9IG5leHRNb250aC52YWx1ZU9mKCk7XG5cdFx0XHR2YXIgaHRtbCA9IFtdO1xuXHRcdFx0dmFyIGNsc05hbWU7XG5cdFx0XHR3aGlsZSAocHJldk1vbnRoLnZhbHVlT2YoKSA8IG5leHRNb250aCl7XG5cdFx0XHRcdGlmIChwcmV2TW9udGguZ2V0VVRDRGF5KCkgPT09IHRoaXMuby53ZWVrU3RhcnQpe1xuXHRcdFx0XHRcdGh0bWwucHVzaCgnPHRyPicpO1xuXHRcdFx0XHRcdGlmICh0aGlzLm8uY2FsZW5kYXJXZWVrcyl7XG5cdFx0XHRcdFx0XHQvLyBJU08gODYwMTogRmlyc3Qgd2VlayBjb250YWlucyBmaXJzdCB0aHVyc2RheS5cblx0XHRcdFx0XHRcdC8vIElTTyBhbHNvIHN0YXRlcyB3ZWVrIHN0YXJ0cyBvbiBNb25kYXksIGJ1dCB3ZSBjYW4gYmUgbW9yZSBhYnN0cmFjdCBoZXJlLlxuXHRcdFx0XHRcdFx0dmFyXG5cdFx0XHRcdFx0XHRcdC8vIFN0YXJ0IG9mIGN1cnJlbnQgd2VlazogYmFzZWQgb24gd2Vla3N0YXJ0L2N1cnJlbnQgZGF0ZVxuXHRcdFx0XHRcdFx0XHR3cyA9IG5ldyBEYXRlKCtwcmV2TW9udGggKyAodGhpcy5vLndlZWtTdGFydCAtIHByZXZNb250aC5nZXRVVENEYXkoKSAtIDcpICUgNyAqIDg2NGU1KSxcblx0XHRcdFx0XHRcdFx0Ly8gVGh1cnNkYXkgb2YgdGhpcyB3ZWVrXG5cdFx0XHRcdFx0XHRcdHRoID0gbmV3IERhdGUoTnVtYmVyKHdzKSArICg3ICsgNCAtIHdzLmdldFVUQ0RheSgpKSAlIDcgKiA4NjRlNSksXG5cdFx0XHRcdFx0XHRcdC8vIEZpcnN0IFRodXJzZGF5IG9mIHllYXIsIHllYXIgZnJvbSB0aHVyc2RheVxuXHRcdFx0XHRcdFx0XHR5dGggPSBuZXcgRGF0ZShOdW1iZXIoeXRoID0gVVRDRGF0ZSh0aC5nZXRVVENGdWxsWWVhcigpLCAwLCAxKSkgKyAoNyArIDQgLSB5dGguZ2V0VVRDRGF5KCkpJTcqODY0ZTUpLFxuXHRcdFx0XHRcdFx0XHQvLyBDYWxlbmRhciB3ZWVrOiBtcyBiZXR3ZWVuIHRodXJzZGF5cywgZGl2IG1zIHBlciBkYXksIGRpdiA3IGRheXNcblx0XHRcdFx0XHRcdFx0Y2FsV2VlayA9ICAodGggLSB5dGgpIC8gODY0ZTUgLyA3ICsgMTtcblx0XHRcdFx0XHRcdGh0bWwucHVzaCgnPHRkIGNsYXNzPVwiY3dcIj4nKyBjYWxXZWVrICsnPC90ZD4nKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblx0XHRcdFx0Y2xzTmFtZSA9IHRoaXMuZ2V0Q2xhc3NOYW1lcyhwcmV2TW9udGgpO1xuXHRcdFx0XHRjbHNOYW1lLnB1c2goJ2RheScpO1xuXG5cdFx0XHRcdGlmICh0aGlzLm8uYmVmb3JlU2hvd0RheSAhPT0gJC5ub29wKXtcblx0XHRcdFx0XHRiZWZvcmUgPSB0aGlzLm8uYmVmb3JlU2hvd0RheSh0aGlzLl91dGNfdG9fbG9jYWwocHJldk1vbnRoKSk7XG5cdFx0XHRcdFx0aWYgKGJlZm9yZSA9PT0gdW5kZWZpbmVkKVxuXHRcdFx0XHRcdFx0YmVmb3JlID0ge307XG5cdFx0XHRcdFx0ZWxzZSBpZiAodHlwZW9mKGJlZm9yZSkgPT09ICdib29sZWFuJylcblx0XHRcdFx0XHRcdGJlZm9yZSA9IHtlbmFibGVkOiBiZWZvcmV9O1xuXHRcdFx0XHRcdGVsc2UgaWYgKHR5cGVvZihiZWZvcmUpID09PSAnc3RyaW5nJylcblx0XHRcdFx0XHRcdGJlZm9yZSA9IHtjbGFzc2VzOiBiZWZvcmV9O1xuXHRcdFx0XHRcdGlmIChiZWZvcmUuZW5hYmxlZCA9PT0gZmFsc2UpXG5cdFx0XHRcdFx0XHRjbHNOYW1lLnB1c2goJ2Rpc2FibGVkJyk7XG5cdFx0XHRcdFx0aWYgKGJlZm9yZS5jbGFzc2VzKVxuXHRcdFx0XHRcdFx0Y2xzTmFtZSA9IGNsc05hbWUuY29uY2F0KGJlZm9yZS5jbGFzc2VzLnNwbGl0KC9cXHMrLykpO1xuXHRcdFx0XHRcdGlmIChiZWZvcmUudG9vbHRpcClcblx0XHRcdFx0XHRcdHRvb2x0aXAgPSBiZWZvcmUudG9vbHRpcDtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vQ2hlY2sgaWYgdW5pcXVlU29ydCBleGlzdHMgKHN1cHBvcnRlZCBieSBqcXVlcnkgPj0xLjEyIGFuZCA+PTIuMilcblx0XHRcdFx0Ly9GYWxsYmFjayB0byB1bmlxdWUgZnVuY3Rpb24gZm9yIG9sZGVyIGpxdWVyeSB2ZXJzaW9uc1xuXHRcdFx0XHRpZiAoJC5pc0Z1bmN0aW9uKCQudW5pcXVlU29ydCkpIHtcblx0XHRcdFx0XHRjbHNOYW1lID0gJC51bmlxdWVTb3J0KGNsc05hbWUpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdGNsc05hbWUgPSAkLnVuaXF1ZShjbHNOYW1lKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGh0bWwucHVzaCgnPHRkIGNsYXNzPVwiJytjbHNOYW1lLmpvaW4oJyAnKSsnXCInICsgKHRvb2x0aXAgPyAnIHRpdGxlPVwiJyt0b29sdGlwKydcIicgOiAnJykgKyAnPicrcHJldk1vbnRoLmdldFVUQ0RhdGUoKSArICc8L3RkPicpO1xuXHRcdFx0XHR0b29sdGlwID0gbnVsbDtcblx0XHRcdFx0aWYgKHByZXZNb250aC5nZXRVVENEYXkoKSA9PT0gdGhpcy5vLndlZWtFbmQpe1xuXHRcdFx0XHRcdGh0bWwucHVzaCgnPC90cj4nKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRwcmV2TW9udGguc2V0VVRDRGF0ZShwcmV2TW9udGguZ2V0VVRDRGF0ZSgpKzEpO1xuXHRcdFx0fVxuXHRcdFx0dGhpcy5waWNrZXIuZmluZCgnLmRhdGVwaWNrZXItZGF5cyB0Ym9keScpLmVtcHR5KCkuYXBwZW5kKGh0bWwuam9pbignJykpO1xuXG5cdFx0XHR2YXIgbW9udGhzVGl0bGUgPSBkYXRlc1t0aGlzLm8ubGFuZ3VhZ2VdLm1vbnRoc1RpdGxlIHx8IGRhdGVzWydlbiddLm1vbnRoc1RpdGxlIHx8ICdNb250aHMnO1xuXHRcdFx0dmFyIG1vbnRocyA9IHRoaXMucGlja2VyLmZpbmQoJy5kYXRlcGlja2VyLW1vbnRocycpXG5cdFx0XHRcdFx0XHQuZmluZCgnLmRhdGVwaWNrZXItc3dpdGNoJylcblx0XHRcdFx0XHRcdFx0LnRleHQodGhpcy5vLm1heFZpZXdNb2RlIDwgMiA/IG1vbnRoc1RpdGxlIDogeWVhcilcblx0XHRcdFx0XHRcdFx0LmVuZCgpXG5cdFx0XHRcdFx0XHQuZmluZCgnc3BhbicpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcblxuXHRcdFx0JC5lYWNoKHRoaXMuZGF0ZXMsIGZ1bmN0aW9uKGksIGQpe1xuXHRcdFx0XHRpZiAoZC5nZXRVVENGdWxsWWVhcigpID09PSB5ZWFyKVxuXHRcdFx0XHRcdG1vbnRocy5lcShkLmdldFVUQ01vbnRoKCkpLmFkZENsYXNzKCdhY3RpdmUnKTtcblx0XHRcdH0pO1xuXG5cdFx0XHRpZiAoeWVhciA8IHN0YXJ0WWVhciB8fCB5ZWFyID4gZW5kWWVhcil7XG5cdFx0XHRcdG1vbnRocy5hZGRDbGFzcygnZGlzYWJsZWQnKTtcblx0XHRcdH1cblx0XHRcdGlmICh5ZWFyID09PSBzdGFydFllYXIpe1xuXHRcdFx0XHRtb250aHMuc2xpY2UoMCwgc3RhcnRNb250aCkuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0XHR9XG5cdFx0XHRpZiAoeWVhciA9PT0gZW5kWWVhcil7XG5cdFx0XHRcdG1vbnRocy5zbGljZShlbmRNb250aCsxKS5hZGRDbGFzcygnZGlzYWJsZWQnKTtcblx0XHRcdH1cblxuXHRcdFx0aWYgKHRoaXMuby5iZWZvcmVTaG93TW9udGggIT09ICQubm9vcCl7XG5cdFx0XHRcdHZhciB0aGF0ID0gdGhpcztcblx0XHRcdFx0JC5lYWNoKG1vbnRocywgZnVuY3Rpb24oaSwgbW9udGgpe1xuICAgICAgICAgIHZhciBtb0RhdGUgPSBuZXcgRGF0ZSh5ZWFyLCBpLCAxKTtcbiAgICAgICAgICB2YXIgYmVmb3JlID0gdGhhdC5vLmJlZm9yZVNob3dNb250aChtb0RhdGUpO1xuXHRcdFx0XHRcdGlmIChiZWZvcmUgPT09IHVuZGVmaW5lZClcblx0XHRcdFx0XHRcdGJlZm9yZSA9IHt9O1xuXHRcdFx0XHRcdGVsc2UgaWYgKHR5cGVvZihiZWZvcmUpID09PSAnYm9vbGVhbicpXG5cdFx0XHRcdFx0XHRiZWZvcmUgPSB7ZW5hYmxlZDogYmVmb3JlfTtcblx0XHRcdFx0XHRlbHNlIGlmICh0eXBlb2YoYmVmb3JlKSA9PT0gJ3N0cmluZycpXG5cdFx0XHRcdFx0XHRiZWZvcmUgPSB7Y2xhc3NlczogYmVmb3JlfTtcblx0XHRcdFx0XHRpZiAoYmVmb3JlLmVuYWJsZWQgPT09IGZhbHNlICYmICEkKG1vbnRoKS5oYXNDbGFzcygnZGlzYWJsZWQnKSlcblx0XHRcdFx0XHQgICAgJChtb250aCkuYWRkQ2xhc3MoJ2Rpc2FibGVkJyk7XG5cdFx0XHRcdFx0aWYgKGJlZm9yZS5jbGFzc2VzKVxuXHRcdFx0XHRcdCAgICAkKG1vbnRoKS5hZGRDbGFzcyhiZWZvcmUuY2xhc3Nlcyk7XG5cdFx0XHRcdFx0aWYgKGJlZm9yZS50b29sdGlwKVxuXHRcdFx0XHRcdCAgICAkKG1vbnRoKS5wcm9wKCd0aXRsZScsIGJlZm9yZS50b29sdGlwKTtcblx0XHRcdFx0fSk7XG5cdFx0XHR9XG5cblx0XHRcdC8vIEdlbmVyYXRpbmcgZGVjYWRlL3llYXJzIHBpY2tlclxuXHRcdFx0dGhpcy5fZmlsbF95ZWFyc1ZpZXcoXG5cdFx0XHRcdCcuZGF0ZXBpY2tlci15ZWFycycsXG5cdFx0XHRcdCd5ZWFyJyxcblx0XHRcdFx0MTAsXG5cdFx0XHRcdDEsXG5cdFx0XHRcdHllYXIsXG5cdFx0XHRcdHN0YXJ0WWVhcixcblx0XHRcdFx0ZW5kWWVhcixcblx0XHRcdFx0dGhpcy5vLmJlZm9yZVNob3dZZWFyXG5cdFx0XHQpO1xuXG5cdFx0XHQvLyBHZW5lcmF0aW5nIGNlbnR1cnkvZGVjYWRlcyBwaWNrZXJcblx0XHRcdHRoaXMuX2ZpbGxfeWVhcnNWaWV3KFxuXHRcdFx0XHQnLmRhdGVwaWNrZXItZGVjYWRlcycsXG5cdFx0XHRcdCdkZWNhZGUnLFxuXHRcdFx0XHQxMDAsXG5cdFx0XHRcdDEwLFxuXHRcdFx0XHR5ZWFyLFxuXHRcdFx0XHRzdGFydFllYXIsXG5cdFx0XHRcdGVuZFllYXIsXG5cdFx0XHRcdHRoaXMuby5iZWZvcmVTaG93RGVjYWRlXG5cdFx0XHQpO1xuXG5cdFx0XHQvLyBHZW5lcmF0aW5nIG1pbGxlbm5pdW0vY2VudHVyaWVzIHBpY2tlclxuXHRcdFx0dGhpcy5fZmlsbF95ZWFyc1ZpZXcoXG5cdFx0XHRcdCcuZGF0ZXBpY2tlci1jZW50dXJpZXMnLFxuXHRcdFx0XHQnY2VudHVyeScsXG5cdFx0XHRcdDEwMDAsXG5cdFx0XHRcdDEwMCxcblx0XHRcdFx0eWVhcixcblx0XHRcdFx0c3RhcnRZZWFyLFxuXHRcdFx0XHRlbmRZZWFyLFxuXHRcdFx0XHR0aGlzLm8uYmVmb3JlU2hvd0NlbnR1cnlcblx0XHRcdCk7XG5cdFx0fSxcblxuXHRcdHVwZGF0ZU5hdkFycm93czogZnVuY3Rpb24oKXtcblx0XHRcdGlmICghdGhpcy5fYWxsb3dfdXBkYXRlKVxuXHRcdFx0XHRyZXR1cm47XG5cblx0XHRcdHZhciBkID0gbmV3IERhdGUodGhpcy52aWV3RGF0ZSksXG5cdFx0XHRcdHllYXIgPSBkLmdldFVUQ0Z1bGxZZWFyKCksXG5cdFx0XHRcdG1vbnRoID0gZC5nZXRVVENNb250aCgpO1xuXHRcdFx0c3dpdGNoICh0aGlzLnZpZXdNb2RlKXtcblx0XHRcdFx0Y2FzZSAwOlxuXHRcdFx0XHRcdGlmICh0aGlzLm8uc3RhcnREYXRlICE9PSAtSW5maW5pdHkgJiYgeWVhciA8PSB0aGlzLm8uc3RhcnREYXRlLmdldFVUQ0Z1bGxZZWFyKCkgJiYgbW9udGggPD0gdGhpcy5vLnN0YXJ0RGF0ZS5nZXRVVENNb250aCgpKXtcblx0XHRcdFx0XHRcdHRoaXMucGlja2VyLmZpbmQoJy5wcmV2JykuY3NzKHt2aXNpYmlsaXR5OiAnaGlkZGVuJ30pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMucGlja2VyLmZpbmQoJy5wcmV2JykuY3NzKHt2aXNpYmlsaXR5OiAndmlzaWJsZSd9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKHRoaXMuby5lbmREYXRlICE9PSBJbmZpbml0eSAmJiB5ZWFyID49IHRoaXMuby5lbmREYXRlLmdldFVUQ0Z1bGxZZWFyKCkgJiYgbW9udGggPj0gdGhpcy5vLmVuZERhdGUuZ2V0VVRDTW9udGgoKSl7XG5cdFx0XHRcdFx0XHR0aGlzLnBpY2tlci5maW5kKCcubmV4dCcpLmNzcyh7dmlzaWJpbGl0eTogJ2hpZGRlbid9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLnBpY2tlci5maW5kKCcubmV4dCcpLmNzcyh7dmlzaWJpbGl0eTogJ3Zpc2libGUnfSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlIDE6XG5cdFx0XHRcdGNhc2UgMjpcblx0XHRcdFx0Y2FzZSAzOlxuXHRcdFx0XHRjYXNlIDQ6XG5cdFx0XHRcdFx0aWYgKHRoaXMuby5zdGFydERhdGUgIT09IC1JbmZpbml0eSAmJiB5ZWFyIDw9IHRoaXMuby5zdGFydERhdGUuZ2V0VVRDRnVsbFllYXIoKSB8fCB0aGlzLm8ubWF4Vmlld01vZGUgPCAyKXtcblx0XHRcdFx0XHRcdHRoaXMucGlja2VyLmZpbmQoJy5wcmV2JykuY3NzKHt2aXNpYmlsaXR5OiAnaGlkZGVuJ30pO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMucGlja2VyLmZpbmQoJy5wcmV2JykuY3NzKHt2aXNpYmlsaXR5OiAndmlzaWJsZSd9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKHRoaXMuby5lbmREYXRlICE9PSBJbmZpbml0eSAmJiB5ZWFyID49IHRoaXMuby5lbmREYXRlLmdldFVUQ0Z1bGxZZWFyKCkgfHwgdGhpcy5vLm1heFZpZXdNb2RlIDwgMil7XG5cdFx0XHRcdFx0XHR0aGlzLnBpY2tlci5maW5kKCcubmV4dCcpLmNzcyh7dmlzaWJpbGl0eTogJ2hpZGRlbid9KTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0ZWxzZSB7XG5cdFx0XHRcdFx0XHR0aGlzLnBpY2tlci5maW5kKCcubmV4dCcpLmNzcyh7dmlzaWJpbGl0eTogJ3Zpc2libGUnfSk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0fVxuXHRcdH0sXG5cblx0XHRjbGljazogZnVuY3Rpb24oZSl7XG5cdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRlLnN0b3BQcm9wYWdhdGlvbigpO1xuXG5cdFx0XHR2YXIgdGFyZ2V0LCBkaXIsIGRheSwgeWVhciwgbW9udGgsIG1vbnRoQ2hhbmdlZCwgeWVhckNoYW5nZWQ7XG5cdFx0XHR0YXJnZXQgPSAkKGUudGFyZ2V0KTtcblxuXHRcdFx0Ly8gQ2xpY2tlZCBvbiB0aGUgc3dpdGNoXG5cdFx0XHRpZiAodGFyZ2V0Lmhhc0NsYXNzKCdkYXRlcGlja2VyLXN3aXRjaCcpKXtcblx0XHRcdFx0dGhpcy5zaG93TW9kZSgxKTtcblx0XHRcdH1cblxuXHRcdFx0Ly8gQ2xpY2tlZCBvbiBwcmV2IG9yIG5leHRcblx0XHRcdHZhciBuYXZBcnJvdyA9IHRhcmdldC5jbG9zZXN0KCcucHJldiwgLm5leHQnKTtcblx0XHRcdGlmIChuYXZBcnJvdy5sZW5ndGggPiAwKSB7XG5cdFx0XHRcdGRpciA9IERQR2xvYmFsLm1vZGVzW3RoaXMudmlld01vZGVdLm5hdlN0ZXAgKiAobmF2QXJyb3cuaGFzQ2xhc3MoJ3ByZXYnKSA/IC0xIDogMSk7XG5cdFx0XHRcdGlmICh0aGlzLnZpZXdNb2RlID09PSAwKXtcblx0XHRcdFx0XHR0aGlzLnZpZXdEYXRlID0gdGhpcy5tb3ZlTW9udGgodGhpcy52aWV3RGF0ZSwgZGlyKTtcblx0XHRcdFx0XHR0aGlzLl90cmlnZ2VyKCdjaGFuZ2VNb250aCcsIHRoaXMudmlld0RhdGUpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHRoaXMudmlld0RhdGUgPSB0aGlzLm1vdmVZZWFyKHRoaXMudmlld0RhdGUsIGRpcik7XG5cdFx0XHRcdFx0aWYgKHRoaXMudmlld01vZGUgPT09IDEpe1xuXHRcdFx0XHRcdFx0dGhpcy5fdHJpZ2dlcignY2hhbmdlWWVhcicsIHRoaXMudmlld0RhdGUpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHR0aGlzLmZpbGwoKTtcblx0XHRcdH1cblxuXHRcdFx0Ly8gQ2xpY2tlZCBvbiB0b2RheSBidXR0b25cblx0XHRcdGlmICh0YXJnZXQuaGFzQ2xhc3MoJ3RvZGF5JykgJiYgIXRhcmdldC5oYXNDbGFzcygnZGF5Jykpe1xuXHRcdFx0XHR0aGlzLnNob3dNb2RlKC0yKTtcblx0XHRcdFx0dGhpcy5fc2V0RGF0ZShVVENUb2RheSgpLCB0aGlzLm8udG9kYXlCdG4gPT09ICdsaW5rZWQnID8gbnVsbCA6ICd2aWV3Jyk7XG5cdFx0XHR9XG5cblx0XHRcdC8vIENsaWNrZWQgb24gY2xlYXIgYnV0dG9uXG5cdFx0XHRpZiAodGFyZ2V0Lmhhc0NsYXNzKCdjbGVhcicpKXtcblx0XHRcdFx0dGhpcy5jbGVhckRhdGVzKCk7XG5cdFx0XHR9XG5cblx0XHRcdGlmICghdGFyZ2V0Lmhhc0NsYXNzKCdkaXNhYmxlZCcpKXtcblx0XHRcdFx0Ly8gQ2xpY2tlZCBvbiBhIGRheVxuXHRcdFx0XHRpZiAodGFyZ2V0Lmhhc0NsYXNzKCdkYXknKSl7XG5cdFx0XHRcdFx0ZGF5ID0gcGFyc2VJbnQodGFyZ2V0LnRleHQoKSwgMTApIHx8IDE7XG5cdFx0XHRcdFx0eWVhciA9IHRoaXMudmlld0RhdGUuZ2V0VVRDRnVsbFllYXIoKTtcblx0XHRcdFx0XHRtb250aCA9IHRoaXMudmlld0RhdGUuZ2V0VVRDTW9udGgoKTtcblxuXHRcdFx0XHRcdC8vIEZyb20gbGFzdCBtb250aFxuXHRcdFx0XHRcdGlmICh0YXJnZXQuaGFzQ2xhc3MoJ29sZCcpKXtcblx0XHRcdFx0XHRcdGlmIChtb250aCA9PT0gMCkge1xuXHRcdFx0XHRcdFx0XHRtb250aCA9IDExO1xuXHRcdFx0XHRcdFx0XHR5ZWFyID0geWVhciAtIDE7XG5cdFx0XHRcdFx0XHRcdG1vbnRoQ2hhbmdlZCA9IHRydWU7XG5cdFx0XHRcdFx0XHRcdHllYXJDaGFuZ2VkID0gdHJ1ZTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdG1vbnRoID0gbW9udGggLSAxO1xuXHRcdFx0XHRcdFx0XHRtb250aENoYW5nZWQgPSB0cnVlO1xuIFx0XHRcdFx0XHRcdH1cbiBcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0Ly8gRnJvbSBuZXh0IG1vbnRoXG5cdFx0XHRcdFx0aWYgKHRhcmdldC5oYXNDbGFzcygnbmV3JykpIHtcblx0XHRcdFx0XHRcdGlmIChtb250aCA9PT0gMTEpe1xuXHRcdFx0XHRcdFx0XHRtb250aCA9IDA7XG5cdFx0XHRcdFx0XHRcdHllYXIgPSB5ZWFyICsgMTtcblx0XHRcdFx0XHRcdFx0bW9udGhDaGFuZ2VkID0gdHJ1ZTtcblx0XHRcdFx0XHRcdFx0eWVhckNoYW5nZWQgPSB0cnVlO1xuIFx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdG1vbnRoID0gbW9udGggKyAxO1xuXHRcdFx0XHRcdFx0XHRtb250aENoYW5nZWQgPSB0cnVlO1xuIFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dGhpcy5fc2V0RGF0ZShVVENEYXRlKHllYXIsIG1vbnRoLCBkYXkpKTtcblx0XHRcdFx0XHRpZiAoeWVhckNoYW5nZWQpIHtcblx0XHRcdFx0XHRcdHRoaXMuX3RyaWdnZXIoJ2NoYW5nZVllYXInLCB0aGlzLnZpZXdEYXRlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aWYgKG1vbnRoQ2hhbmdlZCkge1xuXHRcdFx0XHRcdFx0dGhpcy5fdHJpZ2dlcignY2hhbmdlTW9udGgnLCB0aGlzLnZpZXdEYXRlKTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdH1cblxuXHRcdFx0XHQvLyBDbGlja2VkIG9uIGEgbW9udGhcblx0XHRcdFx0aWYgKHRhcmdldC5oYXNDbGFzcygnbW9udGgnKSkge1xuXHRcdFx0XHRcdHRoaXMudmlld0RhdGUuc2V0VVRDRGF0ZSgxKTtcblx0XHRcdFx0XHRkYXkgPSAxO1xuXHRcdFx0XHRcdG1vbnRoID0gdGFyZ2V0LnBhcmVudCgpLmZpbmQoJ3NwYW4nKS5pbmRleCh0YXJnZXQpO1xuXHRcdFx0XHRcdHllYXIgPSB0aGlzLnZpZXdEYXRlLmdldFVUQ0Z1bGxZZWFyKCk7XG5cdFx0XHRcdFx0dGhpcy52aWV3RGF0ZS5zZXRVVENNb250aChtb250aCk7XG5cdFx0XHRcdFx0dGhpcy5fdHJpZ2dlcignY2hhbmdlTW9udGgnLCB0aGlzLnZpZXdEYXRlKTtcblx0XHRcdFx0XHRpZiAodGhpcy5vLm1pblZpZXdNb2RlID09PSAxKXtcblx0XHRcdFx0XHRcdHRoaXMuX3NldERhdGUoVVRDRGF0ZSh5ZWFyLCBtb250aCwgZGF5KSk7XG5cdFx0XHRcdFx0XHR0aGlzLnNob3dNb2RlKCk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHRoaXMuc2hvd01vZGUoLTEpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHR0aGlzLmZpbGwoKTtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdC8vIENsaWNrZWQgb24gYSB5ZWFyXG5cdFx0XHRcdGlmICh0YXJnZXQuaGFzQ2xhc3MoJ3llYXInKVxuXHRcdFx0XHRcdFx0fHwgdGFyZ2V0Lmhhc0NsYXNzKCdkZWNhZGUnKVxuXHRcdFx0XHRcdFx0fHwgdGFyZ2V0Lmhhc0NsYXNzKCdjZW50dXJ5JykpIHtcblx0XHRcdFx0XHR0aGlzLnZpZXdEYXRlLnNldFVUQ0RhdGUoMSk7XG5cblx0XHRcdFx0XHRkYXkgPSAxO1xuXHRcdFx0XHRcdG1vbnRoID0gMDtcblx0XHRcdFx0XHR5ZWFyID0gcGFyc2VJbnQodGFyZ2V0LnRleHQoKSwgMTApfHwwO1xuXHRcdFx0XHRcdHRoaXMudmlld0RhdGUuc2V0VVRDRnVsbFllYXIoeWVhcik7XG5cblx0XHRcdFx0XHRpZiAodGFyZ2V0Lmhhc0NsYXNzKCd5ZWFyJykpe1xuXHRcdFx0XHRcdFx0dGhpcy5fdHJpZ2dlcignY2hhbmdlWWVhcicsIHRoaXMudmlld0RhdGUpO1xuXHRcdFx0XHRcdFx0aWYgKHRoaXMuby5taW5WaWV3TW9kZSA9PT0gMil7XG5cdFx0XHRcdFx0XHRcdHRoaXMuX3NldERhdGUoVVRDRGF0ZSh5ZWFyLCBtb250aCwgZGF5KSk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGlmICh0YXJnZXQuaGFzQ2xhc3MoJ2RlY2FkZScpKXtcblx0XHRcdFx0XHRcdHRoaXMuX3RyaWdnZXIoJ2NoYW5nZURlY2FkZScsIHRoaXMudmlld0RhdGUpO1xuXHRcdFx0XHRcdFx0aWYgKHRoaXMuby5taW5WaWV3TW9kZSA9PT0gMyl7XG5cdFx0XHRcdFx0XHRcdHRoaXMuX3NldERhdGUoVVRDRGF0ZSh5ZWFyLCBtb250aCwgZGF5KSk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGlmICh0YXJnZXQuaGFzQ2xhc3MoJ2NlbnR1cnknKSl7XG5cdFx0XHRcdFx0XHR0aGlzLl90cmlnZ2VyKCdjaGFuZ2VDZW50dXJ5JywgdGhpcy52aWV3RGF0ZSk7XG5cdFx0XHRcdFx0XHRpZiAodGhpcy5vLm1pblZpZXdNb2RlID09PSA0KXtcblx0XHRcdFx0XHRcdFx0dGhpcy5fc2V0RGF0ZShVVENEYXRlKHllYXIsIG1vbnRoLCBkYXkpKTtcblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHR0aGlzLnNob3dNb2RlKC0xKTtcblx0XHRcdFx0XHR0aGlzLmZpbGwoKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRpZiAodGhpcy5waWNrZXIuaXMoJzp2aXNpYmxlJykgJiYgdGhpcy5fZm9jdXNlZF9mcm9tKXtcblx0XHRcdFx0JCh0aGlzLl9mb2N1c2VkX2Zyb20pLmZvY3VzKCk7XG5cdFx0XHR9XG5cdFx0XHRkZWxldGUgdGhpcy5fZm9jdXNlZF9mcm9tO1xuXHRcdH0sXG5cblx0XHRfdG9nZ2xlX211bHRpZGF0ZTogZnVuY3Rpb24oZGF0ZSl7XG5cdFx0XHR2YXIgaXggPSB0aGlzLmRhdGVzLmNvbnRhaW5zKGRhdGUpO1xuXHRcdFx0aWYgKCFkYXRlKXtcblx0XHRcdFx0dGhpcy5kYXRlcy5jbGVhcigpO1xuXHRcdFx0fVxuXG5cdFx0XHRpZiAoaXggIT09IC0xKXtcblx0XHRcdFx0aWYgKHRoaXMuby5tdWx0aWRhdGUgPT09IHRydWUgfHwgdGhpcy5vLm11bHRpZGF0ZSA+IDEgfHwgdGhpcy5vLnRvZ2dsZUFjdGl2ZSl7XG5cdFx0XHRcdFx0dGhpcy5kYXRlcy5yZW1vdmUoaXgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9IGVsc2UgaWYgKHRoaXMuby5tdWx0aWRhdGUgPT09IGZhbHNlKSB7XG5cdFx0XHRcdHRoaXMuZGF0ZXMuY2xlYXIoKTtcblx0XHRcdFx0dGhpcy5kYXRlcy5wdXNoKGRhdGUpO1xuXHRcdFx0fVxuXHRcdFx0ZWxzZSB7XG5cdFx0XHRcdHRoaXMuZGF0ZXMucHVzaChkYXRlKTtcblx0XHRcdH1cblxuXHRcdFx0aWYgKHR5cGVvZiB0aGlzLm8ubXVsdGlkYXRlID09PSAnbnVtYmVyJylcblx0XHRcdFx0d2hpbGUgKHRoaXMuZGF0ZXMubGVuZ3RoID4gdGhpcy5vLm11bHRpZGF0ZSlcblx0XHRcdFx0XHR0aGlzLmRhdGVzLnJlbW92ZSgwKTtcblx0XHR9LFxuXG5cdFx0X3NldERhdGU6IGZ1bmN0aW9uKGRhdGUsIHdoaWNoKXtcblx0XHRcdGlmICghd2hpY2ggfHwgd2hpY2ggPT09ICdkYXRlJylcblx0XHRcdFx0dGhpcy5fdG9nZ2xlX211bHRpZGF0ZShkYXRlICYmIG5ldyBEYXRlKGRhdGUpKTtcblx0XHRcdGlmICghd2hpY2ggfHwgd2hpY2ggPT09ICd2aWV3Jylcblx0XHRcdFx0dGhpcy52aWV3RGF0ZSA9IGRhdGUgJiYgbmV3IERhdGUoZGF0ZSk7XG5cblx0XHRcdHRoaXMuZmlsbCgpO1xuXHRcdFx0dGhpcy5zZXRWYWx1ZSgpO1xuXHRcdFx0aWYgKCF3aGljaCB8fCB3aGljaCAhPT0gJ3ZpZXcnKSB7XG5cdFx0XHRcdHRoaXMuX3RyaWdnZXIoJ2NoYW5nZURhdGUnKTtcblx0XHRcdH1cblx0XHRcdGlmICh0aGlzLmlucHV0RmllbGQpe1xuXHRcdFx0XHR0aGlzLmlucHV0RmllbGQuY2hhbmdlKCk7XG5cdFx0XHR9XG5cdFx0XHRpZiAodGhpcy5vLmF1dG9jbG9zZSAmJiAoIXdoaWNoIHx8IHdoaWNoID09PSAnZGF0ZScpKXtcblx0XHRcdFx0dGhpcy5oaWRlKCk7XG5cdFx0XHR9XG5cdFx0fSxcblxuXHRcdG1vdmVEYXk6IGZ1bmN0aW9uKGRhdGUsIGRpcil7XG5cdFx0XHR2YXIgbmV3RGF0ZSA9IG5ldyBEYXRlKGRhdGUpO1xuXHRcdFx0bmV3RGF0ZS5zZXRVVENEYXRlKGRhdGUuZ2V0VVRDRGF0ZSgpICsgZGlyKTtcblxuXHRcdFx0cmV0dXJuIG5ld0RhdGU7XG5cdFx0fSxcblxuXHRcdG1vdmVXZWVrOiBmdW5jdGlvbihkYXRlLCBkaXIpe1xuXHRcdFx0cmV0dXJuIHRoaXMubW92ZURheShkYXRlLCBkaXIgKiA3KTtcblx0XHR9LFxuXG5cdFx0bW92ZU1vbnRoOiBmdW5jdGlvbihkYXRlLCBkaXIpe1xuXHRcdFx0aWYgKCFpc1ZhbGlkRGF0ZShkYXRlKSlcblx0XHRcdFx0cmV0dXJuIHRoaXMuby5kZWZhdWx0Vmlld0RhdGU7XG5cdFx0XHRpZiAoIWRpcilcblx0XHRcdFx0cmV0dXJuIGRhdGU7XG5cdFx0XHR2YXIgbmV3X2RhdGUgPSBuZXcgRGF0ZShkYXRlLnZhbHVlT2YoKSksXG5cdFx0XHRcdGRheSA9IG5ld19kYXRlLmdldFVUQ0RhdGUoKSxcblx0XHRcdFx0bW9udGggPSBuZXdfZGF0ZS5nZXRVVENNb250aCgpLFxuXHRcdFx0XHRtYWcgPSBNYXRoLmFicyhkaXIpLFxuXHRcdFx0XHRuZXdfbW9udGgsIHRlc3Q7XG5cdFx0XHRkaXIgPSBkaXIgPiAwID8gMSA6IC0xO1xuXHRcdFx0aWYgKG1hZyA9PT0gMSl7XG5cdFx0XHRcdHRlc3QgPSBkaXIgPT09IC0xXG5cdFx0XHRcdFx0Ly8gSWYgZ29pbmcgYmFjayBvbmUgbW9udGgsIG1ha2Ugc3VyZSBtb250aCBpcyBub3QgY3VycmVudCBtb250aFxuXHRcdFx0XHRcdC8vIChlZywgTWFyIDMxIC0+IEZlYiAzMSA9PSBGZWIgMjgsIG5vdCBNYXIgMDIpXG5cdFx0XHRcdFx0PyBmdW5jdGlvbigpe1xuXHRcdFx0XHRcdFx0cmV0dXJuIG5ld19kYXRlLmdldFVUQ01vbnRoKCkgPT09IG1vbnRoO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHQvLyBJZiBnb2luZyBmb3J3YXJkIG9uZSBtb250aCwgbWFrZSBzdXJlIG1vbnRoIGlzIGFzIGV4cGVjdGVkXG5cdFx0XHRcdFx0Ly8gKGVnLCBKYW4gMzEgLT4gRmViIDMxID09IEZlYiAyOCwgbm90IE1hciAwMilcblx0XHRcdFx0XHQ6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0XHRyZXR1cm4gbmV3X2RhdGUuZ2V0VVRDTW9udGgoKSAhPT0gbmV3X21vbnRoO1xuXHRcdFx0XHRcdH07XG5cdFx0XHRcdG5ld19tb250aCA9IG1vbnRoICsgZGlyO1xuXHRcdFx0XHRuZXdfZGF0ZS5zZXRVVENNb250aChuZXdfbW9udGgpO1xuXHRcdFx0XHQvLyBEZWMgLT4gSmFuICgxMikgb3IgSmFuIC0+IERlYyAoLTEpIC0tIGxpbWl0IGV4cGVjdGVkIGRhdGUgdG8gMC0xMVxuXHRcdFx0XHRpZiAobmV3X21vbnRoIDwgMCB8fCBuZXdfbW9udGggPiAxMSlcblx0XHRcdFx0XHRuZXdfbW9udGggPSAobmV3X21vbnRoICsgMTIpICUgMTI7XG5cdFx0XHR9XG5cdFx0XHRlbHNlIHtcblx0XHRcdFx0Ly8gRm9yIG1hZ25pdHVkZXMgPjEsIG1vdmUgb25lIG1vbnRoIGF0IGEgdGltZS4uLlxuXHRcdFx0XHRmb3IgKHZhciBpPTA7IGkgPCBtYWc7IGkrKylcblx0XHRcdFx0XHQvLyAuLi53aGljaCBtaWdodCBkZWNyZWFzZSB0aGUgZGF5IChlZywgSmFuIDMxIHRvIEZlYiAyOCwgZXRjKS4uLlxuXHRcdFx0XHRcdG5ld19kYXRlID0gdGhpcy5tb3ZlTW9udGgobmV3X2RhdGUsIGRpcik7XG5cdFx0XHRcdC8vIC4uLnRoZW4gcmVzZXQgdGhlIGRheSwga2VlcGluZyBpdCBpbiB0aGUgbmV3IG1vbnRoXG5cdFx0XHRcdG5ld19tb250aCA9IG5ld19kYXRlLmdldFVUQ01vbnRoKCk7XG5cdFx0XHRcdG5ld19kYXRlLnNldFVUQ0RhdGUoZGF5KTtcblx0XHRcdFx0dGVzdCA9IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0cmV0dXJuIG5ld19tb250aCAhPT0gbmV3X2RhdGUuZ2V0VVRDTW9udGgoKTtcblx0XHRcdFx0fTtcblx0XHRcdH1cblx0XHRcdC8vIENvbW1vbiBkYXRlLXJlc2V0dGluZyBsb29wIC0tIGlmIGRhdGUgaXMgYmV5b25kIGVuZCBvZiBtb250aCwgbWFrZSBpdFxuXHRcdFx0Ly8gZW5kIG9mIG1vbnRoXG5cdFx0XHR3aGlsZSAodGVzdCgpKXtcblx0XHRcdFx0bmV3X2RhdGUuc2V0VVRDRGF0ZSgtLWRheSk7XG5cdFx0XHRcdG5ld19kYXRlLnNldFVUQ01vbnRoKG5ld19tb250aCk7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gbmV3X2RhdGU7XG5cdFx0fSxcblxuXHRcdG1vdmVZZWFyOiBmdW5jdGlvbihkYXRlLCBkaXIpe1xuXHRcdFx0cmV0dXJuIHRoaXMubW92ZU1vbnRoKGRhdGUsIGRpcioxMik7XG5cdFx0fSxcblxuXHRcdG1vdmVBdmFpbGFibGVEYXRlOiBmdW5jdGlvbihkYXRlLCBkaXIsIGZuKXtcblx0XHRcdGRvIHtcblx0XHRcdFx0ZGF0ZSA9IHRoaXNbZm5dKGRhdGUsIGRpcik7XG5cblx0XHRcdFx0aWYgKCF0aGlzLmRhdGVXaXRoaW5SYW5nZShkYXRlKSlcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cblx0XHRcdFx0Zm4gPSAnbW92ZURheSc7XG5cdFx0XHR9XG5cdFx0XHR3aGlsZSAodGhpcy5kYXRlSXNEaXNhYmxlZChkYXRlKSk7XG5cblx0XHRcdHJldHVybiBkYXRlO1xuXHRcdH0sXG5cblx0XHR3ZWVrT2ZEYXRlSXNEaXNhYmxlZDogZnVuY3Rpb24oZGF0ZSl7XG5cdFx0XHRyZXR1cm4gJC5pbkFycmF5KGRhdGUuZ2V0VVRDRGF5KCksIHRoaXMuby5kYXlzT2ZXZWVrRGlzYWJsZWQpICE9PSAtMTtcblx0XHR9LFxuXG5cdFx0ZGF0ZUlzRGlzYWJsZWQ6IGZ1bmN0aW9uKGRhdGUpe1xuXHRcdFx0cmV0dXJuIChcblx0XHRcdFx0dGhpcy53ZWVrT2ZEYXRlSXNEaXNhYmxlZChkYXRlKSB8fFxuXHRcdFx0XHQkLmdyZXAodGhpcy5vLmRhdGVzRGlzYWJsZWQsIGZ1bmN0aW9uKGQpe1xuXHRcdFx0XHRcdHJldHVybiBpc1VUQ0VxdWFscyhkYXRlLCBkKTtcblx0XHRcdFx0fSkubGVuZ3RoID4gMFxuXHRcdFx0KTtcblx0XHR9LFxuXG5cdFx0ZGF0ZVdpdGhpblJhbmdlOiBmdW5jdGlvbihkYXRlKXtcblx0XHRcdHJldHVybiBkYXRlID49IHRoaXMuby5zdGFydERhdGUgJiYgZGF0ZSA8PSB0aGlzLm8uZW5kRGF0ZTtcblx0XHR9LFxuXG5cdFx0a2V5ZG93bjogZnVuY3Rpb24oZSl7XG5cdFx0XHRpZiAoIXRoaXMucGlja2VyLmlzKCc6dmlzaWJsZScpKXtcblx0XHRcdFx0aWYgKGUua2V5Q29kZSA9PT0gNDAgfHwgZS5rZXlDb2RlID09PSAyNykgeyAvLyBhbGxvdyBkb3duIHRvIHJlLXNob3cgcGlja2VyXG5cdFx0XHRcdFx0dGhpcy5zaG93KCk7XG5cdFx0XHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgfVxuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cdFx0XHR2YXIgZGF0ZUNoYW5nZWQgPSBmYWxzZSxcblx0XHRcdFx0ZGlyLCBuZXdWaWV3RGF0ZSxcblx0XHRcdFx0Zm9jdXNEYXRlID0gdGhpcy5mb2N1c0RhdGUgfHwgdGhpcy52aWV3RGF0ZTtcblx0XHRcdHN3aXRjaCAoZS5rZXlDb2RlKXtcblx0XHRcdFx0Y2FzZSAyNzogLy8gZXNjYXBlXG5cdFx0XHRcdFx0aWYgKHRoaXMuZm9jdXNEYXRlKXtcblx0XHRcdFx0XHRcdHRoaXMuZm9jdXNEYXRlID0gbnVsbDtcblx0XHRcdFx0XHRcdHRoaXMudmlld0RhdGUgPSB0aGlzLmRhdGVzLmdldCgtMSkgfHwgdGhpcy52aWV3RGF0ZTtcblx0XHRcdFx0XHRcdHRoaXMuZmlsbCgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0XHR0aGlzLmhpZGUoKTtcblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0ZS5zdG9wUHJvcGFnYXRpb24oKTtcblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAzNzogLy8gbGVmdFxuXHRcdFx0XHRjYXNlIDM4OiAvLyB1cFxuXHRcdFx0XHRjYXNlIDM5OiAvLyByaWdodFxuXHRcdFx0XHRjYXNlIDQwOiAvLyBkb3duXG5cdFx0XHRcdFx0aWYgKCF0aGlzLm8ua2V5Ym9hcmROYXZpZ2F0aW9uIHx8IHRoaXMuby5kYXlzT2ZXZWVrRGlzYWJsZWQubGVuZ3RoID09PSA3KVxuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0ZGlyID0gZS5rZXlDb2RlID09PSAzNyB8fCBlLmtleUNvZGUgPT09IDM4ID8gLTEgOiAxO1xuICAgICAgICAgIGlmICh0aGlzLnZpZXdNb2RlID09PSAwKSB7XG4gIFx0XHRcdFx0XHRpZiAoZS5jdHJsS2V5KXtcbiAgXHRcdFx0XHRcdFx0bmV3Vmlld0RhdGUgPSB0aGlzLm1vdmVBdmFpbGFibGVEYXRlKGZvY3VzRGF0ZSwgZGlyLCAnbW92ZVllYXInKTtcblxuICBcdFx0XHRcdFx0XHRpZiAobmV3Vmlld0RhdGUpXG4gIFx0XHRcdFx0XHRcdFx0dGhpcy5fdHJpZ2dlcignY2hhbmdlWWVhcicsIHRoaXMudmlld0RhdGUpO1xuICBcdFx0XHRcdFx0fVxuICBcdFx0XHRcdFx0ZWxzZSBpZiAoZS5zaGlmdEtleSl7XG4gIFx0XHRcdFx0XHRcdG5ld1ZpZXdEYXRlID0gdGhpcy5tb3ZlQXZhaWxhYmxlRGF0ZShmb2N1c0RhdGUsIGRpciwgJ21vdmVNb250aCcpO1xuXG4gIFx0XHRcdFx0XHRcdGlmIChuZXdWaWV3RGF0ZSlcbiAgXHRcdFx0XHRcdFx0XHR0aGlzLl90cmlnZ2VyKCdjaGFuZ2VNb250aCcsIHRoaXMudmlld0RhdGUpO1xuICBcdFx0XHRcdFx0fVxuICBcdFx0XHRcdFx0ZWxzZSBpZiAoZS5rZXlDb2RlID09PSAzNyB8fCBlLmtleUNvZGUgPT09IDM5KXtcbiAgXHRcdFx0XHRcdFx0bmV3Vmlld0RhdGUgPSB0aGlzLm1vdmVBdmFpbGFibGVEYXRlKGZvY3VzRGF0ZSwgZGlyLCAnbW92ZURheScpO1xuICBcdFx0XHRcdFx0fVxuICBcdFx0XHRcdFx0ZWxzZSBpZiAoIXRoaXMud2Vla09mRGF0ZUlzRGlzYWJsZWQoZm9jdXNEYXRlKSl7XG4gIFx0XHRcdFx0XHRcdG5ld1ZpZXdEYXRlID0gdGhpcy5tb3ZlQXZhaWxhYmxlRGF0ZShmb2N1c0RhdGUsIGRpciwgJ21vdmVXZWVrJyk7XG4gIFx0XHRcdFx0XHR9XG4gICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnZpZXdNb2RlID09PSAxKSB7XG4gICAgICAgICAgICBpZiAoZS5rZXlDb2RlID09PSAzOCB8fCBlLmtleUNvZGUgPT09IDQwKSB7XG4gICAgICAgICAgICAgIGRpciA9IGRpciAqIDQ7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBuZXdWaWV3RGF0ZSA9IHRoaXMubW92ZUF2YWlsYWJsZURhdGUoZm9jdXNEYXRlLCBkaXIsICdtb3ZlTW9udGgnKTtcbiAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudmlld01vZGUgPT09IDIpIHtcbiAgICAgICAgICAgIGlmIChlLmtleUNvZGUgPT09IDM4IHx8IGUua2V5Q29kZSA9PT0gNDApIHtcbiAgICAgICAgICAgICAgZGlyID0gZGlyICogNDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG5ld1ZpZXdEYXRlID0gdGhpcy5tb3ZlQXZhaWxhYmxlRGF0ZShmb2N1c0RhdGUsIGRpciwgJ21vdmVZZWFyJyk7XG4gICAgICAgICAgfVxuXHRcdFx0XHRcdGlmIChuZXdWaWV3RGF0ZSl7XG5cdFx0XHRcdFx0XHR0aGlzLmZvY3VzRGF0ZSA9IHRoaXMudmlld0RhdGUgPSBuZXdWaWV3RGF0ZTtcblx0XHRcdFx0XHRcdHRoaXMuc2V0VmFsdWUoKTtcblx0XHRcdFx0XHRcdHRoaXMuZmlsbCgpO1xuXHRcdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRicmVhaztcblx0XHRcdFx0Y2FzZSAxMzogLy8gZW50ZXJcblx0XHRcdFx0XHRpZiAoIXRoaXMuby5mb3JjZVBhcnNlKVxuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0Zm9jdXNEYXRlID0gdGhpcy5mb2N1c0RhdGUgfHwgdGhpcy5kYXRlcy5nZXQoLTEpIHx8IHRoaXMudmlld0RhdGU7XG5cdFx0XHRcdFx0aWYgKHRoaXMuby5rZXlib2FyZE5hdmlnYXRpb24pIHtcblx0XHRcdFx0XHRcdHRoaXMuX3RvZ2dsZV9tdWx0aWRhdGUoZm9jdXNEYXRlKTtcblx0XHRcdFx0XHRcdGRhdGVDaGFuZ2VkID0gdHJ1ZTtcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0dGhpcy5mb2N1c0RhdGUgPSBudWxsO1xuXHRcdFx0XHRcdHRoaXMudmlld0RhdGUgPSB0aGlzLmRhdGVzLmdldCgtMSkgfHwgdGhpcy52aWV3RGF0ZTtcblx0XHRcdFx0XHR0aGlzLnNldFZhbHVlKCk7XG5cdFx0XHRcdFx0dGhpcy5maWxsKCk7XG5cdFx0XHRcdFx0aWYgKHRoaXMucGlja2VyLmlzKCc6dmlzaWJsZScpKXtcblx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHRcdGUuc3RvcFByb3BhZ2F0aW9uKCk7XG5cdFx0XHRcdFx0XHRpZiAodGhpcy5vLmF1dG9jbG9zZSlcblx0XHRcdFx0XHRcdFx0dGhpcy5oaWRlKCk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRjYXNlIDk6IC8vIHRhYlxuXHRcdFx0XHRcdHRoaXMuZm9jdXNEYXRlID0gbnVsbDtcblx0XHRcdFx0XHR0aGlzLnZpZXdEYXRlID0gdGhpcy5kYXRlcy5nZXQoLTEpIHx8IHRoaXMudmlld0RhdGU7XG5cdFx0XHRcdFx0dGhpcy5maWxsKCk7XG5cdFx0XHRcdFx0dGhpcy5oaWRlKCk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHR9XG5cdFx0XHRpZiAoZGF0ZUNoYW5nZWQpe1xuXHRcdFx0XHRpZiAodGhpcy5kYXRlcy5sZW5ndGgpXG5cdFx0XHRcdFx0dGhpcy5fdHJpZ2dlcignY2hhbmdlRGF0ZScpO1xuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0dGhpcy5fdHJpZ2dlcignY2xlYXJEYXRlJyk7XG5cdFx0XHRcdGlmICh0aGlzLmlucHV0RmllbGQpe1xuXHRcdFx0XHRcdHRoaXMuaW5wdXRGaWVsZC5jaGFuZ2UoKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0sXG5cblx0XHRzaG93TW9kZTogZnVuY3Rpb24oZGlyKXtcblx0XHRcdGlmIChkaXIpe1xuXHRcdFx0XHR0aGlzLnZpZXdNb2RlID0gTWF0aC5tYXgodGhpcy5vLm1pblZpZXdNb2RlLCBNYXRoLm1pbih0aGlzLm8ubWF4Vmlld01vZGUsIHRoaXMudmlld01vZGUgKyBkaXIpKTtcblx0XHRcdH1cblx0XHRcdHRoaXMucGlja2VyXG5cdFx0XHRcdC5jaGlsZHJlbignZGl2Jylcblx0XHRcdFx0LmhpZGUoKVxuXHRcdFx0XHQuZmlsdGVyKCcuZGF0ZXBpY2tlci0nICsgRFBHbG9iYWwubW9kZXNbdGhpcy52aWV3TW9kZV0uY2xzTmFtZSlcblx0XHRcdFx0XHQuc2hvdygpO1xuXHRcdFx0dGhpcy51cGRhdGVOYXZBcnJvd3MoKTtcblx0XHR9XG5cdH07XG5cblx0dmFyIERhdGVSYW5nZVBpY2tlciA9IGZ1bmN0aW9uKGVsZW1lbnQsIG9wdGlvbnMpe1xuXHRcdCQoZWxlbWVudCkuZGF0YSgnZGF0ZXBpY2tlcicsIHRoaXMpO1xuXHRcdHRoaXMuZWxlbWVudCA9ICQoZWxlbWVudCk7XG5cdFx0dGhpcy5pbnB1dHMgPSAkLm1hcChvcHRpb25zLmlucHV0cywgZnVuY3Rpb24oaSl7XG5cdFx0XHRyZXR1cm4gaS5qcXVlcnkgPyBpWzBdIDogaTtcblx0XHR9KTtcblx0XHRkZWxldGUgb3B0aW9ucy5pbnB1dHM7XG5cblx0XHRkYXRlcGlja2VyUGx1Z2luLmNhbGwoJCh0aGlzLmlucHV0cyksIG9wdGlvbnMpXG5cdFx0XHQub24oJ2NoYW5nZURhdGUnLCAkLnByb3h5KHRoaXMuZGF0ZVVwZGF0ZWQsIHRoaXMpKTtcblxuXHRcdHRoaXMucGlja2VycyA9ICQubWFwKHRoaXMuaW5wdXRzLCBmdW5jdGlvbihpKXtcblx0XHRcdHJldHVybiAkKGkpLmRhdGEoJ2RhdGVwaWNrZXInKTtcblx0XHR9KTtcblx0XHR0aGlzLnVwZGF0ZURhdGVzKCk7XG5cdH07XG5cdERhdGVSYW5nZVBpY2tlci5wcm90b3R5cGUgPSB7XG5cdFx0dXBkYXRlRGF0ZXM6IGZ1bmN0aW9uKCl7XG5cdFx0XHR0aGlzLmRhdGVzID0gJC5tYXAodGhpcy5waWNrZXJzLCBmdW5jdGlvbihpKXtcblx0XHRcdFx0cmV0dXJuIGkuZ2V0VVRDRGF0ZSgpO1xuXHRcdFx0fSk7XG5cdFx0XHR0aGlzLnVwZGF0ZVJhbmdlcygpO1xuXHRcdH0sXG5cdFx0dXBkYXRlUmFuZ2VzOiBmdW5jdGlvbigpe1xuXHRcdFx0dmFyIHJhbmdlID0gJC5tYXAodGhpcy5kYXRlcywgZnVuY3Rpb24oZCl7XG5cdFx0XHRcdHJldHVybiBkLnZhbHVlT2YoKTtcblx0XHRcdH0pO1xuXHRcdFx0JC5lYWNoKHRoaXMucGlja2VycywgZnVuY3Rpb24oaSwgcCl7XG5cdFx0XHRcdHAuc2V0UmFuZ2UocmFuZ2UpO1xuXHRcdFx0fSk7XG5cdFx0fSxcblx0XHRkYXRlVXBkYXRlZDogZnVuY3Rpb24oZSl7XG5cdFx0XHQvLyBgdGhpcy51cGRhdGluZ2AgaXMgYSB3b3JrYXJvdW5kIGZvciBwcmV2ZW50aW5nIGluZmluaXRlIHJlY3Vyc2lvblxuXHRcdFx0Ly8gYmV0d2VlbiBgY2hhbmdlRGF0ZWAgdHJpZ2dlcmluZyBhbmQgYHNldFVUQ0RhdGVgIGNhbGxpbmcuICBVbnRpbFxuXHRcdFx0Ly8gdGhlcmUgaXMgYSBiZXR0ZXIgbWVjaGFuaXNtLlxuXHRcdFx0aWYgKHRoaXMudXBkYXRpbmcpXG5cdFx0XHRcdHJldHVybjtcblx0XHRcdHRoaXMudXBkYXRpbmcgPSB0cnVlO1xuXG5cdFx0XHR2YXIgZHAgPSAkKGUudGFyZ2V0KS5kYXRhKCdkYXRlcGlja2VyJyk7XG5cblx0XHRcdGlmICh0eXBlb2YoZHApID09PSBcInVuZGVmaW5lZFwiKSB7XG5cdFx0XHRcdHJldHVybjtcblx0XHRcdH1cblxuXHRcdFx0dmFyIG5ld19kYXRlID0gZHAuZ2V0VVRDRGF0ZSgpLFxuXHRcdFx0XHRpID0gJC5pbkFycmF5KGUudGFyZ2V0LCB0aGlzLmlucHV0cyksXG5cdFx0XHRcdGogPSBpIC0gMSxcblx0XHRcdFx0ayA9IGkgKyAxLFxuXHRcdFx0XHRsID0gdGhpcy5pbnB1dHMubGVuZ3RoO1xuXHRcdFx0aWYgKGkgPT09IC0xKVxuXHRcdFx0XHRyZXR1cm47XG5cblx0XHRcdCQuZWFjaCh0aGlzLnBpY2tlcnMsIGZ1bmN0aW9uKGksIHApe1xuXHRcdFx0XHRpZiAoIXAuZ2V0VVRDRGF0ZSgpKVxuXHRcdFx0XHRcdHAuc2V0VVRDRGF0ZShuZXdfZGF0ZSk7XG5cdFx0XHR9KTtcblxuXHRcdFx0aWYgKG5ld19kYXRlIDwgdGhpcy5kYXRlc1tqXSl7XG5cdFx0XHRcdC8vIERhdGUgYmVpbmcgbW92ZWQgZWFybGllci9sZWZ0XG5cdFx0XHRcdHdoaWxlIChqID49IDAgJiYgbmV3X2RhdGUgPCB0aGlzLmRhdGVzW2pdKXtcblx0XHRcdFx0XHR0aGlzLnBpY2tlcnNbai0tXS5zZXRVVENEYXRlKG5ld19kYXRlKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0ZWxzZSBpZiAobmV3X2RhdGUgPiB0aGlzLmRhdGVzW2tdKXtcblx0XHRcdFx0Ly8gRGF0ZSBiZWluZyBtb3ZlZCBsYXRlci9yaWdodFxuXHRcdFx0XHR3aGlsZSAoayA8IGwgJiYgbmV3X2RhdGUgPiB0aGlzLmRhdGVzW2tdKXtcblx0XHRcdFx0XHR0aGlzLnBpY2tlcnNbaysrXS5zZXRVVENEYXRlKG5ld19kYXRlKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0dGhpcy51cGRhdGVEYXRlcygpO1xuXG5cdFx0XHRkZWxldGUgdGhpcy51cGRhdGluZztcblx0XHR9LFxuXHRcdHJlbW92ZTogZnVuY3Rpb24oKXtcblx0XHRcdCQubWFwKHRoaXMucGlja2VycywgZnVuY3Rpb24ocCl7IHAucmVtb3ZlKCk7IH0pO1xuXHRcdFx0ZGVsZXRlIHRoaXMuZWxlbWVudC5kYXRhKCkuZGF0ZXBpY2tlcjtcblx0XHR9XG5cdH07XG5cblx0ZnVuY3Rpb24gb3B0c19mcm9tX2VsKGVsLCBwcmVmaXgpe1xuXHRcdC8vIERlcml2ZSBvcHRpb25zIGZyb20gZWxlbWVudCBkYXRhLWF0dHJzXG5cdFx0dmFyIGRhdGEgPSAkKGVsKS5kYXRhKCksXG5cdFx0XHRvdXQgPSB7fSwgaW5rZXksXG5cdFx0XHRyZXBsYWNlID0gbmV3IFJlZ0V4cCgnXicgKyBwcmVmaXgudG9Mb3dlckNhc2UoKSArICcoW0EtWl0pJyk7XG5cdFx0cHJlZml4ID0gbmV3IFJlZ0V4cCgnXicgKyBwcmVmaXgudG9Mb3dlckNhc2UoKSk7XG5cdFx0ZnVuY3Rpb24gcmVfbG93ZXIoXyxhKXtcblx0XHRcdHJldHVybiBhLnRvTG93ZXJDYXNlKCk7XG5cdFx0fVxuXHRcdGZvciAodmFyIGtleSBpbiBkYXRhKVxuXHRcdFx0aWYgKHByZWZpeC50ZXN0KGtleSkpe1xuXHRcdFx0XHRpbmtleSA9IGtleS5yZXBsYWNlKHJlcGxhY2UsIHJlX2xvd2VyKTtcblx0XHRcdFx0b3V0W2lua2V5XSA9IGRhdGFba2V5XTtcblx0XHRcdH1cblx0XHRyZXR1cm4gb3V0O1xuXHR9XG5cblx0ZnVuY3Rpb24gb3B0c19mcm9tX2xvY2FsZShsYW5nKXtcblx0XHQvLyBEZXJpdmUgb3B0aW9ucyBmcm9tIGxvY2FsZSBwbHVnaW5zXG5cdFx0dmFyIG91dCA9IHt9O1xuXHRcdC8vIENoZWNrIGlmIFwiZGUtREVcIiBzdHlsZSBkYXRlIGlzIGF2YWlsYWJsZSwgaWYgbm90IGxhbmd1YWdlIHNob3VsZFxuXHRcdC8vIGZhbGxiYWNrIHRvIDIgbGV0dGVyIGNvZGUgZWcgXCJkZVwiXG5cdFx0aWYgKCFkYXRlc1tsYW5nXSl7XG5cdFx0XHRsYW5nID0gbGFuZy5zcGxpdCgnLScpWzBdO1xuXHRcdFx0aWYgKCFkYXRlc1tsYW5nXSlcblx0XHRcdFx0cmV0dXJuO1xuXHRcdH1cblx0XHR2YXIgZCA9IGRhdGVzW2xhbmddO1xuXHRcdCQuZWFjaChsb2NhbGVfb3B0cywgZnVuY3Rpb24oaSxrKXtcblx0XHRcdGlmIChrIGluIGQpXG5cdFx0XHRcdG91dFtrXSA9IGRba107XG5cdFx0fSk7XG5cdFx0cmV0dXJuIG91dDtcblx0fVxuXG5cdHZhciBvbGQgPSAkLmZuLmRhdGVwaWNrZXI7XG5cdHZhciBkYXRlcGlja2VyUGx1Z2luID0gZnVuY3Rpb24ob3B0aW9uKXtcblx0XHR2YXIgYXJncyA9IEFycmF5LmFwcGx5KG51bGwsIGFyZ3VtZW50cyk7XG5cdFx0YXJncy5zaGlmdCgpO1xuXHRcdHZhciBpbnRlcm5hbF9yZXR1cm47XG5cdFx0dGhpcy5lYWNoKGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgJHRoaXMgPSAkKHRoaXMpLFxuXHRcdFx0XHRkYXRhID0gJHRoaXMuZGF0YSgnZGF0ZXBpY2tlcicpLFxuXHRcdFx0XHRvcHRpb25zID0gdHlwZW9mIG9wdGlvbiA9PT0gJ29iamVjdCcgJiYgb3B0aW9uO1xuXHRcdFx0aWYgKCFkYXRhKXtcblx0XHRcdFx0dmFyIGVsb3B0cyA9IG9wdHNfZnJvbV9lbCh0aGlzLCAnZGF0ZScpLFxuXHRcdFx0XHRcdC8vIFByZWxpbWluYXJ5IG90aW9uc1xuXHRcdFx0XHRcdHhvcHRzID0gJC5leHRlbmQoe30sIGRlZmF1bHRzLCBlbG9wdHMsIG9wdGlvbnMpLFxuXHRcdFx0XHRcdGxvY29wdHMgPSBvcHRzX2Zyb21fbG9jYWxlKHhvcHRzLmxhbmd1YWdlKSxcblx0XHRcdFx0XHQvLyBPcHRpb25zIHByaW9yaXR5OiBqcyBhcmdzLCBkYXRhLWF0dHJzLCBsb2NhbGVzLCBkZWZhdWx0c1xuXHRcdFx0XHRcdG9wdHMgPSAkLmV4dGVuZCh7fSwgZGVmYXVsdHMsIGxvY29wdHMsIGVsb3B0cywgb3B0aW9ucyk7XG5cdFx0XHRcdGlmICgkdGhpcy5oYXNDbGFzcygnaW5wdXQtZGF0ZXJhbmdlJykgfHwgb3B0cy5pbnB1dHMpe1xuXHRcdFx0XHRcdCQuZXh0ZW5kKG9wdHMsIHtcblx0XHRcdFx0XHRcdGlucHV0czogb3B0cy5pbnB1dHMgfHwgJHRoaXMuZmluZCgnaW5wdXQnKS50b0FycmF5KClcblx0XHRcdFx0XHR9KTtcblx0XHRcdFx0XHRkYXRhID0gbmV3IERhdGVSYW5nZVBpY2tlcih0aGlzLCBvcHRzKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRlbHNlIHtcblx0XHRcdFx0XHRkYXRhID0gbmV3IERhdGVwaWNrZXIodGhpcywgb3B0cyk7XG5cdFx0XHRcdH1cblx0XHRcdFx0JHRoaXMuZGF0YSgnZGF0ZXBpY2tlcicsIGRhdGEpO1xuXHRcdFx0fVxuXHRcdFx0aWYgKHR5cGVvZiBvcHRpb24gPT09ICdzdHJpbmcnICYmIHR5cGVvZiBkYXRhW29wdGlvbl0gPT09ICdmdW5jdGlvbicpe1xuXHRcdFx0XHRpbnRlcm5hbF9yZXR1cm4gPSBkYXRhW29wdGlvbl0uYXBwbHkoZGF0YSwgYXJncyk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRpZiAoXG5cdFx0XHRpbnRlcm5hbF9yZXR1cm4gPT09IHVuZGVmaW5lZCB8fFxuXHRcdFx0aW50ZXJuYWxfcmV0dXJuIGluc3RhbmNlb2YgRGF0ZXBpY2tlciB8fFxuXHRcdFx0aW50ZXJuYWxfcmV0dXJuIGluc3RhbmNlb2YgRGF0ZVJhbmdlUGlja2VyXG5cdFx0KVxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cblx0XHRpZiAodGhpcy5sZW5ndGggPiAxKVxuXHRcdFx0dGhyb3cgbmV3IEVycm9yKCdVc2luZyBvbmx5IGFsbG93ZWQgZm9yIHRoZSBjb2xsZWN0aW9uIG9mIGEgc2luZ2xlIGVsZW1lbnQgKCcgKyBvcHRpb24gKyAnIGZ1bmN0aW9uKScpO1xuXHRcdGVsc2Vcblx0XHRcdHJldHVybiBpbnRlcm5hbF9yZXR1cm47XG5cdH07XG5cdCQuZm4uZGF0ZXBpY2tlciA9IGRhdGVwaWNrZXJQbHVnaW47XG5cblx0dmFyIGRlZmF1bHRzID0gJC5mbi5kYXRlcGlja2VyLmRlZmF1bHRzID0ge1xuXHRcdGFzc3VtZU5lYXJieVllYXI6IGZhbHNlLFxuXHRcdGF1dG9jbG9zZTogZmFsc2UsXG5cdFx0YmVmb3JlU2hvd0RheTogJC5ub29wLFxuXHRcdGJlZm9yZVNob3dNb250aDogJC5ub29wLFxuXHRcdGJlZm9yZVNob3dZZWFyOiAkLm5vb3AsXG5cdFx0YmVmb3JlU2hvd0RlY2FkZTogJC5ub29wLFxuXHRcdGJlZm9yZVNob3dDZW50dXJ5OiAkLm5vb3AsXG5cdFx0Y2FsZW5kYXJXZWVrczogZmFsc2UsXG5cdFx0Y2xlYXJCdG46IGZhbHNlLFxuXHRcdHRvZ2dsZUFjdGl2ZTogZmFsc2UsXG5cdFx0ZGF5c09mV2Vla0Rpc2FibGVkOiBbXSxcblx0XHRkYXlzT2ZXZWVrSGlnaGxpZ2h0ZWQ6IFtdLFxuXHRcdGRhdGVzRGlzYWJsZWQ6IFtdLFxuXHRcdGVuZERhdGU6IEluZmluaXR5LFxuXHRcdGZvcmNlUGFyc2U6IHRydWUsXG5cdFx0Zm9ybWF0OiAnbW0vZGQveXl5eScsXG5cdFx0a2V5Ym9hcmROYXZpZ2F0aW9uOiB0cnVlLFxuXHRcdGxhbmd1YWdlOiAnZW4nLFxuXHRcdG1pblZpZXdNb2RlOiAwLFxuXHRcdG1heFZpZXdNb2RlOiA0LFxuXHRcdG11bHRpZGF0ZTogZmFsc2UsXG5cdFx0bXVsdGlkYXRlU2VwYXJhdG9yOiAnLCcsXG5cdFx0b3JpZW50YXRpb246IFwiYXV0b1wiLFxuXHRcdHJ0bDogZmFsc2UsXG5cdFx0c3RhcnREYXRlOiAtSW5maW5pdHksXG5cdFx0c3RhcnRWaWV3OiAwLFxuXHRcdHRvZGF5QnRuOiBmYWxzZSxcblx0XHR0b2RheUhpZ2hsaWdodDogZmFsc2UsXG5cdFx0d2Vla1N0YXJ0OiAwLFxuXHRcdGRpc2FibGVUb3VjaEtleWJvYXJkOiBmYWxzZSxcblx0XHRlbmFibGVPblJlYWRvbmx5OiB0cnVlLFxuXHRcdHNob3dPbkZvY3VzOiB0cnVlLFxuXHRcdHpJbmRleE9mZnNldDogMTAsXG5cdFx0Y29udGFpbmVyOiAnYm9keScsXG5cdFx0aW1tZWRpYXRlVXBkYXRlczogZmFsc2UsXG5cdFx0dGl0bGU6ICcnLFxuXHRcdHRlbXBsYXRlczoge1xuXHRcdFx0bGVmdEFycm93OiAnJmxhcXVvOycsXG5cdFx0XHRyaWdodEFycm93OiAnJnJhcXVvOydcblx0XHR9XG5cdH07XG5cdHZhciBsb2NhbGVfb3B0cyA9ICQuZm4uZGF0ZXBpY2tlci5sb2NhbGVfb3B0cyA9IFtcblx0XHQnZm9ybWF0Jyxcblx0XHQncnRsJyxcblx0XHQnd2Vla1N0YXJ0J1xuXHRdO1xuXHQkLmZuLmRhdGVwaWNrZXIuQ29uc3RydWN0b3IgPSBEYXRlcGlja2VyO1xuXHR2YXIgZGF0ZXMgPSAkLmZuLmRhdGVwaWNrZXIuZGF0ZXMgPSB7XG5cdFx0ZW46IHtcblx0XHRcdGRheXM6IFtcIlN1bmRheVwiLCBcIk1vbmRheVwiLCBcIlR1ZXNkYXlcIiwgXCJXZWRuZXNkYXlcIiwgXCJUaHVyc2RheVwiLCBcIkZyaWRheVwiLCBcIlNhdHVyZGF5XCJdLFxuXHRcdFx0ZGF5c1Nob3J0OiBbXCJTdW5cIiwgXCJNb25cIiwgXCJUdWVcIiwgXCJXZWRcIiwgXCJUaHVcIiwgXCJGcmlcIiwgXCJTYXRcIl0sXG5cdFx0XHRkYXlzTWluOiBbXCJTdVwiLCBcIk1vXCIsIFwiVHVcIiwgXCJXZVwiLCBcIlRoXCIsIFwiRnJcIiwgXCJTYVwiXSxcblx0XHRcdG1vbnRoczogW1wiSmFudWFyeVwiLCBcIkZlYnJ1YXJ5XCIsIFwiTWFyY2hcIiwgXCJBcHJpbFwiLCBcIk1heVwiLCBcIkp1bmVcIiwgXCJKdWx5XCIsIFwiQXVndXN0XCIsIFwiU2VwdGVtYmVyXCIsIFwiT2N0b2JlclwiLCBcIk5vdmVtYmVyXCIsIFwiRGVjZW1iZXJcIl0sXG5cdFx0XHRtb250aHNTaG9ydDogW1wiSmFuXCIsIFwiRmViXCIsIFwiTWFyXCIsIFwiQXByXCIsIFwiTWF5XCIsIFwiSnVuXCIsIFwiSnVsXCIsIFwiQXVnXCIsIFwiU2VwXCIsIFwiT2N0XCIsIFwiTm92XCIsIFwiRGVjXCJdLFxuXHRcdFx0dG9kYXk6IFwiVG9kYXlcIixcblx0XHRcdGNsZWFyOiBcIkNsZWFyXCIsXG5cdFx0XHR0aXRsZUZvcm1hdDogXCJNTSB5eXl5XCJcblx0XHR9XG5cdH07XG5cblx0dmFyIERQR2xvYmFsID0ge1xuXHRcdG1vZGVzOiBbXG5cdFx0XHR7XG5cdFx0XHRcdGNsc05hbWU6ICdkYXlzJyxcblx0XHRcdFx0bmF2Rm5jOiAnTW9udGgnLFxuXHRcdFx0XHRuYXZTdGVwOiAxXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRjbHNOYW1lOiAnbW9udGhzJyxcblx0XHRcdFx0bmF2Rm5jOiAnRnVsbFllYXInLFxuXHRcdFx0XHRuYXZTdGVwOiAxXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRjbHNOYW1lOiAneWVhcnMnLFxuXHRcdFx0XHRuYXZGbmM6ICdGdWxsWWVhcicsXG5cdFx0XHRcdG5hdlN0ZXA6IDEwXG5cdFx0XHR9LFxuXHRcdFx0e1xuXHRcdFx0XHRjbHNOYW1lOiAnZGVjYWRlcycsXG5cdFx0XHRcdG5hdkZuYzogJ0Z1bGxEZWNhZGUnLFxuXHRcdFx0XHRuYXZTdGVwOiAxMDBcblx0XHRcdH0sXG5cdFx0XHR7XG5cdFx0XHRcdGNsc05hbWU6ICdjZW50dXJpZXMnLFxuXHRcdFx0XHRuYXZGbmM6ICdGdWxsQ2VudHVyeScsXG5cdFx0XHRcdG5hdlN0ZXA6IDEwMDBcblx0XHR9XSxcblx0XHRpc0xlYXBZZWFyOiBmdW5jdGlvbih5ZWFyKXtcblx0XHRcdHJldHVybiAoKCh5ZWFyICUgNCA9PT0gMCkgJiYgKHllYXIgJSAxMDAgIT09IDApKSB8fCAoeWVhciAlIDQwMCA9PT0gMCkpO1xuXHRcdH0sXG5cdFx0Z2V0RGF5c0luTW9udGg6IGZ1bmN0aW9uKHllYXIsIG1vbnRoKXtcblx0XHRcdHJldHVybiBbMzEsIChEUEdsb2JhbC5pc0xlYXBZZWFyKHllYXIpID8gMjkgOiAyOCksIDMxLCAzMCwgMzEsIDMwLCAzMSwgMzEsIDMwLCAzMSwgMzAsIDMxXVttb250aF07XG5cdFx0fSxcblx0XHR2YWxpZFBhcnRzOiAvZGQ/fEREP3xtbT98TU0/fHl5KD86eXkpPy9nLFxuXHRcdG5vbnB1bmN0dWF0aW9uOiAvW14gLVxcLzotQFxcdTVlNzRcXHU2NzA4XFx1NjVlNVxcWy1gey1+XFx0XFxuXFxyXSsvZyxcblx0XHRwYXJzZUZvcm1hdDogZnVuY3Rpb24oZm9ybWF0KXtcblx0XHRcdGlmICh0eXBlb2YgZm9ybWF0LnRvVmFsdWUgPT09ICdmdW5jdGlvbicgJiYgdHlwZW9mIGZvcm1hdC50b0Rpc3BsYXkgPT09ICdmdW5jdGlvbicpXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZvcm1hdDtcbiAgICAgICAgICAgIC8vIElFIHRyZWF0cyBcXDAgYXMgYSBzdHJpbmcgZW5kIGluIGlucHV0cyAodHJ1bmNhdGluZyB0aGUgdmFsdWUpLFxuXHRcdFx0Ly8gc28gaXQncyBhIGJhZCBmb3JtYXQgZGVsaW1pdGVyLCBhbnl3YXlcblx0XHRcdHZhciBzZXBhcmF0b3JzID0gZm9ybWF0LnJlcGxhY2UodGhpcy52YWxpZFBhcnRzLCAnXFwwJykuc3BsaXQoJ1xcMCcpLFxuXHRcdFx0XHRwYXJ0cyA9IGZvcm1hdC5tYXRjaCh0aGlzLnZhbGlkUGFydHMpO1xuXHRcdFx0aWYgKCFzZXBhcmF0b3JzIHx8ICFzZXBhcmF0b3JzLmxlbmd0aCB8fCAhcGFydHMgfHwgcGFydHMubGVuZ3RoID09PSAwKXtcblx0XHRcdFx0dGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCBkYXRlIGZvcm1hdC5cIik7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4ge3NlcGFyYXRvcnM6IHNlcGFyYXRvcnMsIHBhcnRzOiBwYXJ0c307XG5cdFx0fSxcblx0XHRwYXJzZURhdGU6IGZ1bmN0aW9uKGRhdGUsIGZvcm1hdCwgbGFuZ3VhZ2UsIGFzc3VtZU5lYXJieSl7XG5cdFx0XHRpZiAoIWRhdGUpXG5cdFx0XHRcdHJldHVybiB1bmRlZmluZWQ7XG5cdFx0XHRpZiAoZGF0ZSBpbnN0YW5jZW9mIERhdGUpXG5cdFx0XHRcdHJldHVybiBkYXRlO1xuXHRcdFx0aWYgKHR5cGVvZiBmb3JtYXQgPT09ICdzdHJpbmcnKVxuXHRcdFx0XHRmb3JtYXQgPSBEUEdsb2JhbC5wYXJzZUZvcm1hdChmb3JtYXQpO1xuXHRcdFx0aWYgKGZvcm1hdC50b1ZhbHVlKVxuICAgICAgICAgICAgICAgIHJldHVybiBmb3JtYXQudG9WYWx1ZShkYXRlLCBmb3JtYXQsIGxhbmd1YWdlKTtcbiAgICAgICAgICAgIHZhciBwYXJ0X3JlID0gLyhbXFwtK11cXGQrKShbZG13eV0pLyxcblx0XHRcdFx0cGFydHMgPSBkYXRlLm1hdGNoKC8oW1xcLStdXFxkKykoW2Rtd3ldKS9nKSxcblx0XHRcdFx0Zm5fbWFwID0ge1xuXHRcdFx0XHRcdGQ6ICdtb3ZlRGF5Jyxcblx0XHRcdFx0XHRtOiAnbW92ZU1vbnRoJyxcblx0XHRcdFx0XHR3OiAnbW92ZVdlZWsnLFxuXHRcdFx0XHRcdHk6ICdtb3ZlWWVhcidcblx0XHRcdFx0fSxcblx0XHRcdFx0ZGF0ZUFsaWFzZXMgPSB7XG5cdFx0XHRcdFx0eWVzdGVyZGF5OiAnLTFkJyxcblx0XHRcdFx0XHR0b2RheTogJyswZCcsXG5cdFx0XHRcdFx0dG9tb3Jyb3c6ICcrMWQnXG5cdFx0XHRcdH0sXG5cdFx0XHRcdHBhcnQsIGRpciwgaSwgZm47XG5cdFx0XHRpZiAoL15bXFwtK11cXGQrW2Rtd3ldKFtcXHMsXStbXFwtK11cXGQrW2Rtd3ldKSokLy50ZXN0KGRhdGUpKXtcblx0XHRcdFx0ZGF0ZSA9IG5ldyBEYXRlKCk7XG5cdFx0XHRcdGZvciAoaT0wOyBpIDwgcGFydHMubGVuZ3RoOyBpKyspe1xuXHRcdFx0XHRcdHBhcnQgPSBwYXJ0X3JlLmV4ZWMocGFydHNbaV0pO1xuXHRcdFx0XHRcdGRpciA9IHBhcnNlSW50KHBhcnRbMV0pO1xuXHRcdFx0XHRcdGZuID0gZm5fbWFwW3BhcnRbMl1dO1xuXHRcdFx0XHRcdGRhdGUgPSBEYXRlcGlja2VyLnByb3RvdHlwZVtmbl0oZGF0ZSwgZGlyKTtcblx0XHRcdFx0fVxuXHRcdFx0XHRyZXR1cm4gVVRDRGF0ZShkYXRlLmdldFVUQ0Z1bGxZZWFyKCksIGRhdGUuZ2V0VVRDTW9udGgoKSwgZGF0ZS5nZXRVVENEYXRlKCkpO1xuXHRcdFx0fVxuXG5cdFx0XHRpZiAodHlwZW9mIGRhdGVBbGlhc2VzW2RhdGVdICE9PSAndW5kZWZpbmVkJykge1xuXHRcdFx0XHRkYXRlID0gZGF0ZUFsaWFzZXNbZGF0ZV07XG5cdFx0XHRcdHBhcnRzID0gZGF0ZS5tYXRjaCgvKFtcXC0rXVxcZCspKFtkbXd5XSkvZyk7XG5cblx0XHRcdFx0aWYgKC9eW1xcLStdXFxkK1tkbXd5XShbXFxzLF0rW1xcLStdXFxkK1tkbXd5XSkqJC8udGVzdChkYXRlKSl7XG5cdFx0XHRcdFx0ZGF0ZSA9IG5ldyBEYXRlKCk7XG5cdFx0XHRcdCAgXHRmb3IgKGk9MDsgaSA8IHBhcnRzLmxlbmd0aDsgaSsrKXtcblx0XHRcdFx0XHRcdHBhcnQgPSBwYXJ0X3JlLmV4ZWMocGFydHNbaV0pO1xuXHRcdFx0XHRcdFx0ZGlyID0gcGFyc2VJbnQocGFydFsxXSk7XG5cdFx0XHRcdFx0XHRmbiA9IGZuX21hcFtwYXJ0WzJdXTtcblx0XHRcdFx0XHRcdGRhdGUgPSBEYXRlcGlja2VyLnByb3RvdHlwZVtmbl0oZGF0ZSwgZGlyKTtcblx0XHRcdFx0ICBcdH1cblxuXHRcdFx0ICBcdFx0cmV0dXJuIFVUQ0RhdGUoZGF0ZS5nZXRVVENGdWxsWWVhcigpLCBkYXRlLmdldFVUQ01vbnRoKCksIGRhdGUuZ2V0VVRDRGF0ZSgpKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRwYXJ0cyA9IGRhdGUgJiYgZGF0ZS5tYXRjaCh0aGlzLm5vbnB1bmN0dWF0aW9uKSB8fCBbXTtcblx0XHRcdGRhdGUgPSBuZXcgRGF0ZSgpO1xuXG5cdFx0XHRmdW5jdGlvbiBhcHBseU5lYXJieVllYXIoeWVhciwgdGhyZXNob2xkKXtcblx0XHRcdFx0aWYgKHRocmVzaG9sZCA9PT0gdHJ1ZSlcblx0XHRcdFx0XHR0aHJlc2hvbGQgPSAxMDtcblxuXHRcdFx0XHQvLyBpZiB5ZWFyIGlzIDIgZGlnaXRzIG9yIGxlc3MsIHRoYW4gdGhlIHVzZXIgbW9zdCBsaWtlbHkgaXMgdHJ5aW5nIHRvIGdldCBhIHJlY2VudCBjZW50dXJ5XG5cdFx0XHRcdGlmICh5ZWFyIDwgMTAwKXtcblx0XHRcdFx0XHR5ZWFyICs9IDIwMDA7XG5cdFx0XHRcdFx0Ly8gaWYgdGhlIG5ldyB5ZWFyIGlzIG1vcmUgdGhhbiB0aHJlc2hvbGQgeWVhcnMgaW4gYWR2YW5jZSwgdXNlIGxhc3QgY2VudHVyeVxuXHRcdFx0XHRcdGlmICh5ZWFyID4gKChuZXcgRGF0ZSgpKS5nZXRGdWxsWWVhcigpK3RocmVzaG9sZCkpe1xuXHRcdFx0XHRcdFx0eWVhciAtPSAxMDA7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0cmV0dXJuIHllYXI7XG5cdFx0XHR9XG5cblx0XHRcdHZhciBwYXJzZWQgPSB7fSxcblx0XHRcdFx0c2V0dGVyc19vcmRlciA9IFsneXl5eScsICd5eScsICdNJywgJ01NJywgJ20nLCAnbW0nLCAnZCcsICdkZCddLFxuXHRcdFx0XHRzZXR0ZXJzX21hcCA9IHtcblx0XHRcdFx0XHR5eXl5OiBmdW5jdGlvbihkLHYpe1xuXHRcdFx0XHRcdFx0cmV0dXJuIGQuc2V0VVRDRnVsbFllYXIoYXNzdW1lTmVhcmJ5ID8gYXBwbHlOZWFyYnlZZWFyKHYsIGFzc3VtZU5lYXJieSkgOiB2KTtcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdHl5OiBmdW5jdGlvbihkLHYpe1xuXHRcdFx0XHRcdFx0cmV0dXJuIGQuc2V0VVRDRnVsbFllYXIoYXNzdW1lTmVhcmJ5ID8gYXBwbHlOZWFyYnlZZWFyKHYsIGFzc3VtZU5lYXJieSkgOiB2KTtcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdG06IGZ1bmN0aW9uKGQsdil7XG5cdFx0XHRcdFx0XHRpZiAoaXNOYU4oZCkpXG5cdFx0XHRcdFx0XHRcdHJldHVybiBkO1xuXHRcdFx0XHRcdFx0diAtPSAxO1xuXHRcdFx0XHRcdFx0d2hpbGUgKHYgPCAwKSB2ICs9IDEyO1xuXHRcdFx0XHRcdFx0diAlPSAxMjtcblx0XHRcdFx0XHRcdGQuc2V0VVRDTW9udGgodik7XG5cdFx0XHRcdFx0XHR3aGlsZSAoZC5nZXRVVENNb250aCgpICE9PSB2KVxuXHRcdFx0XHRcdFx0XHRkLnNldFVUQ0RhdGUoZC5nZXRVVENEYXRlKCktMSk7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZDtcblx0XHRcdFx0XHR9LFxuXHRcdFx0XHRcdGQ6IGZ1bmN0aW9uKGQsdil7XG5cdFx0XHRcdFx0XHRyZXR1cm4gZC5zZXRVVENEYXRlKHYpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSxcblx0XHRcdFx0dmFsLCBmaWx0ZXJlZDtcblx0XHRcdHNldHRlcnNfbWFwWydNJ10gPSBzZXR0ZXJzX21hcFsnTU0nXSA9IHNldHRlcnNfbWFwWydtbSddID0gc2V0dGVyc19tYXBbJ20nXTtcblx0XHRcdHNldHRlcnNfbWFwWydkZCddID0gc2V0dGVyc19tYXBbJ2QnXTtcblx0XHRcdGRhdGUgPSBVVENUb2RheSgpO1xuXHRcdFx0dmFyIGZwYXJ0cyA9IGZvcm1hdC5wYXJ0cy5zbGljZSgpO1xuXHRcdFx0Ly8gUmVtb3ZlIG5vb3AgcGFydHNcblx0XHRcdGlmIChwYXJ0cy5sZW5ndGggIT09IGZwYXJ0cy5sZW5ndGgpe1xuXHRcdFx0XHRmcGFydHMgPSAkKGZwYXJ0cykuZmlsdGVyKGZ1bmN0aW9uKGkscCl7XG5cdFx0XHRcdFx0cmV0dXJuICQuaW5BcnJheShwLCBzZXR0ZXJzX29yZGVyKSAhPT0gLTE7XG5cdFx0XHRcdH0pLnRvQXJyYXkoKTtcblx0XHRcdH1cblx0XHRcdC8vIFByb2Nlc3MgcmVtYWluZGVyXG5cdFx0XHRmdW5jdGlvbiBtYXRjaF9wYXJ0KCl7XG5cdFx0XHRcdHZhciBtID0gdGhpcy5zbGljZSgwLCBwYXJ0c1tpXS5sZW5ndGgpLFxuXHRcdFx0XHRcdHAgPSBwYXJ0c1tpXS5zbGljZSgwLCBtLmxlbmd0aCk7XG5cdFx0XHRcdHJldHVybiBtLnRvTG93ZXJDYXNlKCkgPT09IHAudG9Mb3dlckNhc2UoKTtcblx0XHRcdH1cblx0XHRcdGlmIChwYXJ0cy5sZW5ndGggPT09IGZwYXJ0cy5sZW5ndGgpe1xuXHRcdFx0XHR2YXIgY250O1xuXHRcdFx0XHRmb3IgKGk9MCwgY250ID0gZnBhcnRzLmxlbmd0aDsgaSA8IGNudDsgaSsrKXtcblx0XHRcdFx0XHR2YWwgPSBwYXJzZUludChwYXJ0c1tpXSwgMTApO1xuXHRcdFx0XHRcdHBhcnQgPSBmcGFydHNbaV07XG5cdFx0XHRcdFx0aWYgKGlzTmFOKHZhbCkpe1xuXHRcdFx0XHRcdFx0c3dpdGNoIChwYXJ0KXtcblx0XHRcdFx0XHRcdFx0Y2FzZSAnTU0nOlxuXHRcdFx0XHRcdFx0XHRcdGZpbHRlcmVkID0gJChkYXRlc1tsYW5ndWFnZV0ubW9udGhzKS5maWx0ZXIobWF0Y2hfcGFydCk7XG5cdFx0XHRcdFx0XHRcdFx0dmFsID0gJC5pbkFycmF5KGZpbHRlcmVkWzBdLCBkYXRlc1tsYW5ndWFnZV0ubW9udGhzKSArIDE7XG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdGNhc2UgJ00nOlxuXHRcdFx0XHRcdFx0XHRcdGZpbHRlcmVkID0gJChkYXRlc1tsYW5ndWFnZV0ubW9udGhzU2hvcnQpLmZpbHRlcihtYXRjaF9wYXJ0KTtcblx0XHRcdFx0XHRcdFx0XHR2YWwgPSAkLmluQXJyYXkoZmlsdGVyZWRbMF0sIGRhdGVzW2xhbmd1YWdlXS5tb250aHNTaG9ydCkgKyAxO1xuXHRcdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRwYXJzZWRbcGFydF0gPSB2YWw7XG5cdFx0XHRcdH1cblx0XHRcdFx0dmFyIF9kYXRlLCBzO1xuXHRcdFx0XHRmb3IgKGk9MDsgaSA8IHNldHRlcnNfb3JkZXIubGVuZ3RoOyBpKyspe1xuXHRcdFx0XHRcdHMgPSBzZXR0ZXJzX29yZGVyW2ldO1xuXHRcdFx0XHRcdGlmIChzIGluIHBhcnNlZCAmJiAhaXNOYU4ocGFyc2VkW3NdKSl7XG5cdFx0XHRcdFx0XHRfZGF0ZSA9IG5ldyBEYXRlKGRhdGUpO1xuXHRcdFx0XHRcdFx0c2V0dGVyc19tYXBbc10oX2RhdGUsIHBhcnNlZFtzXSk7XG5cdFx0XHRcdFx0XHRpZiAoIWlzTmFOKF9kYXRlKSlcblx0XHRcdFx0XHRcdFx0ZGF0ZSA9IF9kYXRlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIGRhdGU7XG5cdFx0fSxcblx0XHRmb3JtYXREYXRlOiBmdW5jdGlvbihkYXRlLCBmb3JtYXQsIGxhbmd1YWdlKXtcblx0XHRcdGlmICghZGF0ZSlcblx0XHRcdFx0cmV0dXJuICcnO1xuXHRcdFx0aWYgKHR5cGVvZiBmb3JtYXQgPT09ICdzdHJpbmcnKVxuXHRcdFx0XHRmb3JtYXQgPSBEUEdsb2JhbC5wYXJzZUZvcm1hdChmb3JtYXQpO1xuXHRcdFx0aWYgKGZvcm1hdC50b0Rpc3BsYXkpXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZvcm1hdC50b0Rpc3BsYXkoZGF0ZSwgZm9ybWF0LCBsYW5ndWFnZSk7XG4gICAgICAgICAgICB2YXIgdmFsID0ge1xuXHRcdFx0XHRkOiBkYXRlLmdldFVUQ0RhdGUoKSxcblx0XHRcdFx0RDogZGF0ZXNbbGFuZ3VhZ2VdLmRheXNTaG9ydFtkYXRlLmdldFVUQ0RheSgpXSxcblx0XHRcdFx0REQ6IGRhdGVzW2xhbmd1YWdlXS5kYXlzW2RhdGUuZ2V0VVRDRGF5KCldLFxuXHRcdFx0XHRtOiBkYXRlLmdldFVUQ01vbnRoKCkgKyAxLFxuXHRcdFx0XHRNOiBkYXRlc1tsYW5ndWFnZV0ubW9udGhzU2hvcnRbZGF0ZS5nZXRVVENNb250aCgpXSxcblx0XHRcdFx0TU06IGRhdGVzW2xhbmd1YWdlXS5tb250aHNbZGF0ZS5nZXRVVENNb250aCgpXSxcblx0XHRcdFx0eXk6IGRhdGUuZ2V0VVRDRnVsbFllYXIoKS50b1N0cmluZygpLnN1YnN0cmluZygyKSxcblx0XHRcdFx0eXl5eTogZGF0ZS5nZXRVVENGdWxsWWVhcigpXG5cdFx0XHR9O1xuXHRcdFx0dmFsLmRkID0gKHZhbC5kIDwgMTAgPyAnMCcgOiAnJykgKyB2YWwuZDtcblx0XHRcdHZhbC5tbSA9ICh2YWwubSA8IDEwID8gJzAnIDogJycpICsgdmFsLm07XG5cdFx0XHRkYXRlID0gW107XG5cdFx0XHR2YXIgc2VwcyA9ICQuZXh0ZW5kKFtdLCBmb3JtYXQuc2VwYXJhdG9ycyk7XG5cdFx0XHRmb3IgKHZhciBpPTAsIGNudCA9IGZvcm1hdC5wYXJ0cy5sZW5ndGg7IGkgPD0gY250OyBpKyspe1xuXHRcdFx0XHRpZiAoc2Vwcy5sZW5ndGgpXG5cdFx0XHRcdFx0ZGF0ZS5wdXNoKHNlcHMuc2hpZnQoKSk7XG5cdFx0XHRcdGRhdGUucHVzaCh2YWxbZm9ybWF0LnBhcnRzW2ldXSk7XG5cdFx0XHR9XG5cdFx0XHRyZXR1cm4gZGF0ZS5qb2luKCcnKTtcblx0XHR9LFxuXHRcdGhlYWRUZW1wbGF0ZTogJzx0aGVhZD4nK1xuXHRcdFx0ICAgICAgICAgICAgICAnPHRyPicrXG5cdFx0XHQgICAgICAgICAgICAgICAgJzx0aCBjb2xzcGFuPVwiN1wiIGNsYXNzPVwiZGF0ZXBpY2tlci10aXRsZVwiPjwvdGg+Jytcblx0XHRcdCAgICAgICAgICAgICAgJzwvdHI+Jytcblx0XHRcdFx0XHRcdFx0Jzx0cj4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGggY2xhc3M9XCJwcmV2XCI+PHNwYW4gY2xhc3M9XCJlcy1pY29uIGVzLWljb24tcHJldmlvdXNcIj48L3NwYW4+PC90aD4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGggY29sc3Bhbj1cIjVcIiBjbGFzcz1cImRhdGVwaWNrZXItc3dpdGNoXCI+PC90aD4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGggY2xhc3M9XCJuZXh0XCI+PHNwYW4gY2xhc3M9XCJlcy1pY29uIGVzLWljb24tbmV4dFwiPjwvc3Bhbj48L3RoPicrXG5cdFx0XHRcdFx0XHRcdCc8L3RyPicrXG5cdFx0XHRcdFx0XHQnPC90aGVhZD4nLFxuXHRcdGNvbnRUZW1wbGF0ZTogJzx0Ym9keT48dHI+PHRkIGNvbHNwYW49XCI3XCI+PC90ZD48L3RyPjwvdGJvZHk+Jyxcblx0XHRmb290VGVtcGxhdGU6ICc8dGZvb3Q+Jytcblx0XHRcdFx0XHRcdFx0Jzx0cj4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGggY29sc3Bhbj1cIjdcIiBjbGFzcz1cInRvZGF5XCI+PC90aD4nK1xuXHRcdFx0XHRcdFx0XHQnPC90cj4nK1xuXHRcdFx0XHRcdFx0XHQnPHRyPicrXG5cdFx0XHRcdFx0XHRcdFx0Jzx0aCBjb2xzcGFuPVwiN1wiIGNsYXNzPVwiY2xlYXJcIj48L3RoPicrXG5cdFx0XHRcdFx0XHRcdCc8L3RyPicrXG5cdFx0XHRcdFx0XHQnPC90Zm9vdD4nXG5cdH07XG5cdERQR2xvYmFsLnRlbXBsYXRlID0gJzxkaXYgY2xhc3M9XCJkYXRlcGlja2VyXCI+Jytcblx0XHRcdFx0XHRcdFx0JzxkaXYgY2xhc3M9XCJkYXRlcGlja2VyLWRheXNcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGFibGUgY2xhc3M9XCJ0YWJsZS1jb25kZW5zZWRcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuaGVhZFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdFx0Jzx0Ym9keT48L3Rib2R5PicrXG5cdFx0XHRcdFx0XHRcdFx0XHREUEdsb2JhbC5mb290VGVtcGxhdGUrXG5cdFx0XHRcdFx0XHRcdFx0JzwvdGFibGU+Jytcblx0XHRcdFx0XHRcdFx0JzwvZGl2PicrXG5cdFx0XHRcdFx0XHRcdCc8ZGl2IGNsYXNzPVwiZGF0ZXBpY2tlci1tb250aHNcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGFibGUgY2xhc3M9XCJ0YWJsZS1jb25kZW5zZWRcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuaGVhZFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuY29udFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuZm9vdFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdCc8L3RhYmxlPicrXG5cdFx0XHRcdFx0XHRcdCc8L2Rpdj4nK1xuXHRcdFx0XHRcdFx0XHQnPGRpdiBjbGFzcz1cImRhdGVwaWNrZXIteWVhcnNcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGFibGUgY2xhc3M9XCJ0YWJsZS1jb25kZW5zZWRcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuaGVhZFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuY29udFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuZm9vdFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdCc8L3RhYmxlPicrXG5cdFx0XHRcdFx0XHRcdCc8L2Rpdj4nK1xuXHRcdFx0XHRcdFx0XHQnPGRpdiBjbGFzcz1cImRhdGVwaWNrZXItZGVjYWRlc1wiPicrXG5cdFx0XHRcdFx0XHRcdFx0Jzx0YWJsZSBjbGFzcz1cInRhYmxlLWNvbmRlbnNlZFwiPicrXG5cdFx0XHRcdFx0XHRcdFx0XHREUEdsb2JhbC5oZWFkVGVtcGxhdGUrXG5cdFx0XHRcdFx0XHRcdFx0XHREUEdsb2JhbC5jb250VGVtcGxhdGUrXG5cdFx0XHRcdFx0XHRcdFx0XHREUEdsb2JhbC5mb290VGVtcGxhdGUrXG5cdFx0XHRcdFx0XHRcdFx0JzwvdGFibGU+Jytcblx0XHRcdFx0XHRcdFx0JzwvZGl2PicrXG5cdFx0XHRcdFx0XHRcdCc8ZGl2IGNsYXNzPVwiZGF0ZXBpY2tlci1jZW50dXJpZXNcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdCc8dGFibGUgY2xhc3M9XCJ0YWJsZS1jb25kZW5zZWRcIj4nK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuaGVhZFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuY29udFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdFx0RFBHbG9iYWwuZm9vdFRlbXBsYXRlK1xuXHRcdFx0XHRcdFx0XHRcdCc8L3RhYmxlPicrXG5cdFx0XHRcdFx0XHRcdCc8L2Rpdj4nK1xuXHRcdFx0XHRcdFx0JzwvZGl2Pic7XG5cblx0JC5mbi5kYXRlcGlja2VyLkRQR2xvYmFsID0gRFBHbG9iYWw7XG5cblxuXHQvKiBEQVRFUElDS0VSIE5PIENPTkZMSUNUXG5cdCogPT09PT09PT09PT09PT09PT09PSAqL1xuXG5cdCQuZm4uZGF0ZXBpY2tlci5ub0NvbmZsaWN0ID0gZnVuY3Rpb24oKXtcblx0XHQkLmZuLmRhdGVwaWNrZXIgPSBvbGQ7XG5cdFx0cmV0dXJuIHRoaXM7XG5cdH07XG5cblx0LyogREFURVBJQ0tFUiBWRVJTSU9OXG5cdCAqID09PT09PT09PT09PT09PT09PT0gKi9cblx0JC5mbi5kYXRlcGlja2VyLnZlcnNpb24gPSAnMS42LjQnO1xuXG5cdC8qIERBVEVQSUNLRVIgREFUQS1BUElcblx0KiA9PT09PT09PT09PT09PT09PT0gKi9cblxuXHQkKGRvY3VtZW50KS5vbihcblx0XHQnZm9jdXMuZGF0ZXBpY2tlci5kYXRhLWFwaSBjbGljay5kYXRlcGlja2VyLmRhdGEtYXBpJyxcblx0XHQnW2RhdGEtcHJvdmlkZT1cImRhdGVwaWNrZXJcIl0nLFxuXHRcdGZ1bmN0aW9uKGUpe1xuXHRcdFx0dmFyICR0aGlzID0gJCh0aGlzKTtcblx0XHRcdGlmICgkdGhpcy5kYXRhKCdkYXRlcGlja2VyJykpXG5cdFx0XHRcdHJldHVybjtcblx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdC8vIGNvbXBvbmVudCBjbGljayByZXF1aXJlcyB1cyB0byBleHBsaWNpdGx5IHNob3cgaXRcblx0XHRcdGRhdGVwaWNrZXJQbHVnaW4uY2FsbCgkdGhpcywgJ3Nob3cnKTtcblx0XHR9XG5cdCk7XG5cdCQoZnVuY3Rpb24oKXtcblx0XHRkYXRlcGlja2VyUGx1Z2luLmNhbGwoJCgnW2RhdGEtcHJvdmlkZT1cImRhdGVwaWNrZXItaW5saW5lXCJdJykpO1xuXHR9KTtcblxufSkpO1xuIiwiIWZ1bmN0aW9uKGEpe2EuZm4uZGF0ZXBpY2tlci5kYXRlcy5kZT17ZGF5czpbXCJTb25udGFnXCIsXCJNb250YWdcIixcIkRpZW5zdGFnXCIsXCJNaXR0d29jaFwiLFwiRG9ubmVyc3RhZ1wiLFwiRnJlaXRhZ1wiLFwiU2Ftc3RhZ1wiXSxkYXlzU2hvcnQ6W1wiU29uXCIsXCJNb25cIixcIkRpZVwiLFwiTWl0XCIsXCJEb25cIixcIkZyZVwiLFwiU2FtXCJdLGRheXNNaW46W1wiU29cIixcIk1vXCIsXCJEaVwiLFwiTWlcIixcIkRvXCIsXCJGclwiLFwiU2FcIl0sbW9udGhzOltcIkphbnVhclwiLFwiRmVicnVhclwiLFwiTcOkcnpcIixcIkFwcmlsXCIsXCJNYWlcIixcIkp1bmlcIixcIkp1bGlcIixcIkF1Z3VzdFwiLFwiU2VwdGVtYmVyXCIsXCJPa3RvYmVyXCIsXCJOb3ZlbWJlclwiLFwiRGV6ZW1iZXJcIl0sbW9udGhzU2hvcnQ6W1wiSmFuXCIsXCJGZWJcIixcIk3DpHJcIixcIkFwclwiLFwiTWFpXCIsXCJKdW5cIixcIkp1bFwiLFwiQXVnXCIsXCJTZXBcIixcIk9rdFwiLFwiTm92XCIsXCJEZXpcIl0sdG9kYXk6XCJIZXV0ZVwiLG1vbnRoc1RpdGxlOlwiTW9uYXRlXCIsY2xlYXI6XCJMw7ZzY2hlblwiLHdlZWtTdGFydDoxLGZvcm1hdDpcImRkLm1tLnl5eXlcIn19KGpRdWVyeSk7IiwiIWZ1bmN0aW9uKGEpe2EuZm4uZGF0ZXBpY2tlci5kYXRlc1tcImVuLUdCXCJdPXtkYXlzOltcIlN1bmRheVwiLFwiTW9uZGF5XCIsXCJUdWVzZGF5XCIsXCJXZWRuZXNkYXlcIixcIlRodXJzZGF5XCIsXCJGcmlkYXlcIixcIlNhdHVyZGF5XCJdLGRheXNTaG9ydDpbXCJTdW5cIixcIk1vblwiLFwiVHVlXCIsXCJXZWRcIixcIlRodVwiLFwiRnJpXCIsXCJTYXRcIl0sZGF5c01pbjpbXCJTdVwiLFwiTW9cIixcIlR1XCIsXCJXZVwiLFwiVGhcIixcIkZyXCIsXCJTYVwiXSxtb250aHM6W1wiSmFudWFyeVwiLFwiRmVicnVhcnlcIixcIk1hcmNoXCIsXCJBcHJpbFwiLFwiTWF5XCIsXCJKdW5lXCIsXCJKdWx5XCIsXCJBdWd1c3RcIixcIlNlcHRlbWJlclwiLFwiT2N0b2JlclwiLFwiTm92ZW1iZXJcIixcIkRlY2VtYmVyXCJdLG1vbnRoc1Nob3J0OltcIkphblwiLFwiRmViXCIsXCJNYXJcIixcIkFwclwiLFwiTWF5XCIsXCJKdW5cIixcIkp1bFwiLFwiQXVnXCIsXCJTZXBcIixcIk9jdFwiLFwiTm92XCIsXCJEZWNcIl0sdG9kYXk6XCJUb2RheVwiLG1vbnRoc1RpdGxlOlwiTW9udGhzXCIsY2xlYXI6XCJDbGVhclwiLHdlZWtTdGFydDoxLGZvcm1hdDpcImRkL21tL3l5eXlcIn19KGpRdWVyeSk7IiwiLy8gRmx5LW91dCAvIERyb3Bkb3duIE1lbnVcbihmdW5jdGlvbigkKXtcbiAgICAkKGZ1bmN0aW9uKCl7XG4gICAgICAgICQoJ1tkYXRhLWVzLWRyb3Bkb3duXSA+IGJ1dHRvbiwgW2RhdGEtZXMtZHJvcGRvd25dID4gYScpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICB2YXIgJHAgPSAkKHRoaXMpLnBhcmVudCgpLCBvID0gJHAuaGFzQ2xhc3MoJy1lcy1vcGVuJyk7XG4gICAgICAgICAgICAkKCdbZGF0YS1lcy1kcm9wZG93bl0nKS5yZW1vdmVDbGFzcygnLWVzLW9wZW4nKTtcbiAgICAgICAgICAgICFvICYmICRwLmFkZENsYXNzKCctZXMtb3BlbicpO1xuICAgICAgICB9KTtcbiAgICAgICAgJChkb2N1bWVudCkub24oJ2NsaWNrJywgZnVuY3Rpb24oZSl7XG4gICAgICAgICAgICAhICQoZS50YXJnZXQpLmNsb3Nlc3QoJ1tkYXRhLWVzLWRyb3Bkb3duXSA+IGJ1dHRvbiwgW2RhdGEtZXMtZHJvcGRvd25dID4gYScpLmxlbmd0aCAmJlxuICAgICAgICAgICAgJCgnW2RhdGEtZXMtZHJvcGRvd25dJykucmVtb3ZlQ2xhc3MoJy1lcy1vcGVuJyk7XG4gICAgICAgIH0pXG4gICAgfSk7XG59KShqUXVlcnkpO1xuIiwiKGZ1bmN0aW9uIChyb290LCBmYWN0b3J5KSB7XG4gICAgdmFyIGRvY3VtZW50ID0gdGhpcy5kb2N1bWVudDtcbiAgICBpZiAodHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcgJiYgbW9kdWxlLmV4cG9ydHMpIHtcbiAgICAgICAgbW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KGRvY3VtZW50KTtcbiAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgbnMgPSByb290LmVydCB8fCAocm9vdC5lcnQgPSB7fSk7XG4gICAgICAgIG5zLm5vdGlmaWNhdGlvbnMgPSBmYWN0b3J5KGRvY3VtZW50KTtcbiAgICB9XG59KHRoaXMsIGZ1bmN0aW9uIChkb2N1bWVudCkge1xuXG4gICAgdmFyIGJvZHk7XG4gICAgdmFyIG5vdGlmaWNhdGlvbnNPcHRpb25zID0ge1xuICAgICAgICBjb250YWluZXJDbGFzczogJycsXG4gICAgICAgIGFwcGVhcmFuY2VQb3NpdGlvbjogJ3JpZ2h0JyxcbiAgICAgICAgYXBwZWFyYW5jZVdpZHRoOiAnJ1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDb25maWd1cmUgbm90aWZpY2F0aW9uIG9wdGlvbnMuXG4gICAgICogYXBwZWFyYW5jZVBvc2l0aW9uOiBsZWZ0LCBjZW50ZXIgb3IgcmlnaHRcbiAgICAgKiBhcHBlYXJhbmNlV2lkdGg6IHNtYWxsXG4gICAgICogQHJldHVybiB7dm9pZH1cbiAgICAgKi9cbiAgICBmdW5jdGlvbiBjb25maWd1cmVOb3RpZmljYXRpb25zKG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zKSBvcHRpb25zID0ge307XG4gICAgICAgIGJvZHkgPSBvcHRpb25zLmJvZHkgfHwgZG9jdW1lbnQuYm9keTtcbiAgICAgICAgZGVmYXVsdE1lc3NhZ2VPcHRpb25zLnRpbWVvdXQgPSBvcHRpb25zLnRpbWVvdXQgfHwgZGVmYXVsdE1lc3NhZ2VPcHRpb25zLnRpbWVvdXQ7XG4gICAgICAgIGRlZmF1bHRNZXNzYWdlT3B0aW9ucy5hbmltYXRpb24gPSBvcHRpb25zLmFuaW1hdGlvbiB8fCBkZWZhdWx0TWVzc2FnZU9wdGlvbnMuYW5pbWF0aW9uO1xuICAgICAgICBub3RpZmljYXRpb25zT3B0aW9ucy5hcHBlYXJhbmNlUG9zaXRpb24gPSBvcHRpb25zLmFwcGVhcmFuY2VQb3NpdGlvbiB8fCBub3RpZmljYXRpb25zT3B0aW9ucy5hcHBlYXJhbmNlUG9zaXRpb247XG4gICAgICAgIG5vdGlmaWNhdGlvbnNPcHRpb25zLmFwcGVhcmFuY2VXaWR0aCA9IG9wdGlvbnMuYXBwZWFyYW5jZVdpZHRoIHx8IG5vdGlmaWNhdGlvbnNPcHRpb25zLmFwcGVhcmFuY2VXaWR0aDtcbiAgICAgICAgbm90aWZpY2F0aW9uc09wdGlvbnMuY29udGFpbmVyQ2xhc3MgPSBvcHRpb25zLmNvbnRhaW5lckNsYXNzIHx8IG5vdGlmaWNhdGlvbnNPcHRpb25zLmNvbnRhaW5lckNsYXNzO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFNob3cgbm90aWZpY2F0aW9uIG1lc3NhZ2UuXG4gICAgICogQHBhcmFtICB7T2JqZWN0fSBvcHRpb25zIE9wdGlvbnNcbiAgICAgKiBAcmV0dXJuIHt2b2lkfVxuICAgICAqL1xuICAgIGZ1bmN0aW9uIG5vdGlmaWNhdGlvbk1lc3NhZ2Uob3B0aW9ucykge1xuICAgICAgICB2YXIgbWVzc2FnZSA9IG5ldyBNZXNzYWdlKG9wdGlvbnMpO1xuICAgICAgICB2YXIgbm90aWZpY2F0aW9uc0JhckVsZW1lbnQgPSBjcmVhdGVOb3RpZmljYXRpb25zQmFyKG9wdGlvbnMpO1xuICAgICAgICBub3RpZmljYXRpb25zQmFyRWxlbWVudC5pbnNlcnRCZWZvcmUobWVzc2FnZS5lbGVtZW50LCBub3RpZmljYXRpb25zQmFyRWxlbWVudC5maXJzdENoaWxkKTtcbiAgICAgICAgaWYgKG1lc3NhZ2UudHlwZSgpICE9PSAnZGFuZ2VyJykge1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgcmVtb3ZlRWxlbWVudChtZXNzYWdlLmVsZW1lbnQpO1xuICAgICAgICAgICAgfSwgbWVzc2FnZS5vcHRpb24oJ3RpbWVvdXQnLCAxNTAwMCkpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBtZXNzYWdlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIENyZWF0ZSBvciBnZXQgZXhpc3Rpbmcgbm90aWZpY2F0aW9ucyBiYXIgY29udGFpbmVyLlxuICAgICAqIEBwYXJhbSAge09iamVjdH0gb3B0aW9ucyBPcHRpb25zIGZvciBub3RpZmljYXRpb25zIGJhcjpcbiAgICAgKiBhcHBlYXJhbmNlUG9zaXRpb246IGxlZnQsIGNlbnRlciBvciByaWdodFxuICAgICAqIGFwcGVhcmFuY2VXaWR0aDogZnVsbCBvciBzbWFsbFxuICAgICAqIEByZXR1cm4ge0hUTUxFbGVtZW50fSBOb3RpZmljYXRpb25zIGJhciBjb250YWluZXIgZWxlbWVudC5cbiAgICAgKi9cbiAgICBmdW5jdGlvbiBjcmVhdGVOb3RpZmljYXRpb25zQmFyKG9wdGlvbnMpIHtcbiAgICAgICAgdmFyIG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50ID0gYm9keS5xdWVyeVNlbGVjdG9yKCcuZXMtbm90aWZpY2F0aW9uLWNlbnRlcicpO1xuICAgICAgICB2YXIgYXBwZWFyYW5jZVBvc2l0aW9uID0gZ2V0VmFsdWUob3B0aW9ucywgJ2FwcGVhcmFuY2VQb3NpdGlvbicsIG5vdGlmaWNhdGlvbnNPcHRpb25zLmFwcGVhcmFuY2VQb3NpdGlvbik7XG4gICAgICAgIHZhciBhcHBlYXJhbmNlV2lkdGggPSBnZXRWYWx1ZShvcHRpb25zLCAnYXBwZWFyYW5jZVdpZHRoJywgbm90aWZpY2F0aW9uc09wdGlvbnMuYXBwZWFyYW5jZVdpZHRoKTtcbiAgICAgICAgaWYgKG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50XG4gICAgICAgICAgICAmJiBkYXRhc2V0KG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50LCAncG9zaXRpb24nKSA9PT0gYXBwZWFyYW5jZVBvc2l0aW9uXG4gICAgICAgICAgICAmJiBkYXRhc2V0KG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50LCAnd2lkdGgnKSA9PT0gYXBwZWFyYW5jZVdpZHRoKSB7XG4gICAgICAgICAgICByZXR1cm4gbm90aWZpY2F0aW9uc0JhckVsZW1lbnQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAobm90aWZpY2F0aW9uc0JhckVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICBib2R5LnJlbW92ZUNoaWxkKG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB2YXIgbm90aWZpY2F0aW9uQmFyVGVtcGxhdGUgPSAnPGRpdiBjbGFzcz1cImVzLW5vdGlmaWNhdGlvbi1jZW50ZXJcIj48L2Rpdj4nO1xuICAgICAgICBub3RpZmljYXRpb25zQmFyRWxlbWVudCA9IGNyZWF0ZUVsZW1lbnQobm90aWZpY2F0aW9uQmFyVGVtcGxhdGUpO1xuXG4gICAgICAgIGRhdGFzZXQobm90aWZpY2F0aW9uc0JhckVsZW1lbnQsICdwb3NpdGlvbicsIGFwcGVhcmFuY2VQb3NpdGlvbik7XG4gICAgICAgIGFkZENsYXNzKG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50LCBlcyhhcHBlYXJhbmNlUG9zaXRpb24pKTtcblxuICAgICAgICBkYXRhc2V0KG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50LCAnd2lkdGgnLCBhcHBlYXJhbmNlV2lkdGgpO1xuICAgICAgICBhcHBlYXJhbmNlV2lkdGggJiYgYWRkQ2xhc3Mobm90aWZpY2F0aW9uc0JhckVsZW1lbnQsIGVzKGFwcGVhcmFuY2VXaWR0aCkpO1xuXG4gICAgICAgIGlmIChub3RpZmljYXRpb25zT3B0aW9ucy5jb250YWluZXJDbGFzcykge1xuICAgICAgICAgICAgYWRkQ2xhc3Mobm90aWZpY2F0aW9uc0JhckVsZW1lbnQsIG5vdGlmaWNhdGlvbnNPcHRpb25zLmNvbnRhaW5lckNsYXNzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGJvZHkuYXBwZW5kQ2hpbGQobm90aWZpY2F0aW9uc0JhckVsZW1lbnQpO1xuICAgICAgICBpbml0aWFsaXplRXZlbnRzKG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50KTtcbiAgICAgICAgcmV0dXJuIG5vdGlmaWNhdGlvbnNCYXJFbGVtZW50O1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemUgZXZlbnRzIGRlbGVnYXRpb24uXG4gICAgICogQHBhcmFtICB7SFRNTEVsZW1lbnR9IGNvbnRhaW5lclxuICAgICAqIEByZXR1cm4ge3ZvaWR9XG4gICAgICovXG4gICAgZnVuY3Rpb24gaW5pdGlhbGl6ZUV2ZW50cyhjb250YWluZXIpIHtcblxuICAgICAgICBmdW5jdGlvbiB0cmF2ZXJzZShub2RlKSB7XG4gICAgICAgICAgICBpZiAobm9kZS5tYXRjaGVzKCcuZXMtbm90aWZpY2F0aW9uJykpIHtcbiAgICAgICAgICAgICAgICAvLyBNZXNzYWdlcyBzaG91bGQgY2xvc2Ugd2hlbiB5b3UgY2xpY2sgb24gdGhlbSBhbmQgZGFuZ2VyIG1lc3NhZ2VzIGFyZSBvbmx5IGNsb3NpbmcgaWYgeW91IGRpc21pc3MgdGhlbS5cbiAgICAgICAgICAgICAgICBpZiAoZGF0YXNldChub2RlLCAndHlwZScpICE9PSAnZGFuZ2VyJykge1xuICAgICAgICAgICAgICAgICAgICByZW1vdmVFbGVtZW50KG5vZGUpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSBpZiAobm9kZS5tYXRjaGVzKCcuZXMtbm90aWZpY2F0aW9uIC5lcy1jbG9zZScpKSB7XG4gICAgICAgICAgICAgICAgcmVtb3ZlRWxlbWVudChub2RlLnBhcmVudE5vZGUpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB2YXIgcGFyZW50Tm9kZSA9IG5vZGUucGFyZW50Tm9kZTtcbiAgICAgICAgICAgICAgICBpZiAocGFyZW50Tm9kZSkge1xuICAgICAgICAgICAgICAgICAgICB0cmF2ZXJzZShwYXJlbnROb2RlKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBjb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgdHJhdmVyc2UoZS50YXJnZXQpO1xuICAgICAgICB9KTtcblxuICAgIH1cblxuICAgIHZhciBkZWZhdWx0TWVzc2FnZU9wdGlvbnMgPSB7XG4gICAgICAgIGFuaW1hdGlvbjogJ3NsaWRlLWluLXJpZ2h0JyxcbiAgICAgICAgcHJpb3JpdHk6ICdub3JtYWwnLFxuICAgICAgICB0aW1lb3V0OiAxNTAwMCxcbiAgICAgICAgdHlwZTogJydcbiAgICB9O1xuXG4gICAgdmFyIHR5cGVJY29ucyA9IHtcbiAgICAgICAgaW5mbzogJ2luZm8nLFxuICAgICAgICBzdWNjZXNzOiAnYWxlcnQtc3VjY2VzcycsXG4gICAgICAgIHdhcm5pbmc6ICdhbGVydC13YXJuaW5nJyxcbiAgICAgICAgZGFuZ2VyOiAnYWxlcnQtZGFuZ2VyJ1xuICAgIH07XG5cbiAgICAvKipcbiAgICAgKiBDcmVhdGUgbWVzc2FnZSBvYmplY3QuXG4gICAgICogQHBhcmFtIHtPYmplY3R9IG9wdGlvbnM6XG4gICAgICogYW5pbWF0aW9uOiBzbGlkZS1pbi1yaWdodCBvciBmYWRlLWluLWRvd24gKGRlZmF1bHQ6IHNsaWRlLWluLXJpZ2h0KVxuICAgICAqIHByaW9yaXR5OiBoaWdoLCBub3JtYWwgb3IgbG93IChkZWZhdWx0OiBub3JtYWwpXG4gICAgICogdHlwZTogc3VjY2VzcywgaW5mbywgd2FybmluZyBvciBkYW5nZXIgKGRlZmF1bHQ6IG5vbmUpXG4gICAgICovXG4gICAgZnVuY3Rpb24gTWVzc2FnZShvcHRpb25zKSB7XG4gICAgICAgIHRoaXMub3B0aW9ucyA9IG9wdGlvbnM7XG4gICAgICAgIHRoaXMuZWxlbWVudCA9IGNyZWF0ZUVsZW1lbnQoJzxkaXYgY2xhc3M9XCJlcy1ub3RpZmljYXRpb25cIj48c3BhbiBjbGFzcz1cImVzLWljb25cIj48L3NwYW4+PHA+PC9wPjxzcGFuIGNsYXNzPVwiZXMtaWNvbiBlcy1pY29uLWNsb3NlIGVzLWNsb3NlXCI+PC9zcGFuPjwvZGl2PicpO1xuICAgICAgICB0aGlzLnRleHRFbGVtZW50ID0gdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJ3AnKTtcbiAgICAgICAgdGhpcy5pY29uRWxlbWVudCA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuZXMtaWNvbicpO1xuICAgICAgICB0aGlzLmNsb3NlRWxlbWVudCA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuY2xvc2UnKTtcblxuICAgICAgICB0aGlzLnRleHQodGhpcy5vcHRpb24oJ3RleHQnLCAnJykpO1xuICAgICAgICB0aGlzLnR5cGUodGhpcy5vcHRpb24oJ3R5cGUnKSk7XG4gICAgICAgIHRoaXMuaWNvbih0aGlzLm9wdGlvbignaWNvbicpKTtcbiAgICAgICAgdGhpcy5pY29uU2l6ZSh0aGlzLm9wdGlvbignaWNvblNpemUnKSk7XG4gICAgICAgIHRoaXMucHJpb3JpdHkodGhpcy5vcHRpb24oJ3ByaW9yaXR5JykpO1xuICAgICAgICB0aGlzLmFuaW1hdGlvbih0aGlzLm9wdGlvbignYW5pbWF0aW9uJykpO1xuXG4gICAgICAgIGRhdGFzZXQodGhpcy5lbGVtZW50LCAndHlwZScsIHRoaXMudHlwZSgpKTtcbiAgICB9XG5cbiAgICBNZXNzYWdlLnByb3RvdHlwZS5vcHRpb24gPSBmdW5jdGlvbiAobmFtZSwgZGVmYXVsdFZhbHVlKSB7XG4gICAgICAgIGlmIChkZWZhdWx0VmFsdWUgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgZGVmYXVsdFZhbHVlID0gZGVmYXVsdE1lc3NhZ2VPcHRpb25zW25hbWVdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBnZXRWYWx1ZSh0aGlzLm9wdGlvbnMsIG5hbWUsIGRlZmF1bHRWYWx1ZSk7XG4gICAgfTtcblxuICAgIE1lc3NhZ2UucHJvdG90eXBlLnRleHQgPSBmdW5jdGlvbiAodGV4dCkge1xuICAgICAgICB0aGlzLnRleHRFbGVtZW50LmlubmVySFRNTCA9IHRleHQ7XG4gICAgfTtcblxuICAgIE1lc3NhZ2UucHJvdG90eXBlLnR5cGUgPSBmdW5jdGlvbiAodHlwZSkge1xuICAgICAgICBpZiAodHlwZSA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy50eXBlVmFsdWU7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy50eXBlVmFsdWUgPSB0eXBlO1xuICAgICAgICBhZGRDbGFzcyh0aGlzLmVsZW1lbnQsIGVzKHR5cGUpKTtcblxuICAgICAgICB0aGlzLmljb24odHlwZUljb25zW3R5cGVdKTtcbiAgICB9O1xuXG4gICAgTWVzc2FnZS5wcm90b3R5cGUuaWNvbiA9IGZ1bmN0aW9uIChpY29uKSB7XG4gICAgICAgIGlmIChpY29uKSB7XG4gICAgICAgICAgICB0aGlzLmljb25FbGVtZW50LmNsYXNzTmFtZSA9IHRoaXMuaWNvbkVsZW1lbnQuY2xhc3NOYW1lXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoL2VzLWljb24tXFxTKy9nLCAnJylcbiAgICAgICAgICAgICAgICAucmVwbGFjZSgvXFxzKy9nLCAnICcpXG4gICAgICAgICAgICAgICAgLnJlcGxhY2UoLyheXFxzK3xcXHMrJCkvZywgJycpO1xuICAgICAgICAgICAgYWRkQ2xhc3ModGhpcy5pY29uRWxlbWVudCwgJ2VzLWljb24tJyArIGljb24pO1xuICAgICAgICB9XG4gICAgfTtcblxuICAgIE1lc3NhZ2UucHJvdG90eXBlLmljb25TaXplID0gZnVuY3Rpb24gKHNpemUpIHtcbiAgICAgICAgaWYgKHNpemUpIHtcbiAgICAgICAgICAgIGFkZENsYXNzKHRoaXMuaWNvbkVsZW1lbnQsIGVzKHNpemUpKTtcbiAgICAgICAgfVxuICAgIH07XG5cbiAgICBNZXNzYWdlLnByb3RvdHlwZS5wcmlvcml0eSA9IGZ1bmN0aW9uIChwKSB7XG4gICAgICAgIGFkZENsYXNzKHRoaXMuZWxlbWVudCwgZXMoJ3ByaW9yaXR5LScgKyBwKSk7XG4gICAgfTtcblxuICAgIE1lc3NhZ2UucHJvdG90eXBlLmFuaW1hdGlvbiA9IGZ1bmN0aW9uIChhbmltYXRpb24pIHtcbiAgICAgICAgYWRkQ2xhc3ModGhpcy5lbGVtZW50LCAnLWVzLWFuaW1hdGVkICcgKyBlcyhhbmltYXRpb24pKTtcbiAgICB9O1xuXG4gICAgLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4gICAgLy8gSGVscGVycy5cbiAgICAvLyA9PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cblxuICAgIGZ1bmN0aW9uIGNyZWF0ZUVsZW1lbnQoaHRtbCkge1xuICAgICAgICB2YXIgdGVtcGxhdGUgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0ZW1wbGF0ZScpO1xuICAgICAgICB0ZW1wbGF0ZS5pbm5lckhUTUwgPSBodG1sO1xuICAgICAgICBpZiAodGVtcGxhdGUuY29udGVudCAmJiB0ZW1wbGF0ZS5jb250ZW50LmZpcnN0Q2hpbGQpIHtcbiAgICAgICAgICAgIHJldHVybiB0ZW1wbGF0ZS5jb250ZW50LmZpcnN0Q2hpbGQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRlbXBsYXRlLmZpcnN0Q2hpbGQ7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gYWRkQ2xhc3Mobm9kZSwgY2xhc3NOYW1lKSB7XG4gICAgICAgIG5vZGUuY2xhc3NOYW1lICs9ICcgJyArIGNsYXNzTmFtZTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBlcyh2YWx1ZSkge1xuICAgICAgICByZXR1cm4gJy1lcy0nICsgdmFsdWU7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZGF0YXNldChlbGVtZW50LCBuYW1lLCB2YWx1ZSkge1xuICAgICAgICB2YXIgYXR0ck5hbWUgPSAnZGF0YS0nICsgbmFtZTtcbiAgICAgICAgcmV0dXJuICh2YWx1ZSA9PT0gdW5kZWZpbmVkKSA/IGVsZW1lbnQuZ2V0QXR0cmlidXRlKGF0dHJOYW1lKSA6IGVsZW1lbnQuc2V0QXR0cmlidXRlKGF0dHJOYW1lLCB2YWx1ZSk7XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gcmVtb3ZlRWxlbWVudChlbGVtZW50KSB7XG4gICAgICAgIHZhciBub2RlID0gZWxlbWVudCAmJiBlbGVtZW50LnBhcmVudE5vZGU7XG4gICAgICAgIGlmIChub2RlKSB7XG4gICAgICAgICAgICBub2RlLnJlbW92ZUNoaWxkKGVsZW1lbnQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gZ2V0VmFsdWUoZGF0YSwga2V5LCBkZWZhdWx0VmFsdWUsIHJlbW92ZSkge1xuICAgICAgICB2YXIgcmVzdWx0ID0gZGVmYXVsdFZhbHVlO1xuICAgICAgICBpZiAodHlwZW9mIGRhdGFba2V5XSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IGRhdGFba2V5XTtcbiAgICAgICAgfVxuICAgICAgICBpZiAocmVtb3ZlKSB7XG4gICAgICAgICAgICBkZWxldGUgZGF0YVtrZXldO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfVxuXG4gICAgcmV0dXJuIHtcbiAgICAgICAgbWVzc2FnZTogbm90aWZpY2F0aW9uTWVzc2FnZSxcbiAgICAgICAgY29uZmlndXJlOiBjb25maWd1cmVOb3RpZmljYXRpb25zXG4gICAgfTtcbn0pKTtcblxuLy8gaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4vZG9jcy9XZWIvQVBJL0VsZW1lbnQvbWF0Y2hlc1xuaWYgKCFFbGVtZW50LnByb3RvdHlwZS5tYXRjaGVzKSB7XG4gICAgRWxlbWVudC5wcm90b3R5cGUubWF0Y2hlcyA9XG4gICAgICAgIEVsZW1lbnQucHJvdG90eXBlLm1hdGNoZXNTZWxlY3RvciB8fFxuICAgICAgICBFbGVtZW50LnByb3RvdHlwZS5tb3pNYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgRWxlbWVudC5wcm90b3R5cGUubXNNYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgRWxlbWVudC5wcm90b3R5cGUub01hdGNoZXNTZWxlY3RvciB8fFxuICAgICAgICBFbGVtZW50LnByb3RvdHlwZS53ZWJraXRNYXRjaGVzU2VsZWN0b3IgfHxcbiAgICAgICAgZnVuY3Rpb24ocykge1xuICAgICAgICAgICAgdmFyIG1hdGNoZXMgPSAodGhpcy5kb2N1bWVudCB8fCB0aGlzLm93bmVyRG9jdW1lbnQpLnF1ZXJ5U2VsZWN0b3JBbGwocyksXG4gICAgICAgICAgICAgICAgaSA9IG1hdGNoZXMubGVuZ3RoO1xuICAgICAgICAgICAgd2hpbGUgKC0taSA+PSAwICYmIG1hdGNoZXMuaXRlbShpKSAhPT0gdGhpcyk7XG4gICAgICAgICAgICByZXR1cm4gaSA+IC0xO1xuICAgICAgICB9O1xufSIsIi8vIGNvbGxhcHNpYmxlIHBhbmVsc1xuKGZ1bmN0aW9uKCQpe1xuICAgICQoZnVuY3Rpb24oKXtcbiAgICAgICAgJCgnW2RhdGEtdG9nZ2xlPVwicGFuZWxcIl0nKS5vbignY2xpY2snLCBmdW5jdGlvbihlKXtcbiAgICAgICAgICAgICQoZS50YXJnZXQpLmNsb3Nlc3QoJy5lcy1jb2xsYXBzaWJsZS1wYW5lbCcpLnRvZ2dsZUNsYXNzKCctZXMtb3BlbicpO1xuICAgICAgICB9KTtcbiAgICB9KTtcbn0pKGpRdWVyeSk7XG4iLCIvLyBEYXNoYm9hcmQgd2lkZ2V0XG4oZnVuY3Rpb24gKCQpIHtcbiAgICAkKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgnW2RhdGEtZXMtZXhwYW5kLWNvbGxhcHNlXScpLm9uKCdjbGljaycsIGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICB2YXIgJHRhcmdldCA9ICQoZS50YXJnZXQpLFxuICAgICAgICAgICAgICAgICR3aWRnZXQgPSAkdGFyZ2V0LmNsb3Nlc3QoJy5lcy13aWRnZXQnKSxcbiAgICAgICAgICAgICAgICAkaGVhZGVyID0gJHdpZGdldC5maW5kKCcuZXMtd2lkZ2V0LWhlYWRlcicpLFxuICAgICAgICAgICAgICAgICRpY29uID0gJGhlYWRlci5maW5kKCcuZXMtaWNvbicpLFxuICAgICAgICAgICAgICAgICRjb250ZW50ID0gJHdpZGdldC5maW5kKCcuZXMtd2lkZ2V0LWJvZHknKTtcblxuICAgICAgICAgICAgaWYgKCR3aWRnZXQuaGFzQ2xhc3MoJy1lcy1jb2xsYXBzZWQnKSkge1xuICAgICAgICAgICAgICAgICRjb250ZW50LnNob3coKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJGNvbnRlbnQuaGlkZSgpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAkd2lkZ2V0LnRvZ2dsZUNsYXNzKCctZXMtY29sbGFwc2VkJyk7XG4gICAgICAgICAgICAkaWNvbi50b2dnbGVDbGFzcygnZXMtaWNvbi1jaGV2cm9uLXJpZ2h0IGVzLWljb24tY2hldnJvbi1kb3duJyk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgICQod2luZG93KS5vbigncmVzaXplJywgY2xvc2UpO1xuICAgICAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCBjbG9zZSk7XG5cbiAgICAgICAgZnVuY3Rpb24gY2xvc2UoKSB7XG4gICAgICAgICAgICB2YXIgJHdpZGdldCA9ICQoZG9jdW1lbnQpLmZpbmQoJy5lcy13aWRnZXQnKSxcbiAgICAgICAgICAgICAgICAkbmF2ID0gJHdpZGdldC5maW5kKCduYXYnKTtcblxuICAgICAgICAgICAgJG5hdi5yZW1vdmVDbGFzcygnLWVzLW9wZW4nKTtcbiAgICAgICAgfVxuICAgIH0pO1xufSkoalF1ZXJ5KTtcbiIsIi8vIFRhYnNcbihmdW5jdGlvbiAoJCkge1xuICAgICQoJy5lcy10YWIgYScpLmNsaWNrKGZ1bmN0aW9uKGUpe1xuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG5cbiAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKTtcbiAgICAgICAgdmFyICR0YWJzID0gJHRoaXMucGFyZW50cygnLmVzLXRhYnMnKTtcbiAgICAgICAgdmFyICR0YWJQYW5lbHMgPSAkKCcjJyskdGhpcy5wYXJlbnRzKCcuZXMtdGFicycpLmRhdGEoJ3BhbmVsLWdyb3VwJykpO1xuICAgICAgICB2YXIgdGFyZ2V0ID0gJHRoaXMuYXR0cignaHJlZicpO1xuICAgICAgICB2YXIgJHRhcmdldFBhbmVsID0gJCh0YXJnZXQpO1xuICAgICAgICB2YXIgJHRhcmdldFRhYiA9ICR0YWJzLm5vdCgnLi1lcy1tb3JlJykuZmluZCgnbGkgYVtocmVmPVwiJyt0YXJnZXQrJ1wiXScpO1xuICAgICAgICAgICAgXG4gICAgICAgIC8vIHNldCBlbWJlZGRlZCB0YWIgcGFuZWxzIGluYWN0aXZlXG4gICAgICAgICR0YWJQYW5lbHMuZmluZCgnLmVzLXBhbmVsJykucmVtb3ZlQ2xhc3MoJy1lcy1hY3RpdmUnKTtcbiAgICAgICAgLy8gc2V0IG5hdmlnYXRpb24gZWxlbWVudHMgaW5hY3RpdmVcbiAgICAgICAgJHRhYnMuZmluZCgnbGknKS5yZW1vdmVDbGFzcygnLWVzLWFjdGl2ZScpO1xuXG4gICAgICAgIC8vIHNldCB0YXJnZXQgcGFuZWwgYWN0aXZlIFxuICAgICAgICAkdGFyZ2V0UGFuZWwuYWRkQ2xhc3MoJy1lcy1hY3RpdmUnKTtcbiAgICAgICAgLy8gbWFyayB0YXJnZXQgcGFuZWwgbmF2aWdhdGlvbiBlbGVtZW50IGFjdGl2ZVxuICAgICAgICAkdGFyZ2V0VGFiLnBhcmVudCgpLmFkZENsYXNzKCctZXMtYWN0aXZlJyk7XG4gICAgfSk7XG59KShqUXVlcnkpO1xuXG4iLCIvLyBEYXRlIFBpY2tlciBwb3B1cFxuKGZ1bmN0aW9uICgkKSB7XG4gICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgICQoJy5lcy1kYXRlLWlucHV0ID4gaW5wdXQnKS5kYXRlcGlja2VyKHtcbiAgICAgICAgICAgIGF1dG9jbG9zZTogdHJ1ZSxcbiAgICAgICAgICAgIGZvcm1hdDogXCJkZC1NLXl5eXlcIixcbiAgICAgICAgICAgIGxhbmd1YWdlOiBcImVuXCIsXG4gICAgICAgICAgICBvcmllbnRhdGlvbjogXCJib3R0b20gYXV0b1wiLFxuICAgICAgICAgICAgdG9kYXlCdG46IFwibGlua2VkXCIsXG4gICAgICAgICAgICB0b2RheUhpZ2hsaWdodDogdHJ1ZVxuICAgICAgICB9KS5vbignY2xpY2snLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgJGkgPSAkKHRoaXMpO1xuICAgICAgICAgICAgaWYgKCRpLmhhc0NsYXNzKCdvcGVuZWQnKSkge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdBbHJlYWR5IG9wZW5lZC4nKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgJGkuYWRkQ2xhc3MoJ29wZW5lZCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTtcbn0pKGpRdWVyeSk7XG4iLCIvLyBGaWxlIGlucHV0XG4oZnVuY3Rpb24gKCQpIHtcbiAgICAkKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgnLmVzLWZpbGUtY29udHJvbCAuZXMtYnV0dG9uJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgICAgICQoJy5lcy1oaWRkZW4tZmlsZS1pbnB1dCBpbnB1dCcsICQodGhpcykucGFyZW50KCkucGFyZW50KCkpLmNsaWNrKCk7XG4gICAgICAgIH0pO1xuICAgICAgICAkKCcuZXMtZmlsZS1jb250cm9sIC5lcy1oaWRkZW4tZmlsZS1pbnB1dCBpbnB1dCcpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgdmFyIGZpbGVzID0gJCh0aGlzKVswXS5maWxlcyxcbiAgICAgICAgICAgICAgICBmaWxlTmFtZXMgPSBmaWxlcyA/IEFycmF5LnByb3RvdHlwZS5tYXAuY2FsbChmaWxlcywgZnVuY3Rpb24gKGZpbGUpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZpbGUubmFtZTtcbiAgICAgICAgICAgICAgICB9KS5qb2luKCcsICcpIDogJCh0aGlzKS52YWwoKS5zcGxpdCgvKFxcXFx8XFwvKS9nKS5wb3AoKTtcbiAgICAgICAgICAgICQoJy5lcy1maWxlLW5hbWUgaW5wdXQnLCAkKHRoaXMpLnBhcmVudCgpLnBhcmVudCgpKS52YWwoZmlsZU5hbWVzKTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG59KShqUXVlcnkpO1xuIiwiLy8gVGltZSBwaWNrZXIgdGltZSBmdW5jdGlvblxuKGZ1bmN0aW9uICgkKSB7XG4gICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgICQoJy5lcy10aW1lLWlucHV0ID4gYnV0dG9uJykub24oJ2NsaWNrJywgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgLy9nZXQgdGhlIGN1cnJlbnQgdGltZVxuICAgICAgICAgICAgdmFyIG5vdyA9IG5ldyBEYXRlKERhdGUubm93KCkpO1xuICAgICAgICAgICAgdmFyICR0aW1lID0gbm93LmdldEhvdXJzKCkgKyBcIjpcIiArIG5vdy5nZXRNaW51dGVzKCkgKyBcIjpcIiArIG5vdy5nZXRTZWNvbmRzKCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHZhciAkaW5wdXQgPSAkKHRoaXMpLnByZXYoKTtcbiAgICAgICAgICAgICRpbnB1dC52YWwoJHRpbWUpO1xuICAgICAgICB9KTtcbiAgICB9KTtcbn0pKGpRdWVyeSk7XG4iLCIvLyBHcmlkIHNlY3Rpb25zXG4oZnVuY3Rpb24oJCl7XG4gICAgJChmdW5jdGlvbigpe1xuICAgICAgICAkKCcuZXMtZ3JpZCAuZXMtYWN0aW9uYmFyID4gLmVzLWNoZWNrYm94ID4gaW5wdXQnKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgJChlLnRhcmdldCkuY2xvc2VzdCgndHInKS50b2dnbGVDbGFzcyhcIi1lcy1zZWxlY3RlZFwiKTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG59KShqUXVlcnkpO1xuIiwiLy8gUmVzcG9uc2l2ZSBCcmVhZGNydW1ic1xuKGZ1bmN0aW9uICgkKSB7XG4gICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBhZGp1c3RGb3JNb2JpbGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKCcuZXMtYnJlYWRjcnVtYnMnKS5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpO1xuICAgICAgICAgICAgICAgIHZhciAkbW9yZUVsZW1lbnQgPSAkdGhpcy5maW5kKCcuZXMtbW9yZScpO1xuICAgICAgICAgICAgICAgIGlmICghJG1vcmVFbGVtZW50KSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKCRtb3JlRWxlbWVudC5pcygnOnZpc2libGUnKSAmJiAkdGhpcy5maW5kKFwiPiBuYXYgPiB1bFwiKS5jaGlsZHJlbigpLmxlbmd0aCA+PSAzKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciAkZWxlbWVudHMgPSAkdGhpcy5maW5kKCc+IG5hdiA+IHVsJykuY2hpbGRyZW4oKS5zbGljZSgxLCAtMik7XG4gICAgICAgICAgICAgICAgICAgICRlbGVtZW50cy5lYWNoKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciAkbmF2TGlzdCA9ICRtb3JlRWxlbWVudC5maW5kKCduYXYgPiB1bCcpO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCRuYXZMaXN0KSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJG5hdkxpc3QuYXBwZW5kKCQodGhpcykpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmICghJG1vcmVFbGVtZW50LmlzKCc6dmlzaWJsZScpICYmICRtb3JlRWxlbWVudC5maW5kKFwiZGl2ID4gbmF2ID4gdWxcIikuY2hpbGRyZW4oKS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciAkZWxlbWVudHMgPSAkbW9yZUVsZW1lbnQuZmluZChcImRpdiA+IG5hdiA+IHVsXCIpLmNoaWxkcmVuKCk7XG4gICAgICAgICAgICAgICAgICAgICRtb3JlRWxlbWVudC5hZnRlcigkZWxlbWVudHMpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcblxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgIH07XG4gICAgICAgIGFkanVzdEZvck1vYmlsZSgpO1xuICAgICAgICAkKHdpbmRvdykucmVzaXplKGFkanVzdEZvck1vYmlsZSk7XG4gICAgICAgICQod2luZG93KS5vbignb3JpZW50YXRpb25jaGFuZ2UnLCBhZGp1c3RGb3JNb2JpbGUoKSk7XG4gICAgfSk7XG59KShqUXVlcnkpOyIsIi8vIE1vZGFsIFBvcC11cFxuKGZ1bmN0aW9uICgkKSB7XG4gICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciAkcG9wdXAsXG4gICAgICAgICAgICBmb2N1c2FibGVTZWxlY3RvciA9ICdhW2hyZWZdLCBhcmVhW2hyZWZdLCBpbnB1dDpub3QoW2Rpc2FibGVkXSksIGJ1dHRvbjpub3QoW2Rpc2FibGVkXSksIHNlbGVjdDpub3QoW2Rpc2FibGVkXSksIHRleHRhcmVhOm5vdChbZGlzYWJsZWRdKSwgaWZyYW1lLCBvYmplY3QsIGVtYmVkLCAqW3RhYmluZGV4XSwgKltjb250ZW50ZWRpdGFibGU9dHJ1ZV0nO1xuXG4gICAgICAgIGZ1bmN0aW9uIG9uQ2xpY2soZSkge1xuICAgICAgICAgICAgICgkKGUudGFyZ2V0KS5jbG9zZXN0KCdbZGF0YS1kaXNtaXNzPVwibW9kYWxcIl0nKS5sZW5ndGgpICYmIGNsb3NlKCk7XG4gICAgICAgIH1cblxuICAgICAgICAvL2xldCB0YWIgbmF2aWdhdGlvbiBjeWNsZSBpbiB0aGUgcG9wdXAgYm9keVxuICAgICAgICBmdW5jdGlvbiBjeWNsZShlKSB7XG4gICAgICAgICAgICB2YXIgZm9jdXNDaGFuZ2VkID0gZmFsc2U7XG4gICAgICAgICAgICB2YXIgZm9jdXNhYmxlID0gW10uc2xpY2UuY2FsbCgkcG9wdXBbMF0ucXVlcnlTZWxlY3RvckFsbChmb2N1c2FibGVTZWxlY3RvcikpO1xuXG4gICAgICAgICAgICBmdW5jdGlvbiBpc0ZvY3VzSW5GaXJzdCgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gISEoZm9jdXNhYmxlICYmIGZvY3VzYWJsZS5sZW5ndGggJiYgKGUudGFyZ2V0IHx8IGUuc3JjRWxlbWVudCkgPT09IGZvY3VzYWJsZVswXSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGlzRm9jdXNJbkxhc3QoKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuICEhKGZvY3VzYWJsZSAmJiBmb2N1c2FibGUubGVuZ3RoICYmIChlLnRhcmdldCB8fCBlLnNyY0VsZW1lbnQpID09PSBmb2N1c2FibGVbZm9jdXNhYmxlLmxlbmd0aCAtIDFdKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgZnVuY3Rpb24gaXNGb2N1c0luc2lkZSgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gJC5jb250YWlucygkcG9wdXBbMF0sIGUudGFyZ2V0IHx8IGUuc3JjRWxlbWVudCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGZvY3VzRmlyc3QoKSB7XG4gICAgICAgICAgICAgICAgaWYgKGZvY3VzYWJsZSAmJiBmb2N1c2FibGUubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvY3VzYWJsZVswXS5mb2N1cygpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBmdW5jdGlvbiBmb2N1c0xhc3QoKSB7XG4gICAgICAgICAgICAgICAgaWYgKGZvY3VzYWJsZSAmJiBmb2N1c2FibGUubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvY3VzYWJsZVtmb2N1c2FibGUubGVuZ3RoIC0gMV0uZm9jdXMoKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKGUuc2hpZnRLZXkpIHtcbiAgICAgICAgICAgICAgICBpZiAoIWlzRm9jdXNJbnNpZGUoKSB8fCBpc0ZvY3VzSW5GaXJzdCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvY3VzQ2hhbmdlZCA9IGZvY3VzTGFzdCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgaWYgKCFpc0ZvY3VzSW5zaWRlKCkgfHwgaXNGb2N1c0luTGFzdCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvY3VzQ2hhbmdlZCA9IGZvY3VzRmlyc3QoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoZm9jdXNDaGFuZ2VkKSB7XG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBvbktleURvd24oZSkge1xuICAgICAgICAgICAgaWYgKGUua2V5KSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChlLmtleSkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdFc2MnOlxuICAgICAgICAgICAgICAgICAgICBjYXNlICdFc2NhcGUnOlxuICAgICAgICAgICAgICAgICAgICAgICAgY2xvc2UoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICBjYXNlICdUYWInOlxuICAgICAgICAgICAgICAgICAgICAgICAgY3ljbGUoZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBpbml0RXZlbnRzKCkge1xuICAgICAgICAgICAgJChkb2N1bWVudCkub24oJ2tleWRvd24nLCBvbktleURvd24pO1xuICAgICAgICAgICAgJHBvcHVwLm9uKCdjbGljaycsIG9uQ2xpY2spO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gcmVtb3ZlRXZlbnRzKCkge1xuICAgICAgICAgICAgJChkb2N1bWVudCkub2ZmKCdrZXlkb3duJywgb25LZXlEb3duKTtcbiAgICAgICAgICAgICRwb3B1cC5vZmYoJ2NsaWNrJywgb25jbGljayk7XG4gICAgICAgIH1cblxuICAgICAgICBmdW5jdGlvbiBjbG9zZSgpIHtcbiAgICAgICAgICAgICRwb3B1cC5yZW1vdmVDbGFzcygnLWVzLW9wZW4nKTtcbiAgICAgICAgICAgICQoJ2h0bWwgYm9keScpLnJlbW92ZUNsYXNzKCctZXMtcG9wdXAtb3BlbmVkJyk7XG4gICAgICAgICAgICByZW1vdmVFdmVudHMoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGZ1bmN0aW9uIG9wZW4oKSB7XG4gICAgICAgICAgICAkcG9wdXAuYWRkQ2xhc3MoJy1lcy1vcGVuJyk7XG4gICAgICAgICAgICAkKCdodG1sIGJvZHknKS5hZGRDbGFzcygnLWVzLXBvcHVwLW9wZW5lZCcpO1xuICAgICAgICAgICAgaW5pdEV2ZW50cygpO1xuICAgICAgICB9XG5cbiAgICAgICAgZnVuY3Rpb24gaXNPcGVuKCkge1xuICAgICAgICAgICAgcmV0dXJuICRwb3B1cC5oYXNDbGFzcygnLi1lcy1vcGVuJyk7XG4gICAgICAgIH1cblxuICAgICAgICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnW2RhdGEtZXMtb3Blbi1wb3B1cF0nLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgdmFyICRwID0gJCgnIycgKyAkKHRoaXMpLmRhdGEoJ2VzLW9wZW4tcG9wdXAnKSk7XG4gICAgICAgICAgICAkcG9wdXAgPSAkcC5sZW5ndGggJiYgJHA7XG4gICAgICAgICAgICAkcG9wdXAgJiYgb3BlbigpO1xuICAgICAgICB9KTtcbiAgICB9KTtcbn0pKGpRdWVyeSk7XG4iLCIvLyBTb3J0aW5nIFRhYmxlXG4oZnVuY3Rpb24gKCQpIHtcbiAgICAkKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgJCgndGguLWVzLXNvcnQsIHRoLi1lcy1zb3J0LWRlc2NlbmRpbmcsIHRoLi1lcy1zb3J0LWFzY2VuZGluZycpLmNsaWNrKGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgJCh0aGlzKS5lYWNoKGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgdmFyIGNsYXNzZXMgPSBbJy1lcy1zb3J0LWRlc2NlbmRpbmcnLCctZXMtc29ydC1hc2NlbmRpbmcnLCctZXMtc29ydCddO1xuICAgICAgICAgICAgICAgIHRoaXMuY2xhc3NOYW1lID0gY2xhc3Nlc1soJC5pbkFycmF5KHRoaXMuY2xhc3NOYW1lLCBjbGFzc2VzKSsxKSVjbGFzc2VzLmxlbmd0aF07XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG59KShqUXVlcnkpO1xuIiwiLy8gU2VsZWN0YWJsZSBUYWJsZVxuKGZ1bmN0aW9uICgkKSB7XG4gICAgJChmdW5jdGlvbiAoKSB7XG4gICAgICAgICQoJy4tZXMtc2VsZWN0YWJsZSA+IHRib2R5ID4gdHInKS5vbignY2xpY2snLCBmdW5jdGlvbiAoZSkge1xuXG4gICAgICAgICAgICBpZiAoICQoZS50YXJnZXQpLmNsb3Nlc3QoXCIuZXMtZ3JpZC1ib2R5XCIpLmxlbmd0aCA+IDAgKSB7XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICQoJCh0aGlzKSkudG9nZ2xlQ2xhc3MoXCItZXMtc2VsZWN0ZWRcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pO1xufSkoalF1ZXJ5KTtcbiJdfQ==
