// Fly-out / Dropdown Menu
(function($){
    $(function(){
        $('[data-es-dropdown] > button, [data-es-dropdown] > a').on('click', function(){
            var $p = $(this).parent(), o = $p.hasClass('-es-open');
            $('[data-es-dropdown]').removeClass('-es-open');
            !o && $p.addClass('-es-open');
        });
        $(document).on('click', function(e){
            ! $(e.target).closest('[data-es-dropdown] > button, [data-es-dropdown] > a').length &&
            $('[data-es-dropdown]').removeClass('-es-open');
        })
    });
})(jQuery);
