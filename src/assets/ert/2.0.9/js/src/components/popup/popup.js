// Modal Pop-up
(function ($) {
    $(function () {
        var $popup,
            focusableSelector = 'a[href], area[href], input:not([disabled]), button:not([disabled]), select:not([disabled]), textarea:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable=true]';

        function onClick(e) {
             ($(e.target).closest('[data-dismiss="modal"]').length) && close();
        }

        //let tab navigation cycle in the popup body
        function cycle(e) {
            var focusChanged = false;
            var focusable = [].slice.call($popup[0].querySelectorAll(focusableSelector));

            function isFocusInFirst() {
                return !!(focusable && focusable.length && (e.target || e.srcElement) === focusable[0]);
            }

            function isFocusInLast() {
                return !!(focusable && focusable.length && (e.target || e.srcElement) === focusable[focusable.length - 1]);
            }

            function isFocusInside() {
                return $.contains($popup[0], e.target || e.srcElement);
            }

            function focusFirst() {
                if (focusable && focusable.length) {
                    focusable[0].focus();
                    return true;
                }
                return false;
            }

            function focusLast() {
                if (focusable && focusable.length) {
                    focusable[focusable.length - 1].focus();
                    return true;
                }
                return false;
            }

            if (e.shiftKey) {
                if (!isFocusInside() || isFocusInFirst()) {
                    focusChanged = focusLast();
                }
            } else {
                if (!isFocusInside() || isFocusInLast()) {
                    focusChanged = focusFirst();
                }
            }
            if (focusChanged) {
                e.preventDefault();
                e.stopPropagation();
            }
        }

        function onKeyDown(e) {
            if (e.key) {
                switch (e.key) {
                    case 'Esc':
                    case 'Escape':
                        close();
                        break;
                    case 'Tab':
                        cycle(e);
                        break;
                }
            }
        }

        function initEvents() {
            $(document).on('keydown', onKeyDown);
            $popup.on('click', onClick);
        }

        function removeEvents() {
            $(document).off('keydown', onKeyDown);
            $popup.off('click', onclick);
        }

        function close() {
            $popup.removeClass('-es-open');
            $('html body').removeClass('-es-popup-opened');
            removeEvents();
        }

        function open() {
            $popup.addClass('-es-open');
            $('html body').addClass('-es-popup-opened');
            initEvents();
        }

        function isOpen() {
            return $popup.hasClass('.-es-open');
        }

        $(document).on('click', '[data-es-open-popup]', function (e) {
            var $p = $('#' + $(this).data('es-open-popup'));
            $popup = $p.length && $p;
            $popup && open();
        });
    });
})(jQuery);
