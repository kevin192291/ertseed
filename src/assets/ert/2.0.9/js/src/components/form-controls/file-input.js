// File input
(function ($) {
    $(function () {
        $('.es-file-control .es-button').on('click', function (e) {
            $('.es-hidden-file-input input', $(this).parent().parent()).click();
        });
        $('.es-file-control .es-hidden-file-input input').on('change', function (e) {
            var files = $(this)[0].files,
                fileNames = files ? Array.prototype.map.call(files, function (file) {
                    return file.name;
                }).join(', ') : $(this).val().split(/(\\|\/)/g).pop();
            $('.es-file-name input', $(this).parent().parent()).val(fileNames);
        });
    });
})(jQuery);
