// Date Picker popup
(function ($) {
    $(function () {
        $('.es-date-input > input').datepicker({
            autoclose: true,
            format: "dd-M-yyyy",
            language: "en",
            orientation: "bottom auto",
            todayBtn: "linked",
            todayHighlight: true
        }).on('click', function () {
            var $i = $(this);
            if ($i.hasClass('opened')) {
                console.log('Already opened.')
            }
            else {
                $i.addClass('opened');
            }
        });
    });
})(jQuery);
