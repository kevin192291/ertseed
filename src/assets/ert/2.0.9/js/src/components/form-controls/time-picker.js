// Time picker time function
(function ($) {
    $(function () {
        $('.es-time-input > button').on('click', function () {
            //get the current time
            var now = new Date(Date.now());
            var $time = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
            
            var $input = $(this).prev();
            $input.val($time);
        });
    });
})(jQuery);
