// Responsive Breadcrumbs
(function ($) {
    $(function () {
        var adjustForMobile = function () {
            $('.es-breadcrumbs').each(function () {
                var $this = $(this);
                var $moreElement = $this.find('.es-more');
                if (!$moreElement) {
                    return;
                }
                if ($moreElement.is(':visible') && $this.find("> nav > ul").children().length >= 3) {
                    var $elements = $this.find('> nav > ul').children().slice(1, -2);
                    $elements.each(function () {
                        var $navList = $moreElement.find('nav > ul');
                        if ($navList) {
                            $navList.append($(this));
                        }
                    });

                }
                else if (!$moreElement.is(':visible') && $moreElement.find("div > nav > ul").children().length > 0) {
                    var $elements = $moreElement.find("div > nav > ul").children();
                    $moreElement.after($elements);
                }
                else {

                }
            })
        };
        adjustForMobile();
        $(window).resize(adjustForMobile);
        $(window).on('orientationchange', adjustForMobile());
    });
})(jQuery);