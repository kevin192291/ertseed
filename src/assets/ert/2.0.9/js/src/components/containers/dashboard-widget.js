// Dashboard widget
(function ($) {
    $(function () {
        $('[data-es-expand-collapse]').on('click', function (e) {
            var $target = $(e.target),
                $widget = $target.closest('.es-widget'),
                $header = $widget.find('.es-widget-header'),
                $icon = $header.find('.es-icon'),
                $content = $widget.find('.es-widget-body');

            if ($widget.hasClass('-es-collapsed')) {
                $content.show();
            } else {
                $content.hide();
            }

            $widget.toggleClass('-es-collapsed');
            $icon.toggleClass('es-icon-chevron-right es-icon-chevron-down');
        });

        $(window).on('resize', close);
        $(document).on('click', close);

        function close() {
            var $widget = $(document).find('.es-widget'),
                $nav = $widget.find('nav');

            $nav.removeClass('-es-open');
        }
    });
})(jQuery);
