// Tabs
(function ($) {
    $('.es-tab a').click(function(e){
        e.preventDefault();

        var $this = $(this);
        var $tabs = $this.parents('.es-tabs');
        var $tabPanels = $('#'+$this.parents('.es-tabs').data('panel-group'));
        var target = $this.attr('href');
        var $targetPanel = $(target);
        var $targetTab = $tabs.not('.-es-more').find('li a[href="'+target+'"]');
            
        // set embedded tab panels inactive
        $tabPanels.find('.es-panel').removeClass('-es-active');
        // set navigation elements inactive
        $tabs.find('li').removeClass('-es-active');

        // set target panel active 
        $targetPanel.addClass('-es-active');
        // mark target panel navigation element active
        $targetTab.parent().addClass('-es-active');
    });
})(jQuery);

