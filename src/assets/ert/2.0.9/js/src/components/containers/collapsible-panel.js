// collapsible panels
(function($){
    $(function(){
        $('[data-toggle="panel"]').on('click', function(e){
            $(e.target).closest('.es-collapsible-panel').toggleClass('-es-open');
        });
    });
})(jQuery);
