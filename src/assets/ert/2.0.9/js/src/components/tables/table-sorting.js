// Sorting Table
(function ($) {
    $(function () {
        $('th.-es-sort, th.-es-sort-descending, th.-es-sort-ascending').click(function () {
           $(this).each(function(){
                var classes = ['-es-sort-descending','-es-sort-ascending','-es-sort'];
                this.className = classes[($.inArray(this.className, classes)+1)%classes.length];
            });
        });
    });
})(jQuery);
