# ERT WebStyle UI Toolkit for Web Applications

Purpose of the ERT WebStyle UI Toolkit is to provide a common CSS style UI toolkit for all ERT web developers who are creating web applications and would like to provide a unified look and feel for end users.

# Table of Contents

* [Technology Overview](#technology-overview)
* [Dependencies](#dependencies)
* [Getting started](#gettiung-started)
* [Development](#development)
* [Useful links](#useful-links)
* [QA](#QA)
* [File Structure](#file-structure)


#  Technology Overview

The ERT WebStyle Framework is implemented using [SASS](http://sass-lang.com/)

## Dependencies

What you need to run this app:
* `node` and `npm` (Use [NVM](https://github.com/creationix/nvm))
* Ensure you're running Node (`v4.1.x`+) and NPM (`2.14.x`+)

>Warning: Make sure you're using the latest version of Node.js and NPM

# Getting Started

## Installation for npm package

# How to download npm package from nexus server

You need a `.npmrc` file in the root of your project to be able to download the ert-webstyle npm package. If you don`t have one, create a file .npmrc in your root folder and put this code in it:

'''
registry = http://cd.ert.com/nexus/content/groups/npm-public

'''

# Install package

`npm install ert-webstyle --save`

# Usage in your project

After the installation you have to include the CSS file `ert-webstyle.css` in your HTML files(s). When this is done, you can start using the CSS styles in your project/application.

Please have a look at the [StyleGuide](http://styleguide.ert.com) for examples of all elements and how to use the styles.

Every css class or modifier starts with an `es-`.
So to include the styles just add the class to your html element.

Examples:

* Attach the class `es-button` to your  Button <button>
* Attach the class `es-input` to your input field <input>
* Attach the class `es-table` to your html table <table>
  and so on.

For specific usage, descriptions and guidelines on how to use it, visit the StyleGuide [Homepage](http://styleguide.ert.com)

## Installation for Development

If you want to start developing elements with or for the StyleGuide, please follow these steps:

# Clone git repository
`git clone https://crucible.ert.com/git/webStyleGuide.git`
`cd webStyleGuide/ertWebStyle`

# Install public NPM packages

`npm install`

## Running the app

After you have installed all dependencies you can now run the app with:

`gulp`

It will compile the current style guide and start a local web server using `browser-sync` which will watch, build (in-memory), and reload for you. The port will be displayed to you as `http://localhost:9009`.

# Development

## Building style guide

* simply run: `gulp build`
* Build style guide and watch the live changes in a Browser window: `gulp`
* You can trigger a Release Build with `gulp build --env=release`

## Tools

* WebStorm https://www.jetbrains.com/webstorm/
* Google Chrome Developer Tools https://developer.chrome.com/devtools

# General

## Useful links

* AngularJs and front-end JS code conventions https://goo.gl/Ypr0aO
* StyleGuide Homepage http://styleguide.ert.com

# Q&A

## Questions

Please have a look at the Intro Section on the StyleGuide [Homepage](http://styleguide.ert.com)
or please feel free to contact [Axel Pfeuffer](mailto:axel.pfeuffer@ert.com) or [Daniel Lenhart](mailto: daniel.lenhart@ert.com)


## File Structure

```
} package.json                          * package file for npm
} README.md                             * README
} css                                  * folder for CSS build output. Do not touch! Will be generated!
    L ...
} fonts                                * folder for CSS UI specific font files
|   L ...
} images                               * folder for CSS UI specific images files
    L ...
} js                                   * folder for CSS UI specific jscript files
    L ...
} sass
|   } _configuration.scss
|   } _configuration                   * folder for SASS configurations and variables
|       } atoms
|   |   |   } ...
|   |   } molecules
|   |   |   L ...
|   |   } organisms
|   |   |   L ...
|   |   } _color-palette.scss
|   |   |   L _general.scss
|   } _modules                         * folder for SASS mixins and functions
|   |   } atoms
|   |   |   L ...
|   |   } common
|   |   |    } _util.scss
|   |   } core
|   |   |   L ...
|   |   } molecules
|   |   |   L ...
|   |   } organisms
|   |   |   L ...
|   |   } pages                        * folder for UI page SASS files
|   |   |   L ...
|   |   L molecules                    * folder for UI molecule specific SASS files
|   |       L ...
|   } atoms                            * folder for UI atom specific SASS files
|   |   L ...
|   } molecules
|   |   L ...
|   } organisms
|   |   L ...
|   } pages
|   |   L ...
|   } _general.scss
|   } _modules.scss
|   } _normalize.scss
|   } ert-webstyle-global-header.scss
|   L ert-webstyle.scss
} .npmrc                               * file needed for download of npm package from nexus server
L ums-pkg.json                         * file for deployment and build



```