import { Navigate } from './../../shared/router.state';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngxs/store';
import { environment } from '../../../environments/environment';
@Component({
  selector: 'ert-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  userName$: Observable<string>;
  userEmail$: Observable<string>;
  appName = environment.appName;

  constructor(
    private store: Store,
  ) { }

  ngOnInit() {
  }

  navToGateway() {
    debugger;
    this.store.dispatch(new Navigate('dashboard/gateway'));
  }

}
