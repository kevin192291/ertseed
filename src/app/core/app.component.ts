import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';
import { AppState } from '../shared/app.state';
import { takeUntil, filter } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @Select(AppState.getAuthenticatedState) authenticated$: Observable<boolean>;
  @Select(AppState.getSidebarState) sidebarOpen$: Observable<boolean>;
  
  constructor(
    private store: Store,

  ) {
  }

  ngOnInit() {
  }

  onActivate($event): void {

  }

  onDeactivate($event): void {
    
  }
}
