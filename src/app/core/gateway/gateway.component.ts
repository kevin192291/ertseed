import { SetUserName, SetUserEmail, SetAuthenticated } from './../../shared/app.actions';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Navigate } from '../../shared/router.state';

@Component({
  selector: 'ert-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.scss']
})
export class GatewayComponent implements OnInit {

  constructor(
    private store: Store,
  ) { }

  ngOnInit() { }

  login(): void {
    this.store.dispatch([
      new SetUserName('Kevin Cantrell'),
      new SetUserEmail('Kevin.Cantrell@ert.com'),
      new SetAuthenticated(true),
    ]);
  }

}