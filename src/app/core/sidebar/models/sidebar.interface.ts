export interface SidebarItem {
    text: string;
    link: string;
    icon: string;
}

export interface SidebarSection {
    text: string;
    icon: string;
    items: SidebarItem[];
}