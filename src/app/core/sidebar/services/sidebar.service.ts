import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import 'rxjs/add/observable/of';
import { SidebarSection, SidebarItem } from '../models/sidebar.interface';

@Injectable()
export class SidebarService {

  constructor() { }

  public getSidebarItems(): Observable<SidebarSection[]> {

    let items: SidebarItem[] = [
      {
        text: 'link link',
        icon: 'es-icon es-icon-dashboard',
        link: '/',
      },
      {
        text: 'link link',
        icon: 'es-icon es-icon-dashboard',
        link: '/',
      },
      {
        text: 'link link',
        icon: 'es-icon es-icon-dashboard',
        link: '/',
      },
      {
        text: 'link link',
        icon: 'es-icon es-icon-dashboard',
        link: '/',
      },
    ];

    let sections: SidebarSection[] = [
      {
        text: 'this that',
        icon: 'es-icon es-icon-dashboard',
        items: items
      },
      {
        text: 'this who',
        icon: 'es-icon es-icon-dashboard',
        items: items
      },
      {
        text: 'me that',
        icon: 'es-icon es-icon-dashboard',
        items: items
      },
    ];

    return Observable.of(sections);
  }

}
