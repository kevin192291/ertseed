import { SidebarService } from './services/sidebar.service';
import { Observable } from 'rxjs';
import { ToggleSidebar } from './../../shared/app.actions';
import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { SidebarSection } from './models/sidebar.interface';

@Component({
  selector: 'ert-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  sidebarItems$: Observable<SidebarSection[]>;

  constructor(
    private store: Store,
    private sidebarService: SidebarService,
  ) {
    this.sidebarItems$ = this.sidebarService.getSidebarItems();

  }

  ngOnInit() {
  }

  toggle(): void {
    this.store.dispatch(ToggleSidebar);
  }

}
