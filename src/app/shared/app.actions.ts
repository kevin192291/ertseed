export class ToggleSidebar {
    static readonly type = '[app] toggle sidebar';
    //constructor(public payload: string) { }
}

export class GetSidebarState {
    static readonly type = '[app] get sidebar state';
}

export class SetUserName {
    static readonly type = '[app] set username';
    constructor(public payload: string) { }
}

export class SetUserEmail {
    static readonly type = '[app] set email';
    constructor(public payload: string) { }
}

export class SetAuthenticated {
    static readonly type = '[app] set authenticated';
    constructor(public payload: boolean) { }
}