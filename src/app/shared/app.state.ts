import { AppStateModel } from './app.state';
import { ToggleSidebar, SetUserName, SetUserEmail, SetAuthenticated } from './app.actions';
import { State, Action, StateContext, Selector } from "@ngxs/store";

export interface AppStateModel {
    sidebarOpen: boolean;
    authenticated: boolean;
    userName: string;
    userEmail: string;
}

@State<AppStateModel>({
    name: 'app',
    defaults: {
        sidebarOpen: false,
        authenticated: false,
        userName: '',
        userEmail: '',
    }
})
export class AppState {

    @Selector()
    static getSidebarState(state: AppStateModel) {
        return state.sidebarOpen;
    }
    @Selector()
    static getAuthenticatedState(state: AppStateModel) {
        return state.authenticated;
    }

    @Action(ToggleSidebar)
    toggleSidebar({ getState, patchState }: StateContext<AppStateModel>) {
        const currentState = getState().sidebarOpen;
        patchState({ sidebarOpen: !currentState })
    }

    @Action(SetAuthenticated)
    setAuthenticated({ patchState }: StateContext<AppStateModel>, action: SetAuthenticated) {
        patchState({ authenticated: action.payload })
    }

    @Action(SetUserName)
    setUserName({ patchState }: StateContext<AppStateModel>, action: SetUserName) {
        patchState({ userName: action.payload })
    }

    @Action(SetUserEmail)
    setUserEmail({ patchState }: StateContext<AppStateModel>, action: SetUserEmail) {
        patchState({ userEmail: action.payload })
    }
}